﻿using AspNetCoregRPCService;
using Grpc.Net.Client;
using System;

namespace AspNetCoregRPCClient
{
    /// <summary>
    /// Nuget Grpc.Net.Client、Google.Protobuf、Grpc.Tools
    /// </summary>
    class Program
    {
        static  void Main(string[] args)
        {
            DoSomething();
            Console.ReadKey();
        }
        static async void DoSomething()
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Greeter.GreeterClient(channel);
            var reply = await client.SayHelloAsync(
                new HelloRequest { Name = "晓晨" });
            Console.WriteLine("Greeter 服务返回数据: " + reply.Message);
        }
    }
}
