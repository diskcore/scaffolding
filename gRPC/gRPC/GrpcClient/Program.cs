using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace GrpcClientDemo
{

    /// <summary>
    /// 安装grpc工具命令行：dotnet tool  install dotnet-grpc -g
    /// 添加grpc文件：dotnet grpc add-file ..\GrpcServer\Proto\order.proto 
    /// 远程url测试地址：https://github.com/grpc/grpc/blob/master/examples/protos/helloworld.proto
    /// 远程url来添加：
    ///  dotnet grpc add-url https://gitee.com/mirrors/grpc/raw/master/examples/protos/helloworld.proto -o Protos\helloword.proto 
    ///  dotnet grpc refresh https://gitee.com/mirrors/grpc/raw/master/examples/protos/helloworld.proto
    ///  dotnet grpc remove https://gitee.com/mirrors/grpc/raw/master/examples/protos/helloworld.proto
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
