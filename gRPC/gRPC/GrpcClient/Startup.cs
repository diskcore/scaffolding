using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GrpcClientDemo.Interceptors;
//using GrpcServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;
using Microsoft.Extensions.Http;
using Polly.Extensions.Http;
//using static GrpcServices.OrderGrpc;
using Polly.CircuitBreaker;
using System.Net;
using Polly.Bulkhead;
using GrpcServer.Proto;
using static GrpcServer.Proto.OrderGrpc;

namespace GrpcClientDemo
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //允许使用不加密的HTTP/2协议
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            services.AddGrpcClient<OrderGrpc.OrderGrpcClient>(options =>
            {
                options.Address = new Uri("https://localhost:5001");
            })
            .ConfigurePrimaryHttpMessageHandler(provider =>
            {
                ////允许无效、或自签名证书
                var handler = new SocketsHttpHandler();
                handler.SslOptions.RemoteCertificateValidationCallback = (a, b, c, d) => true;
                return handler;
            })
            .AddTransientHttpErrorPolicy
            (
                //响应码是500 才会执行重试
                p =>p.WaitAndRetryAsync(20,i => TimeSpan.FromSeconds(2))    //等待两秒再重试 重试20次
               //p.WaitAndRetryAsync(20, i => TimeSpan.FromSeconds(i * 2))  //等待时间步长增长 再重试 重试20次
            //p.RetryAsync()                                                //重试一次
            //p.WaitAndRetryForeverAsync(i => TimeSpan.FromSeconds(i * 3))  //不限次数 一直重试 等待三秒
            );

            //services.AddGrpcClient<Helloworld.Greeter.GreeterClient>();

            //Polly的能力
            //失败重试 
            //服务熔断 
            //超时处理 
            //舱壁隔离 
            //缓存策略 
            //失败降级 
            //组合策略
            //使用步骤
            //1定义要处理的异常类型或返回值
            //2定义要处理的动作（重试 熔断 降级响应）
            //3使用定义的策略来执行代码
            //失败重试场景：服务是短暂失败 可自愈。服务是幂等的 重复调用不会有副作用
            //设置失败重试的次数
            //设置步长
            //设置降级响应
            //设置熔断器

            //定义自己的策略
            var reg = services.AddPolicyRegistry();

            //当响应码是201 就配置一直重试
            reg.Add("retryforever", Policy.HandleResult<HttpResponseMessage>
             (message =>
            {
                return message.StatusCode == System.Net.HttpStatusCode.Created;
            }).RetryForeverAsync());



            //应用策略
            services.AddHttpClient("orderclient").
              AddPolicyHandlerFromRegistry("retryforever");


            services.AddHttpClient("orderclientv2").
                AddPolicyHandlerFromRegistry((registry, message) =>
            {
                return message.Method == HttpMethod.Get ? registry
                .Get<IAsyncPolicy<HttpResponseMessage>>("retryforever") : Policy.NoOpAsync<HttpResponseMessage>();//空策略
            });

            //普通熔断  失败的次数
            services.AddHttpClient("orderclientv3").
                AddPolicyHandler(
                Policy<HttpResponseMessage>.
                Handle<HttpRequestException>().CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 10,     //报错多少次后进行熔断
                durationOfBreak: TimeSpan.FromSeconds(10),  //熔断的时间
                onBreak: (r, t) => { },                     //触发熔断事件
                onReset: () => { },                         //熔断恢复时的事件
                onHalfOpen: () => { }                       //用一部分流量去验证服务是否可用 
                ));


            //高级熔断 根据失败的比例
            services.AddHttpClient("orderclientv3").
                AddPolicyHandler(
                Policy<HttpResponseMessage>
                .Handle<HttpRequestException>()
                .AdvancedCircuitBreakerAsync(
                failureThreshold: 0.8,                       //80%出现错误
                samplingDuration: TimeSpan.FromSeconds(10),  //采样的时间
                minimumThroughput: 100,                      //最小的吞吐量     100 请求量比较小 10秒之内只有2个请求失败 不会触发 最少是100
                durationOfBreak: TimeSpan.FromSeconds(20),   //熔断的时长
                onBreak: (r, t) => { },                      //同上
                onReset: () => { },
                onHalfOpen: () => { }));


            var breakPolicy = Policy<HttpResponseMessage>
                .Handle<HttpRequestException>()
                .AdvancedCircuitBreakerAsync(
                failureThreshold: 0.8,
                samplingDuration: TimeSpan.FromSeconds(10),
                minimumThroughput: 100,
                durationOfBreak: TimeSpan.FromSeconds(20),
                onBreak: (r, t) => { },
                onReset: () => { },
                onHalfOpen: () => { });

            //失败降级策略
            var message = new HttpResponseMessage()
            {
                Content = new StringContent("{}")
            };

            //降级策略
            var fallback = Policy<HttpResponseMessage>
                .Handle<BrokenCircuitException>()
                .FallbackAsync(message);

            //失败重试策略 遇到任何错误都retry
            var retry = Policy<HttpResponseMessage>
                .Handle<Exception>()
                .WaitAndRetryAsync(3, i => TimeSpan.FromSeconds(1));

            //组合策略  熔断降级
            var fallbackBreak = Policy.WrapAsync(fallback, retry, breakPolicy);

            services.AddHttpClient("httpv3")
                .AddPolicyHandler(fallbackBreak);


            //限流策略
            var bulk = Policy.BulkheadAsync<HttpResponseMessage>(
                maxParallelization: 30,   //并发最多是30
                maxQueuingActions: 20,    //最大队列数  请求超过30并发，剩下的请求排队  可以有20个请求在队列排队
                onBulkheadRejectedAsync: contxt => Task.CompletedTask             //请求被限流了怎么处理
                );

            var message2 = new HttpResponseMessage()
            {
                Content = new StringContent("{}")
            };
            var fallback2 = Policy<HttpResponseMessage>
                .Handle<BulkheadRejectedException>()    //降级捕获的异常是被限流的异常
                .FallbackAsync(message);

            ////配置限流组合策略  限流降级 
            var fallbackbulk = Policy.WrapAsync(fallback2, bulk);
            services.AddHttpClient("httpv4").AddPolicyHandler(fallbackbulk);


            #region

            //reg.Add("circuitBreaker", HttpPolicyExtensions.HandleTransientHttpError()
            //    .AdvancedCircuitBreakerAsync(failureThreshold: 0.5,
            //    samplingDuration: TimeSpan.FromSeconds(5),
            //    minimumThroughput: 100,
            //    durationOfBreak: TimeSpan.FromSeconds(20),
            //    onBreak: (ex, state, ts, context) => { },
            //    onReset: context => { },
            //    onHalfOpen: () => { }));


            //HttpResponseMessage defaultMessage = new HttpResponseMessage(HttpStatusCode.OK);
            //defaultMessage.Content = new StringContent("{}");
            //var bcFallback = Policy<HttpResponseMessage>.Handle<BrokenCircuitException>().FallbackAsync<HttpResponseMessage>(defaultMessage);

            //var retry = Policy<HttpResponseMessage>.Handle<Exception>().WaitAndRetryAsync(3, t => TimeSpan.FromSeconds(t + 1));

            //var bc = Policy<HttpResponseMessage>.Handle<HttpRequestException>().AdvancedCircuitBreakerAsync(failureThreshold: 0.5,
            //    samplingDuration: TimeSpan.FromSeconds(5),
            //    minimumThroughput: 100,
            //    durationOfBreak: TimeSpan.FromSeconds(20),
            //    onBreak: (ex, state, ts, context) => { },
            //    onReset: context => { },
            //    onHalfOpen: () => { });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    //调用grpc服务
                    OrderGrpcClient service = context.RequestServices.GetService<OrderGrpcClient>();

                    try
                    {
                        
                        var r = service.CreateOrder(new CreateOrderCommand { BuyerId = "abc" });
                    }
                    catch (Exception ex)
                    {
                    }

                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
