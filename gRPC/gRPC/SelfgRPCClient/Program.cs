﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using SelfgRPCService.Protos;
using System;

namespace SelfgRPCClient
{
    class Program
    {
        static void Main(string[] args)
        {
            DoSomething();
            Console.ReadKey();
        }
        static async void DoSomething()
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var catClient = new LuCat.LuCatClient(channel);
            var catReply = await catClient.SuckingCatAsync(new Empty());
            Console.WriteLine("调用撸猫服务：" + catReply.Message);

        }
    }
}
