using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace StaticFileUnit
{
    /// <summary>
    /// 支持指定相对路径
    /// 支持目录浏览
    /// 支持设置默认文档
    /// 支持多目录映射
    /// 
    /// 
    /// 文件提供程序
    /// IFileProvider
    /// IFileInfo
    /// IDirectoryContent
    /// 内置文件提供程序
    /// PhysicalFileProvider
    /// EmbeddedFileProvider
    /// CompositeFileProvider
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
