﻿using Microsoft.Extensions.FileProviders;
using System;

namespace FileProvider
{
    /// 文件提供程序
    /// IFileProvider
    /// IFileInfo
    /// IDirectoryContent
    /// 内置文件提供程序
    /// PhysicalFileProvider
    /// EmbeddedFileProvider
    /// CompositeFileProvider
    /// 
    ///远程文件配置
    class Program
    {
        static void Main(string[] args)
        {
            IFileProvider provider1 = new PhysicalFileProvider(AppDomain.CurrentDomain.BaseDirectory);

            //var contents = provider1.GetDirectoryContents("/");
            //foreach (var item in contents)
            //{
            //    //读文件流
            //    var stream = item.CreateReadStream();
            //    Console.WriteLine(item.Name);
            //}
            //运行结果
            //FileProvider.deps.json
            //FileProvider.dll
            //FileProvider.exe
            //FileProvider.pdb
            //FileProvider.runtimeconfig.dev.json
            //FileProvider.runtimeconfig.json
            //Microsoft.Extensions.FileProviders.Abstractions.dll
            //Microsoft.Extensions.FileProviders.Physical.dll
            //Microsoft.Extensions.FileSystemGlobbing.dll
            //Microsoft.Extensions.Primitives.dll

            //读取内嵌资源文件  文件属性设置为内嵌资源
            IFileProvider provider2 = new EmbeddedFileProvider(typeof(Program).Assembly);
            var html = provider2.GetFileInfo("emb.html");


            //组合文件提供程序
            IFileProvider provider = new CompositeFileProvider(provider1, provider2);
            var contents = provider.GetDirectoryContents("/");
            foreach (var item in contents)
            {
                //读文件流
                //var stream = item.CreateReadStream();
                Console.WriteLine(item.Name);
            }


            //运行结果
            //emb.html
            //FileProvider.deps.json
            //FileProvider.dll
            //FileProvider.exe
            //FileProvider.pdb
            //FileProvider.runtimeconfig.dev.json
            //FileProvider.runtimeconfig.json
            //Microsoft.Extensions.FileProviders.Abstractions.dll
            //Microsoft.Extensions.FileProviders.Composite.dll
            //Microsoft.Extensions.FileProviders.Embedded.dll
            //Microsoft.Extensions.FileProviders.Physical.dll
            //Microsoft.Extensions.FileSystemGlobbing.dll
            //Microsoft.Extensions.Primitives.dll
            Console.ReadKey();
        }
    }
}
