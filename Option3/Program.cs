using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Option3
{
    /// <summary>
    /// 为我们的服务设计XXXOptions
    /// 使用 
    /// IOptions<XXXOptions>
    /// IOptionsSnapshot<XXXOptions>  作用域范围使用
    /// IOptionsMonitor<XXXOptions>   单例服务使用
    /// 作为服务构造函数的参数
    /// 
    /// 添加验证逻辑
    /// 1直接注册验证函数
    /// 2实现IValidateOptions<TOptions>
    /// 3使用Microsoft.Extensions.Options.DataAnnotations
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
