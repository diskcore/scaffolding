﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Option3.Services
{
    /// <summary>
    /// 服务的定义
    /// 
    /// </summary>
    public interface IOrderService
    {
        int ShowMaxOrderCount();
    }
    public class OrderService : IOrderService
    {
        /// <summary>
        /// 每个服务单独配置Options 服务之间的选项配置也不会互相依赖
        /// </summary>
        //IOptions<OrderServiceOptions> _options;
        //OrderService.ShowMaxOrderCount:200
        //OrderService.ShowMaxOrderCount:200

        //服务感知配置变化 ：每次请求都会重新计算读取配置
        //IOptionsSnapshot<OrderServiceOptions> _options;
        //运行结果
        //OrderService.ShowMaxOrderCount:200
        //OrderService.ShowMaxOrderCount:500

        //可以和单例配合使用可以读取到最新配置
        IOptionsMonitor<OrderServiceOptions> _options;
        //运行结果
        //OrderService.ShowMaxOrderCount:500
        //OrderService.ShowMaxOrderCount:600
        public OrderService(IOptionsMonitor<OrderServiceOptions> options)
        {
            _options = options;
            _options.OnChange(options =>
            {
                Console.WriteLine($"配置发生了变化，新值为{options.MaxOrderCount}"); 
            });
        }
        public int ShowMaxOrderCount()
        {
            //return _options.Value.MaxOrderCount;
            return _options.CurrentValue.MaxOrderCount;   //IOptionsMonitor
            //运行结果
            //Microsoft.Extensions.Options.OptionsValidationException
            //HResult = 0x80131500
            //Message = MaxOrderCount不能大于100
        }
    }
    public class OrderServiceOptions
    {
        /// <summary>
        /// 属性验证的方式
        /// </summary>
        [Range(1,20)]
        public int MaxOrderCount { get; set; } = 100;
    }
    public class OrderServiceValidateOptions : IValidateOptions<OrderServiceOptions>
    {
        public ValidateOptionsResult Validate(string name, OrderServiceOptions options)
        {
            if (options.MaxOrderCount > 100)
            {
                return ValidateOptionsResult.Fail("不能大于100");
            }
            else
            {
                return ValidateOptionsResult.Success;
            }
        }
    }
}
