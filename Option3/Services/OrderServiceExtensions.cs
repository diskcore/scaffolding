﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Option3.Services
{
    public static class OrderServiceExtensions
    {
        public static IServiceCollection AddOrderService(this IServiceCollection services,IConfiguration configuration)
        {
            //services.Configure<OrderServiceOptions>(configuration);


            //options验证
            //services.AddOptions<OrderServiceOptions>().Configure(options =>
            //{
            //    configuration.Bind(options);
            //}).Validate(options=> {
            //    return options.MaxOrderCount <=100;
            //}, "MaxOrderCount不能大于100");

            //属性验证
            //services.AddOptions<OrderServiceOptions>().Configure(options =>
            //{
            //    configuration.Bind(options);
            //}).ValidateDataAnnotations();


            //验证函数
            services.AddOptions<OrderServiceOptions>().Configure(options =>
            {
                configuration.Bind(options);
            }).Services.AddSingleton<IValidateOptions<OrderServiceOptions>,OrderServiceValidateOptions>();

            //读取完配置后操作
            services.PostConfigure<OrderServiceOptions>(options =>
            {
                options.MaxOrderCount += 100;
            });

            services.AddSingleton<IOrderService, OrderService>();  
            return services;
        }
    }
}
