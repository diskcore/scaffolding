using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Option3.Services;

namespace Option3
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //服务注册
            //services.AddSingleton<OrderServiceOptions>();
            //运行结果：OrderService.ShowMaxOrderCount:100
            //services.Configure<OrderServiceOptions>(Configuration.GetSection("OrderService"));
            //运行结果：OrderService.ShowMaxOrderCount:200
            //services.AddSingleton<IOrderService, OrderService>();  //不能和IOptionsSnapshot使用

            //静态扩展版
            services.AddOrderService(Configuration.GetSection("OrderService"));

           // services.AddScoped<IOrderService, OrderService>();
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
