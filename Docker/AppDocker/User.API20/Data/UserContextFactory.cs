﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.API.Data;

namespace User.API20.Data
{
    public class UserContextFactory : IDesignTimeDbContextFactory<UserContext>
    {
        // IConfiguration Configuration { get; } //使用Configuration 获取不到GetConnectionString("SchoolContext")。不能用
        //public UserContext CreateDbContext(string[] args)
        //{
        //    var optionsBuilder = new DbContextOptionsBuilder<UserContext>();
        //    //   optionsBuilder.UseSqlServer(Configuration.GetConnectionString("SchoolContext"));
        //    //optionsBuilder.UseSqlServer("Server = (localdb)\\mssqllocaldb; Database = SchoolContext; Trusted_Connection = True; MultipleActiveResultSets = true");
        //    return new UserContext(optionsBuilder.Options);
        //}

        public UserContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<UserContext>();
            // optionsBuilder.UseSqlServer(Configuration.GetConnectionString("SchoolContext"));
            optionsBuilder.UseMySQL("server=127.0.0.1;port=3306;database=beta_userapp;userid=root;password=pwd123456");
            return new UserContext(optionsBuilder.Options);
        }
    }
}
