﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace User.API
{
    /// <summary>
    /// https://www.cnblogs.com/weipengpeng/p/9522850.html
    /// Docker常用命令：https://shimo.im/docs/anrlYMFEYloN52c8
    /// docker run -d -p 3306:3306 -e MYSQL_USER="zebin" -e MYSQL_PASSWORD="pwd123456" -e MYSQL_ROOT_PASSWORD="pwd123456" --name mysql01 mysql/mysql-server:5.7 --character-set-server=utf8 --collation-server=utf8_general_ci
    /// GRANT ALL PRIVILEGES ON *.* TO 'zebin'@'%'
    /// dotnet 容器加速器：https://zhuanlan.zhihu.com/p/148792969?from_voters_page=true
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            //.UseUrls("http://+:80")
                .UseStartup<Startup>();
    }
}
