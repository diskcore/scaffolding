﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using User.API.Models;

namespace User.API.Data
{
    //数据库配置问题：https://www.it1352.com/1786317.html 
    public class UserContext:DbContext
    {
        private readonly DbContextOptions<UserContext> _options;
        private readonly IConfiguration _config;
        public UserContext(DbContextOptions<UserContext> options, IConfiguration config) :base(options)
        {
            _options = options;
            _config = config;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseMySql(_config.GetConnectionString("MysqlUser"));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppUser>().ToTable("Users")
                .HasKey(u => u.Id);

            base.OnModelCreating(modelBuilder); 
        }
        public DbSet<AppUser> Users { get; set; }
    }
}
