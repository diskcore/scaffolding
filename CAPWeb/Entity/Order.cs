﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CAPWeb.Entity
{
    public class Order
    {
        [Key]
        public Guid Id { get; set; }
        [MaxLength(20,ErrorMessage ="最长字符串不能超过20")]
        public string OrderName { get; set; }

    }
}
