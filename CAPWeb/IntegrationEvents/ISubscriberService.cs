﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CAPWeb.IntegrationEvents
{
    public interface ISubscriberService
    {
        void ReceivedShowTimeMessage(DateTime value);
    }
}
