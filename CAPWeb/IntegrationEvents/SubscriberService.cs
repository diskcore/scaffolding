﻿using CAPWeb.Data;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CAPWeb.IntegrationEvents
{
    public class SubscriberService : ISubscriberService, ICapSubscribe
    {
        [CapSubscribe("jiamiao.x.cap.demo.show.time")]
        //[CapSubscribe("jiamiao.x.cap.demo.show.time", Group = "SubscriberService")]
        public void ReceivedShowTimeMessage(DateTime value)
        {
            Console.WriteLine($"接受方：SubscriberService.ReceivedShowTimeMessage   接收到值：{value}");
            //throw new Exception("出错了");
        }
    }
}
