﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CAPWeb.Data;
using CAPWeb.Entity;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CAPWeb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly ICapPublisher _capPublisher;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, ICapPublisher capPublisher)
        {
            _logger = logger;
            _capPublisher = capPublisher;
        }

        [HttpGet]
        public /*async Task<IEnumerable<WeatherForecast>>*/ IEnumerable<WeatherForecast> Get([FromServices] OrderingContext dbContext)
        {
            //await _capPublisher.PublishAsync("jiamiao.x.cap.demo.show.time", DateTime.Now);
            using (var trans = dbContext.Database.BeginTransaction(_capPublisher/*, autoCommit: true*/))
            {
                //业务代码
                try
                {
                    var entity = new Order
                    {
                        Id = Guid.NewGuid(),
                        OrderName = "zhangxueyou"
                    };
                    dbContext.Orders.Add(entity);
                    dbContext.SaveChanges();
                    _capPublisher.Publish("jiamiao.x.cap.demo.show.time", DateTime.Now);
                    //只有Commit之后Publish才会被执行
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                   //throw;
                }
               
                
                //await _capPublisher.PublishAsync("jiamiao.x.cap.demo.show.time", DateTime.Now);
               
            }
            //return Ok();
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
