﻿using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CAPWeb.Controllers
{
    public class SubController : Controller
    {
        /// <summary>
        /// 控制器中
        /// CAP在默认情况下一个服务多个地方进行订阅，只会进行一次接收，除非进行分组
        /// </summary>
        /// <param name="value"></param>
        //[CapSubscribe("jiamiao.x.cap.demo.show.time")]
        [CapSubscribe("jiamiao.x.cap.demo.show.time", Group = "SubController")]
        public void ShowTime(DateTime value)
        {
            Console.WriteLine($"接受方：SubController.ShowTime   接收到值：{value}");
        }

    }
}
