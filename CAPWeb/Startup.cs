using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CAPWeb.Data;
using CAPWeb.IntegrationEvents;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CAPWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //在CAP之前
            services.AddScoped<ISubscriberService, SubscriberService>();
            //注入SQL上下文
            services.AddDbContext<OrderingContext>(options => {
                //EntityFrameworkCore.SqlServer扩展框架 能够数据库解耦
                options.UseSqlServer(Configuration["DbContext:ConnectionString"]);
                //options.UseMySql(Configuration["DbContext:ConnectionString"]);
            });

            //https://www.dimsum.fun/archives/netcore%E4%BD%BF%E7%94%A8cap
            //CAP
            services.AddCap(options =>
            {
                //options.UseSqlServer(@"Password=Pwd123456;Persist Security Info=True;User ID=sa;Initial Catalog=CapDemo;Data Source=LAPTOP-JDMA6KQD");
                options.UseEntityFramework<OrderingContext>();
                //@"Password=Pwd123456;Persist Security Info=True;User ID=sa;Initial Catalog=CapTransaction;Data Source=LAPTOP-JDMA6KQD"
                //Data Source=(localdb)\MSSQLLocalDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False
                options.UseRabbitMQ(options =>
                {
                    //使用MQ作为消息队列的介质
                    Configuration.GetSection("RabbitMQ").Bind(options);
                });
                //仪表盘：https://localhost:5001/cap
                options.UseDashboard();
            });
            
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
