﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Mobile.Gateway.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BankAccountController : Controller
    {
        /// <summary>
        /// 默认是Cookie 使用jwt是无效的
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IActionResult Cookie()
        {
            return Content("bank account");
        }

        /// <summary>
        /// 指定认证方式 JWT
        /// </summary>
        /// <returns></returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Jwt()
        {
            return Content(User.FindFirst("Name").Value);
        }

        /// <summary>
        /// 多种方式验证
        /// </summary>
        /// <returns></returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme + "," + CookieAuthenticationDefaults.AuthenticationScheme)]
        public IActionResult AnyOne()
        {
            return Content(User.FindFirst("Name").Value);
        }
    }
}