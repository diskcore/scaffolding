﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Abstractions
{
    /// <summary>
    /// 实体一般有一个ID
    /// </summary>
    public interface IEntity
    {
        object[] GetKeys();
    }
    /// <summary>
    /// 表示：实体只有一个ID     可以任意定义ID的类型
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface IEntity<TKey> : IEntity
    {
        TKey Id { get; }
    }
}
