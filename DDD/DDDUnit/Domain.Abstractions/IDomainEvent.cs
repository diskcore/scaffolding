﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
namespace Domain.Abstractions
{
    /// <summary>
    /// MediatR用于实现事件传递
    /// </summary>
    public interface IDomainEvent : INotification
    {
    }
}
