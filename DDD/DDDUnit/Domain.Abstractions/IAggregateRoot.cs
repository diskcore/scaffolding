﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Abstractions
{
    /// <summary>
    /// 聚合根接口
    /// 一个仓储对应一个聚合根
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
