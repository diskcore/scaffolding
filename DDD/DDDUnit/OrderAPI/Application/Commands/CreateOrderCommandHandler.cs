﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ordering.Infrastructure.Repositories;
using Ordering.Domain.OrderAggregate;
using DotNetCore.CAP;
using OrderAPI.Application.IntegrationEvents;

namespace OrderAPI.Application.Commands
{

    /// <summary>
    /// Api Controller 负责用户的输入输出
    /// 负责身份认证和授权
    /// 与领域服务指责区分开，不承载业务逻辑
    ///  注意：
    ///  IRequestHandler的Handle方法是由 _mediator.Send触发的
    ///  INotificationHandler的Handle方法是由 
    /// </summary>
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, long>

    {
        IOrderRepository _orderRepository;
        ICapPublisher _capPublisher;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderRepository"></param>
        /// <param name="capPublisher"></param>
        public CreateOrderCommandHandler(IOrderRepository orderRepository, ICapPublisher capPublisher)
        {
            _orderRepository = orderRepository;
            _capPublisher = capPublisher;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<long> Handle(CreateOrderCommand request, CancellationToken cancellationToken)
        {

            var address = new Address("wen san lu", "hangzhou", "310000");
            var order = new Order("xiaohong1999", "xiaohong", 25, address);

            _orderRepository.Add(order);
            await _orderRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            return order.Id;
        }
    }
}
