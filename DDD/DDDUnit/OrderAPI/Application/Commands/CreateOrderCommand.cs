﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace OrderAPI.Application.Commands
{
    /// <summary>
    /// 这里的泛型定义了返回值类型
    /// 和CreateOrderCommandHandler
    /// </summary>
    public class CreateOrderCommand : IRequest<long>
    {

        //ublic CreateOrderCommand() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemCount"></param>
        public CreateOrderCommand(int itemCount)
        {
            ItemCount = itemCount;
        }
        /// <summary>
        /// 
        /// </summary>
        public long ItemCount { get; private set; }
    }
}
