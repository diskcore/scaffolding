﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using Ordering.Domain.Events;
using DotNetCore.CAP;
using OrderAPI.Application.IntegrationEvents;
using Domain.Abstractions;
using OrderAPI.IntegrationEvents;

namespace OrderAPI.DomainEventHandlers
{
    /// <summary>
    /// 领域事件处理
    /// 事件定义和事件处理是分开的
    /// </summary>
    public class OrderCreatedDomainEventHandler : IDomainEventHandler<OrderCreatedDomainEvent>
    {
        ICapPublisher _capPublisher;
        /// <summary>
        /// ICapPublisher：将集成事件发送出去 发送到MQ或者Kafka
        /// </summary>
        /// <param name="capPublisher"></param>
        public OrderCreatedDomainEventHandler(ICapPublisher capPublisher)
        {
            _capPublisher = capPublisher;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task Handle(OrderCreatedDomainEvent notification, CancellationToken cancellationToken)
        {
            //将集成事件发布出去 OrderCreated
            await _capPublisher.PublishAsync("OrderCreated", new OrderCreatedIntegrationEvent(notification.Order.Id));
        }
    }
}
