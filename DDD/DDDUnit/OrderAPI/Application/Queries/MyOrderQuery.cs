﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
namespace OrderAPI.Application.Queries
{
    /// <summary>
    /// 
    /// </summary>
    public class MyOrderQuery : IRequest<List<string>>
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
    }
}
