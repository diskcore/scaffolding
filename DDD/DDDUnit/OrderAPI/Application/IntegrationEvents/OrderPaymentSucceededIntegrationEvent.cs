﻿namespace OrderAPI.Application.IntegrationEvents
{
    /// <summary>
    /// 集成事件
    /// </summary>
    public class OrderPaymentSucceededIntegrationEvent
    {
        public OrderPaymentSucceededIntegrationEvent(long orderId) => OrderId = orderId;
        public long OrderId { get; }
    }

}
