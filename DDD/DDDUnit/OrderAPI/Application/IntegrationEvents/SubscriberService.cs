﻿using DotNetCore.CAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using OrderAPI.IntegrationEvents;

namespace OrderAPI.Application.IntegrationEvents
{
    /// <summary>
    /// 订阅服务
    /// </summary>
    public class SubscriberService : ISubscriberService, ICapSubscribe
    {
        IMediator _mediator;
        public SubscriberService(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 订阅事件名：OrderPaymentSucceeded
        /// </summary>
        /// <param name="event"></param>
        [CapSubscribe("OrderPaymentSucceeded")]
        public void OrderPaymentSucceeded(OrderPaymentSucceededIntegrationEvent @event)
        {
            //Do SomeThing
        }

        /// <summary>
        /// 订单创建的领域：订阅事件
        /// </summary>
        /// <param name="event"></param>
        [CapSubscribe("OrderCreated")]
        public void OrderCreated(OrderCreatedIntegrationEvent @event)
        {

            //Do SomeThing
        }
    }
}
