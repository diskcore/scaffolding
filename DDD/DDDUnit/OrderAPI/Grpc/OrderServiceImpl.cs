﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using MediatR;
using Microsoft.Extensions.Logging;
using OrderAPI.Application.Commands;
using Ordering.API.Grpc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Grpc
{
    /// <summary>
    /// 服务实现
    /// </summary>
    public class OrderServiceImpl : OrderService.OrderServiceBase
    {
        IMediator _mediator;
        ILogger _logger;

        public OrderServiceImpl(IMediator mediator, ILogger<OrderServiceImpl> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        public override async Task<Int64Value> CreateOrder(Ordering.API.Grpc.CreateOrderCommand request, ServerCallContext context)
        {
            var cmd = new OrderAPI.Application.Commands.CreateOrderCommand((int)request.ItemCount);
            var r = await _mediator.Send(cmd);
            return new Int64Value { Value = r };
        }

        public override async Task<SearchResponse> Search(SearchRequest request, ServerCallContext context)
        {
            var query = new OrderAPI.Application.Queries.MyOrderQuery { UserName = request.UserName };
            var r = await _mediator.Send(query);
            var response = new SearchResponse();
            response.Orders.AddRange(r);
            return response;
        }
    }
}
