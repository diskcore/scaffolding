﻿
using Infrastructure.Core;
using Ordering.Domain.OrderAggregate;

namespace Ordering.Infrastructure.Repositories
{
    /// <summary>
    /// 仓储层的定义：可以定义特殊方法
    /// </summary>
    public interface IOrderRepository : IRepository<Order, long>
    {

    }
}
