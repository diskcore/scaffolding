﻿
using Infrastructure.Core;
using Ordering.Domain.OrderAggregate;

namespace Ordering.Infrastructure.Repositories
{
    /// <summary>
    /// 实现仓促通用方法：Repository<Order, long, OrderingContext>
    /// 实现仓促特殊方法：IOrderRepository
    /// </summary>
    public class OrderRepository :
         Repository<Order, long, OrderingContext>,
        IOrderRepository
    {
        public OrderRepository(OrderingContext context) : base(context)
        {
        }
    }
}
