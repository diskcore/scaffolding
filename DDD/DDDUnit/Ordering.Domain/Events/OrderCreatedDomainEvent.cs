﻿using Domain.Abstractions;
using Ordering.Domain.OrderAggregate;

namespace Ordering.Domain.Events
{
    /// <summary>
    /// 领域事件
    /// </summary>
    public class OrderCreatedDomainEvent : IDomainEvent
    {
        public Order Order { get; private set; }
        public OrderCreatedDomainEvent(Order order)
        {
            this.Order = order;
        }
    }
}
