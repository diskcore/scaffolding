﻿using Domain.Abstractions;
using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ordering.Domain.OrderAggregate
{
    /// <summary>
    /// 值对象
    /// </summary>
    public class Address : ValueObject
    {
        /// <summary>
        /// 符合封闭原则
        /// </summary>
        public string Street { get; private set; }
        public string City { get; private set; }
        public string ZipCode { get; private set; }

        public Address() { }
        public Address(string street, string city, string zipcode)
        {
            Street = street;
            City = city;
            ZipCode = zipcode;
        }
        /// <summary>
        /// 重载获取原子值的方法
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<object> GetAtomicValues()
        {
            // Using a yield return statement to return each element one at a time
            yield return Street;
            yield return City;
            yield return ZipCode;
        }
    }
}
