﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Core
{
    /// <summary>
    /// 工作单元：
    /// 使用同一上下文
    /// 跟踪实体的状态
    /// 保障事务一致性
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        //返回影响数据条数
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        //保存是否成功
        Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default);
    }
}
