﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Core
{
    /// <summary>
    /// 事务管理的接口
    /// </summary>
    public interface ITransaction
    {
        //获取当前事务
        IDbContextTransaction GetCurrentTransaction();

        bool HasActiveTransaction { get; }

        //开启事务
        Task<IDbContextTransaction> BeginTransactionAsync();
        //提交事务
        Task CommitTransactionAsync(IDbContextTransaction transaction);

        void RollbackTransaction();
    }
}
