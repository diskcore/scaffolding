﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityCore
{
    public class LoginController : BaseController<IdentityUser>
    {

        public LoginController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager) : base(userManager, signInManager)
        {
        }
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var cur = await GetUser(HttpContext.User);
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Register(string userName, string password)
        {
            var user = new IdentityUser { UserName = userName };
            var result = await CreateUser(user, password);

            if (result.Succeeded)
            {
                await SignInAsync(user, true);
                return new RedirectToActionResult("Index", "Login", null);
            }
            return BadRequest(result.Errors);
        }

        [HttpGet]
        public async Task<IActionResult> Sign(string userName, string password)
        {
            var res = await PasswordSignInAsync(userName, password, true, false);
            return View();
        }
    }
}
