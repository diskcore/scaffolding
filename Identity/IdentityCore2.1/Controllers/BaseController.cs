﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace IdentityCore
{
    public class BaseController<TUser> : Controller where TUser : class
    {

        protected UserManager<TUser> _userManager;
        protected SignInManager<TUser> _signInManager;

        public BaseController(UserManager<TUser> userManager, SignInManager<TUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public BaseController()
        {

        }

        //获取当前用户
        public virtual Task<TUser> GetUser(ClaimsPrincipal principal)
        {
            return _userManager.GetUserAsync(principal);

        }
        //用户注册
        public Task<IdentityResult> CreateUser(TUser user, string password)
        {
            return _userManager.CreateAsync(user, password);
        }
        //用户登录使用密码
        public virtual Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
        {
            return _signInManager.PasswordSignInAsync(userName, password, isPersistent, lockoutOnFailure);
        }



        //用户注册成功后登录有效
        public Task SignInAsync(TUser user, bool isPersistent)
        {
            return _signInManager.SignInAsync(user, isPersistent);
        }
    }
}
