using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Formatting.Compact;

namespace LogWeb
{
    /// <summary>
    /// 结构化日志
    /// 易于检索  易于统计
    /// 特定信息告警
    /// 上下文关联
    /// 实现追踪系统集成
    /// </summary>
    public class Program
    {
        /// <summary>
        /// 
        /// </summary>
        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", false, true)
            //.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIROMENT") ?? "Production"}.json", false, true)
            .AddEnvironmentVariables()
            .Build();
        public static int Main(string[] args)
        {
            //读取配置到Serilog
            //Serilog需要单独配置
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(Configuration)
                //.MinimumLevel.Debug()
                //.Enrich.FromLogContext()
                //输出到控制台
                .WriteTo.Console(new RenderedCompactJsonFormatter())
                //输出到文件
                .WriteTo.File(formatter: new CompactJsonFormatter(), "logs\\myapp.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            try
            {
                Log.Information("Starting web host");
                CreateHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }

        }

       //结构化日志
        //{"@t":"2020-08-31T14:50:12.3707784Z","@m":"Executing controller factory for controller \"LogWeb.Controllers.WeatherForecastController\" (\"LogWeb\")","@i":"9650b6ae","@l":"Debug","Controller":"LogWeb.Controllers.WeatherForecastController","AssemblyName":"LogWeb","EventId":{"Id":1,"Name":"ControllerFactoryExecuting"},"SourceContext":"Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker","ActionId":"3ee72098-9fc9-4eb0-8545-e8c4a1f28670","ActionName":"LogWeb.Controllers.WeatherForecastController.Get (LogWeb)","RequestId":"0HM2DNJV7T991:00000009","RequestPath":"/weatherforecast","SpanId":"|a31b775d-46a0b767b038727d.","TraceId":"a31b775d-46a0b767b038727d","ParentId":""}
        //{"@t":"2020-08-31T14:50:12.3720873Z","@m":"Executed controller factory for controller \"LogWeb.Controllers.WeatherForecastController\" (\"LogWeb\")","@i":"65418032","@l":"Debug","Controller":"LogWeb.Controllers.WeatherForecastController","AssemblyName":"LogWeb","EventId":{"Id":2,"Name":"ControllerFactoryExecuted"},"SourceContext":"Microsoft.AspNetCore.Mvc.Infrastructure.ControllerActionInvoker","ActionId":"3ee72098-9fc9-4eb0-8545-e8c4a1f28670","ActionName":"LogWeb.Controllers.WeatherForecastController.Get (LogWeb)","RequestId":"0HM2DNJV7T991:00000009","RequestPath":"/weatherforecast","SpanId":"|a31b775d-46a0b767b038727d.","TraceId":"a31b775d-46a0b767b038727d","ParentId":""}
        //{"@t":"2020-08-31T14:50:13.1969394Z","@m":"开始了","@i":"d8e439d1","SourceContext":"LogWeb.Controllers.WeatherForecastController","ActionId":"3ee72098-9fc9-4eb0-8545-e8c4a1f28670","ActionName":"LogWeb.Controllers.WeatherForecastController.Get (LogWeb)","RequestId":"0HM2DNJV7T991:00000009","RequestPath":"/weatherforecast","SpanId":"|a31b775d-46a0b767b038727d.","TraceId":"a31b775d-46a0b767b038727d","ParentId":""}
        //{"@t":"2020-08-31T14:50:22.3513528Z","@m":"结束了","@i":"2f662475","SourceContext":"LogWeb.Controllers.WeatherForecastController","ActionId":"3ee72098-9fc9-4eb0-8545-e8c4a1f28670","ActionName":"LogWeb.Controllers.WeatherForecastController.Get (LogWeb)","RequestId":"0HM2DNJV7T991:00000009","RequestPath":"/weatherforecast","SpanId":"|a31b775d-46a0b767b038727d.","TraceId":"a31b775d-46a0b767b038727d","ParentId":""}
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                 //注册Serilog 退出时 释放日志对象
                .UseSerilog(dispose:true);
    }
}
