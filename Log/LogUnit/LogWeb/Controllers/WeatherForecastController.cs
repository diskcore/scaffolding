﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LogWeb.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            _logger.LogInformation("开始了");
            _logger.LogInformation("结束了");
        //输出结果
        //info: LogWeb.Controllers.WeatherForecastController[0]
        // => RequestPath:/ weatherforecast RequestId: 0HM2C5S3NRLFN: 00000001, SpanId:| ced88019 - 413037f61be55666., TraceId: ced88019 - 413037f61be55666, ParentId: => LogWeb.Controllers.WeatherForecastController.Get(LogWeb)
        // 开始了
        //info: LogWeb.Controllers.WeatherForecastController[0]
        //=> RequestPath:/ weatherforecast RequestId: 0HM2C5S3NRLFN: 00000001, SpanId:| ced88019 - 413037f61be55666., TraceId: ced88019 - 413037f61be55666, ParentId: => LogWeb.Controllers.WeatherForecastController.Get(LogWeb)
        // 结束了
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
