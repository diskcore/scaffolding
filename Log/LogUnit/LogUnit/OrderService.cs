﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogUnit
{
    public class OrderService
    {
        ILogger<OrderService> _logger;
        public OrderService(ILogger<OrderService> logger)
        {
            _logger = logger;
        }
        public void Show()
        {
            //小技巧1：尽量使用模板的方式  如果没有输出的话  没有字符串拼接动作
            //小技巧2：不要记录敏感信息如用户密码
            _logger.LogInformation("Show Time {time}", DateTime.Now);
            _logger.LogInformation($"Show Time {DateTime.Now}" );
        }
    }
}
