﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics.CodeAnalysis;

namespace LogUnit
{
    /// <summary>
    /// Log4 Nuget
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            IConfigurationBuilder configBuilder = new ConfigurationBuilder();

            //命令行
            configBuilder.AddCommandLine(args);

            //Json文件
            configBuilder.AddJsonFile("appsettings.json", false, true);
            var config = configBuilder.Build();

            //容器
            IServiceCollection serviceCollection = new ServiceCollection();
            //工厂模式将配置对象注册到容器管理

            //管理生命周期
            serviceCollection.AddSingleton<IConfiguration>(p => config);
            //不管理生命周期
            //serviceCollection.AddSingleton<IConfiguration>(config);

            //日志框架：将日志记录到不同的地方
            serviceCollection.AddLogging(builder =>
            {
                builder.AddConfiguration(config.GetSection("Logging"));
                //控制台日志输出
                builder.AddConsole();

                //输出到调试信息框
                builder.AddDebug();
            });
            //serviceCollection.AddTransient<OrderService>();
            IServiceProvider service = serviceCollection.BuildServiceProvider();
            //var order = service.GetService<OrderService>();

            //order.Show();
            ////运行结果
            ////info: LogUnit.OrderService[0]
            ////Show Time 2020 / 8 / 29 22:47:47
            //ILoggerFactory loggerFactory = service.GetService<ILoggerFactory>();

            ////获取日志记录器
            //ILogger alogger = loggerFactory.CreateLogger("alogger");

            ////2001 事件ID
            //alogger.LogDebug(2001, "aiya");
            //alogger.LogInformation("hello");
            //var ex = new Exception("出错了");
            //alogger.LogError(ex, "出错了");
            //ILogger alogger2 = loggerFactory.CreateLogger("alogger");

            //alogger2.LogDebug("aiya");


            //日志作用域
            //一个事务包含多条操作时
            //复杂流程的日志关联时
            //调用链追踪与请求处理过程对应

            var logger = service.GetService<ILogger<Program>>();

            while (Console.ReadKey().Key!=ConsoleKey.Escape)
            {
                using (logger.BeginScope("ScopeID:{ScopeID}", Guid.NewGuid()))
                {
                    logger.LogInformation("这是Info");
                    logger.LogError("这是Eroor");
                    logger.LogTrace("这是Trace");
                    //运行结果
                    //info: LogUnit.Program[0]
                    //=> ScopeID: 32840968 - b2ad - 4e6c - 9808 - f0ce3d4264cf
                    //这是Info
                    //fail: LogUnit.Program[0]
                    // => ScopeID: 32840968 - b2ad - 4e6c - 9808 - f0ce3d4264cf
                    //这是Eroor
                    //trce: LogUnit.Program[0]
                    // => ScopeID: 32840968 - b2ad - 4e6c - 9808 - f0ce3d4264cf
                    //这是Trace
                }
                System.Threading.Thread.Sleep(100);
                Console.WriteLine("=============分割线==============");
            //运行结果 支持热更新
            //kinfo: LogUnit.Program[0]
            // => ScopeID: b7389699 - 816d - 4ce4 - bb75 - 1311207b7956
            // 这是Info
            //fail: LogUnit.Program[0]
            // => ScopeID: b7389699 - 816d - 4ce4 - bb75 - 1311207b7956
            // 这是Eroor
            // trce: LogUnit.Program[0]
            // => ScopeID: b7389699 - 816d - 4ce4 - bb75 - 1311207b7956
            // 这是Trace
            //=============分割线==============
            //kinfo: LogUnit.Program[0]
            //      => ScopeID:e0b31a1b-600c-4007-b427-5fd8ff5956a1
            //      这是Info
            //fail: LogUnit.Program[0]
            //      => ScopeID:e0b31a1b-600c-4007-b427-5fd8ff5956a1
            //      这是Eroor
            //trce: LogUnit.Program[0]
            //      => ScopeID:e0b31a1b-600c-4007-b427-5fd8ff5956a1
            //      这是Trace
            //=============分割线==============
              }
           

            Console.ReadKey();
        }
    }
}
