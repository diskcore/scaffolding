﻿using ConfigurationCustom;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;

namespace CustomConfig3
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            //builder.Add(new MyConfigurationSource());
            builder.AddMyConfiguration();
            var configRoot=builder.Build();
            ChangeToken.OnChange(() => configRoot.GetReloadToken(), () =>
            {
                Console.WriteLine($"lastTime:{configRoot["lastTime"]}");
            });

            Console.WriteLine($"lastTime:{configRoot["lastTime"]}");
            //Console.WriteLine("Hello World!");
            Console.ReadKey();
        }
    }
}
