﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;

namespace zhanzebin.Core
{
    /// <summary>
    /// 系统内置：
    /// Microsoft.Extensions.Configuration.Abstractions
    /// Microsoft.Extensions.Configuration
    /// 以key-value字符串键值对的方式抽象了配置
    /// 支持从不同的数据源读取数据
    /// IConfiguration
    /// IConfigurationRoot
    /// IConfigurationSection
    /// IConfigurationBuilder
    /// 扩展点：
    /// IConfigurationSource
    /// IConfigurationProvider
    /// 
    /// 环境变量配置
    /// 场景：Docker  Kubernetes ASP.NET Core 内置配置
    /// 特点：对于配置的分层键，支持用双下划线"__"代替":"  支持根据前缀加载
    /// 
    /// 文件读取
    /// Microsoft.Extensions.Configuration.Ini
    /// Microsoft.Extensions.Configuration.Json
    /// Microsoft.Extensions.Configuration..NewtonsoftJson
    /// Microsoft.Extensions.Configuration.Xml
    /// Microsoft.Extensions.Configuration.UserSecrets
    /// 
    /// 
    /// 监听配置的变化
    /// IChangeToken  IConfiguration  GetReloadToken()
    /// 
    /// 扩展配置框架
    /// 实现IConfigurationSource
    /// 实现IConfigurationProvider
    /// 实现AddXXX扩展方法
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            #region 环境变量配置
            //IConfigurationBuilder builder = new ConfigurationBuilder();
            //builder.AddEnvironmentVariables();
            //var configurationRoot = builder.Build();
            //Console.WriteLine($"key1:{configurationRoot["key1"]}");
            //运行结果
            //key1:value1

            #region 分层键
            //var section = configurationRoot.GetSection("SECTION1");
            //Console.WriteLine($"KEY3:{section["KEY3"]}");

            //var section2 = configurationRoot.GetSection("SECTION1:SECTION2");
            //Console.WriteLine($"KEY4:{section2["KEY4"]}");
            //运行结果：KEY3:value  KEY4: value4
            #region 前缀过滤
            //builder.AddEnvironmentVariables("XIAO_");
            //var configurationRoot = builder.Build();
            //Console.WriteLine($"key1:{configurationRoot["key1"]}");
            //Console.WriteLine($"key2:{configurationRoot["key2"]}");
            //运行结果
            //key1:xiao_key1
            //key2:
            #endregion

            #region 读取文件配置
            //读取Json文件
            IConfigurationBuilder builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json",true,true);
            //builder.AddIniFile("appsettings.ini");
            //后添加的配置优先级更高
            var configurationRoot = builder.Build();
            //强类型承载配置
            var config = new Config
            {
                Key1="config key1",
                Key5=false
                //Key6=100
            };
            configurationRoot.GetSection("OrderService").Bind(config,binderOptions=> {
                //私有字段不读取配置
                binderOptions.BindNonPublicProperties = false;
            //运行结果
            //Key1: order key1
            //Key5: True
            //Key6:0
            });
            Console.WriteLine($"Key1:{config.Key1}");
            Console.WriteLine($"Key5:{config.Key5}");
            Console.WriteLine($"Key6:{config.Key6}");


            // IChangeToken token = configurationRoot.GetReloadToken();

            //ChangeToken.OnChange(() => configurationRoot.GetReloadToken(), () => {
            //    Console.WriteLine($"Key1:{configurationRoot["Key1"]}");
            //    Console.WriteLine($"Key2:{configurationRoot["Key2"]}");
            //    Console.WriteLine($"Key3:{configurationRoot["Key3"]}");
            //    Console.WriteLine($"Key4:{configurationRoot["Key4"]}");
            //});
            //Console.WriteLine("开始了...");
            //token.RegisterChangeCallback(state => {
            //    Console.WriteLine($"Key1:{configurationRoot["Key1"]}");
            //    Console.WriteLine($"Key2:{configurationRoot["Key2"]}");
            //    Console.WriteLine($"Key3:{configurationRoot["Key3"]}");
            //    Console.WriteLine($"Key4:{configurationRoot["Key4"]}");
            //    token = configurationRoot.GetReloadToken();
            ////运行结果
            ////Key1:Value1
            ////Key2: Value2
            ////Key3:
            ////Key4:
            ////Key1: Value1
            ////Key2:Value2_new
            ////Key3:
            ////Key4:
            //}, configurationRoot);

            //Console.WriteLine($"Key1:{configurationRoot["Key1"]}");
            //Console.WriteLine($"Key2:{configurationRoot["Key2"]}");
            //Console.WriteLine($"Key3:{configurationRoot["Key3"]}");
            //Console.WriteLine($"Key4:{configurationRoot["Key4"]}");
            //运行结果
            //Key1: Value1
            //Key2:Value2
            //Key3:
            //Console.ReadKey();

            //Console.WriteLine($"Key1:{configurationRoot["Key1"]}");
            //Console.WriteLine($"Key2:{configurationRoot["Key2"]}");
            //Console.WriteLine($"Key3:{configurationRoot["Key3"]}");
            //Console.WriteLine($"Key4:{configurationRoot["Key4"]}");
            //Console.ReadKey();
            //运行结果
            //Key1: Value1
            //Key2:Value2
            //Key3:
            //Key1: Value1
            //Key2:Value2_new
            //Key3:
            #endregion


            #endregion
            #endregion

            //构建配置的核心
            //IConfigurationBuilder builder = new ConfigurationBuilder();

            //builder.AddCommandLine(args);
            //命令替换
            //var mapper=new Dictionary<string, string>{{ "-k1", "CommandLineKey1" } };
            //builder.AddCommandLine(args, mapper);
            //var configurationRoot = builder.Build();
            //Console.WriteLine($"CommandLineKey1:{configurationRoot["CommandLineKey1"]}");
            //Console.WriteLine($"CommandLineKey2:{configurationRoot["CommandLineKey2"]}");
            //Console.ReadKey();

            //builder.AddInMemoryCollection(new Dictionary<string, string>()
            //{
            //    { "key1" ,"value1"},
            //    { "key2" ,"value2"},
            //    { "section1:key4" ,"value4"},
            //    { "section2:key5" ,"value5"},
            //    { "section2:key6" ,"value6"},
            //   { "section2:section3:key7" ,"value7"}
            //});
            //IConfigurationRoot configurationRoot = builder.Build();

            //IConfiguration config = configurationRoot;

            //Console.WriteLine(configurationRoot["key1"]);
            //Console.WriteLine(configurationRoot["key2"]);

            ////一级嵌套 支
            //IConfigurationSection section1 = config.GetSection("section1");
            //Console.WriteLine(section1["key4"]);
            //IConfigurationSection section2 = config.GetSection("section2");
            //Console.WriteLine(section2["key5"]);
            //Console.WriteLine(section2["key6"]);
            //IConfigurationSection section3 = section2.GetSection("section3");
            //Console.WriteLine(section3["key7"]);
            //运行结果
            //value1
            //value2
            //value4
            //value5
            //value6
            //value7

            //命令行参数配置
            //无前缀 key=value
            //双中线 --key=value  或--key value
            //正斜杠模式 /key=value 或 /key value
            //等号和空格不能混用
            //命令替换模式
            //必须 -或--开头
            //映射字典不能包含重复key

            Console.ReadKey();
            //Dictionary<string, string> Dic = new Dictionary<string, string>();
            //ReadConfig readConfig = new ReadConfig();
            //Class myClass = new Class();
            //readConfig.ReadJsonConfig<Class>("class.json", myClass);
            //string res = readConfig.ReadJsonConfig("class.json", "ClassNo");
            //string res2 = readConfig.ReadJsonConfig("class.json", "ClassNo");
            //string res3 = readConfig.ReadJsonConfig("class.json", "ClassNo");
            //string name = readConfig.ReadCommandConfig(Dic, args, "name");
            //string res1 = readConfig.ReadJsonConfig("class1.json", "ClassNo");
            //readConfig
            //Console.WriteLine(res);
            //Console.WriteLine(name);
            //Console.WriteLine(res1);
            //Console.WriteLine(res2);
            //Console.WriteLine(res3);
            //Console.ReadLine();
            //Console.WriteLine("Hello World!");
        }
    }
    class Config
    {
        public string Key1 { get; set; }
        public bool  Key5 { get; set; }
        public int Key6 { get;private set; } = 1000;
    }
}
