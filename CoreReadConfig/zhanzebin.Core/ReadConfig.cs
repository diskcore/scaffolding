﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace zhanzebin.Core
{
    internal   class ReadConfig
    {
        ConfigurationBuilder configurationIns = new ConfigurationBuilder();
        
        //已经读取过的文件不再重复AddJsonFile
        List<string> fileNameCache = new List<string>();
        /// <summary>
        /// 读取Json文件的键
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public string ReadJsonConfig(string fileName,string key)
        {
            if (fileNameCache.Contains(fileName))
            {
                var configurationCache = configurationIns.Build();
                return configurationCache[key];
            }
            else
            {
                fileNameCache.Add(fileName);
                var builder = configurationIns.AddJsonFile(fileName);
                var configuration = builder.Build();
                return configuration[key];
            }
           
        }
        /// <summary>
        /// 实现配置文件Bind到实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <param name="obj"></param>
        public void ReadJsonConfig<T>(string fileName, T obj)
        {
            if (fileNameCache.Contains(fileName))
            {
                var configurationCache = configurationIns.Build();
                configurationCache.Bind(obj);
            }
            else
            {
                fileNameCache.Add(fileName);
                var builder = configurationIns.AddJsonFile(fileName);
                var configuration = builder.Build();
                configuration.Bind(obj);
            }

        }
        //读取命令行参数的值
        public string ReadCommandConfig(Dictionary<string,string> Dic,string[] args,string key)
        {
            var builder = configurationIns
           .AddInMemoryCollection(Dic)
           .AddCommandLine(args);
            var configuration = builder.Build();
            return configuration[key];
        }
    }
}
