﻿using Microsoft.Extensions.Configuration;
using System;

namespace ConfigurationCustom
{
    public class MyConfigurationSource : IConfigurationSource
    {
        public IConfigurationProvider Build(IConfigurationBuilder builder)
        {
            return new MyConfigurationProvider();
        }
    }
}
