﻿using Microsoft.Extensions.Configuration;
using System;
using System.Timers;

namespace ConfigurationCustom
{
    class MyConfigurationProvider : ConfigurationProvider
    {
        Timer timer;
        public MyConfigurationProvider():base()
        {
            //模拟配置变化
            timer = new Timer();
            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 3000;
            timer.Start();
        }
        private void Timer_Elapsed(object sender,ElapsedEventArgs e)
        {
            Load(true);
        }
        public override void Load()
        {
            //加载数据
            Load(false);
        }
        public void Load(bool reload)
        {
            this.Data["lastTime"] = DateTime.Now.ToString();
            if (reload)
            {
                base.OnReload();
            }
        }
        
     
    }
}
