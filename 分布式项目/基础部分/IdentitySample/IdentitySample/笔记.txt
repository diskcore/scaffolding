﻿1.手动命令行的方式
2.代码自动方式
dotnet ef migrations add InitialCreate            Add-Migration                         添加迁移
dotnet ef database update                         Update-Database                       更新数据库
dotnet ef migrations move                         Remove-Migration                      移除迁移
dotnet ef database update lastGoodMigration       Update-Database  lastGoodMigration    更新数据库
dotnet ef migrations script                       script-Migration                      生成脚本