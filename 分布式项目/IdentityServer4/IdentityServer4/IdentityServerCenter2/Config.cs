﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace IdentityServerCenter2
{
    public class Config
    {
        //添加用户
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
        {
            new TestUser
            {
                SubjectId = "1",
                Username = "alice",
                Password = "password"
            },
            new TestUser
            {
                SubjectId = "2",
                Username = "bob",
                Password = "password"
            }
        };
        }
        //资源
        public static IEnumerable<ApiResource> GetResource()
        {

            return new List<ApiResource> {
                new ApiResource("api","My Api")
            };
        }
        //客户端
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>{
                new Client()
                {
                    ClientId="client",
                    //客户端模式
                    AllowedGrantTypes=GrantTypes.ClientCredentials,
                    ClientSecrets={new Secret("secret".Sha256()) },
                    //可以访问的Resource
                    AllowedScopes={"api"}
                }, 
                new Client()
                {
                    ClientId="pwdClient",
                    //用户名密码模式
                    AllowedGrantTypes=GrantTypes.ResourceOwnerPassword,
                    ClientSecrets={new Secret("secret".Sha256()) },
                    RequireClientSecret=false,
                    //可以访问的Resource
                    AllowedScopes={"api"}
                }

            };
        }
    }
}
