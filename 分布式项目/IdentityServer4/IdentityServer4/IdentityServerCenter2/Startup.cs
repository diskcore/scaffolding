﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace IdentityServerCenter2
{
    //第一步：Nuget添加IdentityServer4
    //第二步：添加Startup配置
    //通过这个url可以访问:http://localhost:5000/.well-known/openid-configuration
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
              .AddIdentityServer()
              .AddDeveloperSigningCredential()
              .AddInMemoryApiResources(Config.GetResource())
              //客户端配置
              .AddInMemoryClients(Config.GetClients())
              //添加用户
              .AddTestUsers(Config.GetUsers());
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseIdentityServer();
            app.UseMvc();
        }
    }
}
