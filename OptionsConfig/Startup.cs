﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

namespace WebApplication1
{
    /// <summary>
    /// 系统内置：
    /// Microsoft.Extensions.Configuration.Abstractions
    /// Microsoft.Extensions.Configuration
    /// 以key-value字符串键值对的方式抽象了配置
    /// 支持从不同的数据源读取数据
    /// 
    /// </summary>
    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Class>(Configuration);
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMvcWithDefaultRoute();
            }

            //app.Run(async (context) =>
            //{
            //    var myClass = new Class();
            //    Configuration.Bind(myClass);
            //    //await context.Response.WriteAsync("Hello World!");
            //    await context.Response.WriteAsync($"{myClass.ClassDesc}");
            //});
        }
    }
}
