﻿using Acme.SimpleTaskApp.Tasks;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.SimpleTaskApp.Task.Dto
{
    public class GetAllTasksInput
    {
        public TaskState? State { get; set; }
    }
}
