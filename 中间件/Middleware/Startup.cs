using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Middleware.Exceptions;
using Newtonsoft.Json.Serialization;

namespace Middleware
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc(mvcOptions=> {

                //mvcOptions.Filters.Add<MyExceptionFilter>();
               // mvcOptions.Filters.Add<MyExceptionFilterAttribute>();
            });
        }

        /// <summary>
        /// 核心对象：IApplicationBuilder  RequestDelegate 处理对象:HttpContext
        /// IApplicationBuilder：注册中间件
        /// RequestDelegate
        /// 分为内置中间件：自定义中间件
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //app.Use(async (context, next) =>
            //{
            //    //await context.Response.WriteAsync("Hello");
            //    //后续的中间件执行
            //    await next();
            //    //await context.Response.WriteAsync("Hello2");
            //});

            //app.Map("/abc", abcBuilder =>
            //{
            //    abcBuilder.Use(async (context, next) =>
            //    {
            //        await next();
            //        await context.Response.WriteAsync("Hello2");
            //    });
            //});

            //高级Map
            //app.MapWhen(context =>
            //{
            //   return context.Request.Query.Keys.Contains("abc");
            //}, builder =>
            //{
            //    //中间件末端
            //    builder.Run(async context =>
            //    {
            //        await context.Response.WriteAsync("Hello3");
            //    });
            //});
            //异常捕获方式一
           // app.UseExceptionHandler("/error");


            //异常捕获方式二
            //app.UseExceptionHandler(errApp =>
            //{
            //    errApp.Run(async context =>
            //    {
            //        var exceptionHandlerPathFeatrue = context.Features.Get<IExceptionHandlerPathFeature>();
            //        var ex = exceptionHandlerPathFeatrue?.Error;
            //        var knowException = ex as IKnowException;
            //        if (knowException == null)
            //        {
            //            var logger = context.RequestServices.GetService<ILogger<MyExceptionFilterAttribute>>();
            //            logger.LogError(ex, ex.Message);
            //            knowException = KnowException.UnKnow;
            //            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            //        }
            //        else
            //        {
            //            knowException = KnowException.FromKnowException(knowException);
            //            context.Response.StatusCode = StatusCodes.Status200OK;
            //        }
            //        //var jsonOptions = context.RequestServices.GetService<IOptions<JsonSerializerOptions>>();
            //        context.Response.ContentType = "application/json";
            //        await context.Response.WriteAsync(System.Text.Json.JsonSerializer.Serialize(knowException));
            //    });
            //});

            //app.UseMyMiddleware();
            //越早注册权力越大
            app.UseHttpsRedirection();      

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
