﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Middleware.Exceptions
{
    public interface IKnowException
    {
        public string Message { get; }
        public int ErrorCode { get; }
        public object[] ErrorData { get; }
    }
}
