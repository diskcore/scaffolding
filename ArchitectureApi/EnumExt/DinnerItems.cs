﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnumExt
{
    [Flags]
    public enum DinnerItems
    {
        None = 0,
        Entree = 1,
        Appetizer = 2,
        Side = 4,
        Dessert = 8,
        Beverage = 16,
        BarBeverage = 32
    }

}
