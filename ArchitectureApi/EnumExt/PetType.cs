﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnumExt
{
    [Flags]
    public enum PetType
    {
        None = 0, 
        Dog = 1,
        Cat = 2,
        Rodent = 4, 
        Bird = 8, 
        Reptile = 16,
        Other = 32
    };
}
