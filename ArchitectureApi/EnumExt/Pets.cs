﻿using System;


namespace EnumExt
{
    [Flags]
    public enum Pets
    {
        None = 0,
        Dog = 1,
        Cat = 2,
        Bird = 4,
        Rabbit = 8,
        Other = 16
    }
}
