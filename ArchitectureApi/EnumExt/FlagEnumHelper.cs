﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnumExt
{
    public static  class FlagEnumHelper
    {
        public static DinnerItems AddEnumElement(this DinnerItems source, DinnerItems element)
        {
            source = source | element;
            return source;
        }

        public static DinnerItems RemoveEnumElement(this DinnerItems source, DinnerItems element)
        {
            source = source & ~element;
            return source;
        }
    }
}
