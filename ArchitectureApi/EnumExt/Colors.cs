﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnumExt
{
    [Flags]
    public enum Colors
    { 
        None = 0,
        Red = 1,
        Green = 2, 
        Blue = 4 
    }

}
