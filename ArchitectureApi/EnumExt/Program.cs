﻿using System;

namespace EnumExt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            #region Enum.HasFlag
            //DinnerItems myOrder = DinnerItems.Appetizer | DinnerItems.Entree |
            //             DinnerItems.Beverage;  // | DinnerItems.Dessert;

            //myOrder = myOrder.AddEnumElement(DinnerItems.Dessert);
            //myOrder = myOrder.RemoveEnumElement(DinnerItems.Dessert);

            //DinnerItems flagValue = DinnerItems.Entree | DinnerItems.Beverage;

            //Console.WriteLine("{0} includes {1}: {2}",
            //                  myOrder, flagValue, myOrder.HasFlag(flagValue));
            #endregion

            #region  Enum.Equal
            //Pets[] petsInFamilies = { Pets.None, Pets.Dog | Pets.Cat, Pets.Dog };
            //int familiesWithoutPets = 0;
            //int familiesWithDog = 0;

            //foreach (var petsInFamily in petsInFamilies)
            //{
            //    // Count families that have no pets.
            //    //The value equal to 0 cannot be used HasFlag method 
            //    //If the underlying value of flag is zero, the HasFlag method returns true
            //    if (petsInFamily.Equals(Pets.None))
            //        familiesWithoutPets++;
            //    // Of families with pets, count families that have a dog.
            //    else if (petsInFamily.HasFlag(Pets.Dog))
            //        familiesWithDog++;
            //}
            //Console.WriteLine("{0} of {1} families in the sample have no pets.",
            //                  familiesWithoutPets, petsInFamilies.Length);
            //Console.WriteLine("{0} of {1} families in the sample have a dog.",
            //                  familiesWithDog, petsInFamilies.Length);
            #endregion

            #region Enum.IsDefined
            //object value;

            //// Call IsDefined with underlying integral value of member.
            //value = 1;
            //Console.WriteLine("{0}: {1}", value, Enum.IsDefined(typeof(PetType), value));
            //// Call IsDefined with invalid underlying integral value.
            //value = 64;
            //Console.WriteLine("{0}: {1}", value, Enum.IsDefined(typeof(PetType), value));
            //// Call IsDefined with string containing member name.
            //value = "Rodent";
            //Console.WriteLine("{0}: {1}", value, Enum.IsDefined(typeof(PetType), value));
            //// Call IsDefined with a variable of type PetType.
            //value = PetType.Dog;
            //Console.WriteLine("{0}: {1}", value, Enum.IsDefined(typeof(PetType), value));
            //value = PetType.Dog | PetType.Cat;
            //Console.WriteLine("{0}: {1}", value, Enum.IsDefined(typeof(PetType), value));
            //// Call IsDefined with uppercase member name.
            //value = "None";
            //Console.WriteLine("{0}: {1}", value, Enum.IsDefined(typeof(PetType), value));
            //value = "NONE";
            //Console.WriteLine("{0}: {1}", value, Enum.IsDefined(typeof(PetType), value));
            //// Call IsDefined with combined value
            //value = PetType.Dog | PetType.Bird;
            //Console.WriteLine("{0:D}: {1}", value, Enum.IsDefined(typeof(PetType), value));
            //value = value.ToString();
            //Console.WriteLine("{0:D}: {1}", value, Enum.IsDefined(typeof(PetType), value));

            //f the constants in enumType define a set of bit fields and value contains the values, names, or underlying values of multiple bit fields, the IsDefined method returns false
            //Pets value = Pets.Dog | Pets.Cat;
            //Console.WriteLine("{0:D} Exists: {1}",
            //                  value, Pets.IsDefined(typeof(Pets), value));
            //string name = value.ToString();
            //Console.WriteLine("{0} Exists: {1}",
            //                  name, Pets.IsDefined(typeof(Pets), name));

            // The example displays the following output:
            //       3 Exists: False
            //       Dog, Cat Exists: False
            #endregion

            #region Enum.Parse

            string[] colorStrings = { "0", "2", "8", "blue", "Blue", "Yellow", "Red, Green" };
            foreach (string colorString in colorStrings)
            {
                try
                {
                    Colors colorValue = (Colors)Enum.Parse(typeof(Colors), colorString, true);
                    //Colors colorValue = (Colors)Enum.Parse<Colors>(colorString, true);
                    if (Enum.IsDefined(typeof(Colors), colorValue) | colorValue.ToString().Contains(","))
                        Console.WriteLine("Converted '{0}' to {1}.", colorString, colorValue.ToString());
                    else
                        Console.WriteLine("{0} is not an underlying value of the Colors enumeration.", colorString);
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("{0} is not a member of the Colors enumeration.", colorString);
                }
            }
            #endregion
            Console.ReadKey();
        }
    }
}
