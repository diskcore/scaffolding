﻿using RedisApp.分布式锁;
using System;
using System.Diagnostics;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;

namespace RedisApp
{
    //Nuget:StackExchange.Redis
    class Program
    {
        public static IGoodsStore goodsStore = null;
        public static GoodsRedisStore goodsRedisStore;
        public static object lockGoods = new object();
        static void Main(string[] args)
        {
            #region Redis常规功能
            //var a = RedisHelperNetCore.Default.StringSet("redisSingle", "redis" + DateTime.Now);
            //var b = RedisHelperNetCore.Default.StringGet("redisSingle");
            #endregion

            #region Redis发布订阅
            //RedisHelperNetCore.Default.Subscribe("RedisMessage", (channel, message) =>
            //{
            //    //输出收到的消息
            //    Console.WriteLine($"[{DateTime.Now:HH:mm:ss}] {message}");
            //});

            //RedisHelperNetCore.One.Publish("RedisMessage", "Redis消息推送");
            //Console.ReadKey();
            #endregion

            #region Redis入队
            //string queueName = "7003CD57-61F5-4B0F-A04F-2F665C1AF729";
            //for (int i = 0; i < 100000; i++)
            //{
            //    RedisHelperNetCore.Default.ListRightPush(queueName, Guid.NewGuid().ToString());
            //}
            //Console.ReadKey();
            #endregion

            #region Redis出队
            //string queueName = "7003CD57-61F5-4B0F-A04F-2F665C1AF729";
            //string queueValue = RedisHelperNetCore.Default.ListRightPop<string>(queueName);
            //while (!string.IsNullOrWhiteSpace(queueValue))
            //{
            //    Console.WriteLine(queueValue);
            //    queueValue = RedisHelperNetCore.Default.ListRightPop<string>(queueName);
            //}
            //Console.ReadKey();
            #endregion


            #region Redis哈希 设置
            //string hashKey = "zhanzebin";
            //RedisHelperNetCore.Default.HashSet(hashKey,
            //    "7003CD57-61F5-4B0F-A04F-2F665C1AF729",
            //    new
            //    {
            //        Name="zhanzebin",
            //        Age=18,
            //        Gender=true
            //    });

            //string hashKey = "zhanzebin";
            //RedisHelperNetCore.Default.HashSet(hashKey,
            //    "7003CD57-61F5-4B0F-A04F-2F665C1AF729",
            //    "测试数据一");
            ////设置过期时间
            //RedisHelperNetCore.Default.SetExpire(hashKey, DateTime.Now.AddDays(3));
            //Console.ReadKey();
            #endregion

            #region Redis哈希 获取
            //var res=RedisHelperNetCore.Default.HashExists(hashKey, "7003CD57-61F5-4B0F-A04F-2F665C1AF729");
            #endregion

            #region redis分布式锁
            //单线程情况下
            //goodsStore = new GoodsStore();
            //for (int i = 0; i < 505; i++)
            //{
            //    Order(i);
            //}
            //输出结果
            //顾客: 498 剩余：2
            //顾客: 499 剩余：1
            //顾客: 500 剩余：0
            //顾客：500 下单失败，库存不足
            //顾客:501 剩余：0
            //顾客：501 下单失败，库存不足
            //顾客:502 剩余：0
            //顾客：502 下单失败，库存不足
            //顾客:503 剩余：0
            //顾客：503 下单失败，库存不足
            //顾客:504 剩余：0
            //顾客：504 下单失败，库存不足

            //多线程
            //goodsStore = new GoodsStore();
            //for (int i = 1; i < 505; i++)
            //{
            //    int j = i;
            //    Task.Run(() =>
            //    {
            //        Order(j);
            //    });

            //}
            //输出结果
            //顾客: 504 剩余：7
            //顾客: 494 剩余：17
            //顾客: 495 剩余：16
            //顾客: 496 剩余：15
            //顾客: 497 剩余：14
            //顾客: 498 剩余：13
            //顾客: 499 剩余：12
            //顾客: 500 剩余：11
            //顾客: 501 剩余：10
            //顾客: 490 剩余：19
            //顾客: 503 剩余：8

            //升级方案一 Lock
            //输出结果
            //顾客: 496 剩余：4
            //顾客: 497 剩余：3
            //顾客: 498 剩余：2
            //顾客: 499 剩余：1
            //顾客: 500 剩余：0
            //顾客：500 下单失败，库存不足
            //顾客:501 剩余：0
            //顾客：501 下单失败，库存不足
            //顾客:502 剩余：0
            //顾客：502 下单失败，库存不足
            //顾客:503 剩余：0
            //顾客：503 下单失败，库存不足
            //顾客:504 剩余：0
            //顾客：504 下单失败，库存不足

            //升级二  ConcurrentDictionary 
            //输出结果
            //顾客: 499 剩余：1
            //顾客: 500 剩余：0
            //顾客: 501 剩余：-1
            //顾客: 502 剩余：-2
            //顾客：502 下单失败，库存不足
            //顾客:504 剩余：-4
            //顾客：504 下单失败，库存不足
            //顾客:503 剩余：-3
            //顾客：503 下单失败，库存不足
            //顾客：501 下单失败，库存不足

            //升级三 redis分布式锁
            goodsStore = new GoodsStore();
            goodsRedisStore = new GoodsRedisStore();
            var goodsA = goodsStore.GetGoodsA();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //Console.WriteLine($""}");
            Console.WriteLine($"商品总数{goodsA}");
            for (int i = 1; i < 805; i++)
            {
                int j = i;
                Task.Run(() =>
                {
                    //Order(j);
                    try
                    {
                        OrderRedisLock(j);
                    }
                    catch (Exception ex)
                    {

                        Console.WriteLine(ex);
                    }
                });
                #endregion
            }
            Thread.Sleep(2000);
            Console.WriteLine($"商品总数{goodsStore.GetGoodsA()}");
            Console.ReadKey();
        }
            //static void Order(int i)
            //{
            //    //lock (lockGoods)
            //    //{
            //    goodsStore.ReduceGoodsA(1);
            //    Console.WriteLine($"顾客:{i} 剩余：{count}");
            //    if (count < 0)
            //    {
            //        Console.WriteLine($"顾客：{i} 下单失败，库存不足");
            //    }
            //    //else
            //    //{
            //    //    goodsStore.GetAndReduceGoods(1);
            //    //}
            //    //}
            //}

            static void OrderRedisLock(int i)
            {
                try
                {
                    var isLock = goodsRedisStore.TryLock();
                    if (isLock)
                    {
                        var count = goodsStore.GetGoodsA();
                        Console.WriteLine($"顾客:{i} 剩余：{count}");
                        if (count <= 0)
                        {
                            Console.WriteLine($"顾客：{i} 下单失败，库存不足");
                        }
                        else
                        {
                            goodsStore.ReduceGoodsA(1);
                            goodsRedisStore.TryDelLokc();
                        }
                    }
                    else
                    {
                        Console.WriteLine("Lock Faild");
                    }
                }
                catch (Exception e)
                {
                goodsRedisStore.TryDelLokc();
            }
                finally
                {
                    //goodsRedisStore.TryDelLokc();
                }
            }
        }
    }

