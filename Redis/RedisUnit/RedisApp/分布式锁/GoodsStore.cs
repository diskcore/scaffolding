﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RedisApp.分布式锁
{
    public class GoodsStore : IGoodsStore
    {
        //private ConcurrentDictionary<string, int> goodsDic = new ConcurrentDictionary<string, int>();

        private  Dictionary<string, int> goodsDic = new Dictionary<string, int>();
        public GoodsStore()
        {
            goodsDic.Add("GoodsA", 20);
        }
        //public GoodsStore()
        //{
        //    goodsDic.TryAdd("GoodsA", 500);

        //}
        public int GetGoodsA()
        {
            return goodsDic["GoodsA"];
        }

        public void ReduceGoodsA(int count = 1)
        {
            goodsDic["GoodsA"] -= count;
        }
        //public int GetAndReduceGoods(int count = 1)
        //{
        //    return goodsDic.AddOrUpdate("GoodsA", count, (x, y) =>
        //    {
        //        return y - count;
        //    });
        //}
    }
}
