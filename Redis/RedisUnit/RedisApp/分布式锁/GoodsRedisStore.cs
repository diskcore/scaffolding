﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace RedisApp.分布式锁
{
    public  class GoodsRedisStore
    {
        private IDatabase Database;
        private static string GoodStoreKey = "Test_Goods_key";
        public GoodsRedisStore()
        {
            Database = RedisHelperNetCore.Default.CacheRedis;
            //Database.StringSet(GoodStoreKey, "500", TimeSpan.FromSeconds(10));
        }
        private static string LockKey = "LockKey";
        public bool TryLock(double time = 1)
        {
            return Database.StringSet(LockKey,
                Guid.NewGuid().ToString(),
                TimeSpan.FromSeconds(time),
                When.NotExists);
        }
        public bool TryDelLokc()
        {
            return Database.KeyDelete(LockKey);
        }

    }
}
