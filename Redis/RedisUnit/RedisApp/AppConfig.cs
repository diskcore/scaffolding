﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedisApp
{
    public class AppConfig
    {
        public static IConfigurationRoot Configuration { get; set; }
        // private static RedisOption _redisOptions;
        public static RedisOption RedisOption
        {
            get
            {
                ConfigurationBuilder configurationIns = new ConfigurationBuilder();
                var builder = configurationIns.AddJsonFile("appsetting.json");
                Configuration = builder.Build();
                string ReadConfig= Configuration["RedisConfig:RedisConnectionString"];
                return JsonConvert.DeserializeObject<RedisOption>(ReadConfig);
            }
        }
    }
}
