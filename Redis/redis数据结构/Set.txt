集合结构
sinter  sdiff sunion
特点：无序 无重复元素 集合间操作

命令 sadd srem
sadd key element
#向集合key中添加element（如果元素element已经存在，添加失败）

srem key element
#删除集合中的元素

scard sismember是否集合中的元素  srandmember随机取一个元素  smembers取集合所有元素

scard key 

sismember key element

srandmember key count

smembers key

抽奖系统
srandmember spop
用户的赞 踩 like

集合间API的使用 
sdiff key1 key2
#差集
sinter key1 key2
#交集
#sunion key1 key2
#并集

sdiff|sinter|sunion +store destkey 
#将差集 交集 并集的结果保存到destkey中
