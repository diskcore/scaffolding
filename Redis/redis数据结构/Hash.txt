哈希：特点 API hash vs string 查漏补缺

格式 key field value

特点 MapMap Small redis field不能相同

命令hget hset hdel

hget key field
#获取hash key 对应的field的value

hset key field value
#设置hash key对应的field的value

hdel key field
#删除hash key对应的field的value

命令hexists,hlen
hexists key field
#判断hash key 是否有 field

hlen key
#获取hash key field的数量 

命令 hmget hmset

hmget key field1 field2 field3    复杂度o(n)
#批量获取hash key的一批field对应的值

hmset key field1 value1 field2 value2  复杂度o(n)
#批量设置hash key的一批field value 

实战：
记录每个用户个人主页的访问量
hincryby  user:1:info pageview count

缓存视频的基本信息


命令：hgetall hvals hkeys
格式:hgetall key  小心使用 
#返回hash key 对应的所有field和value

格式：hvals key
#返回hash key 对应所有field的value

格式：hkeys key
#返回hash key 对应所有field

hash 和 string 对比
用户信息存储

******************过期时间只能针对一个key
命令：hsetnx，hincryby，hincrybyfloat
