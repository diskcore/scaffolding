﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace MongodbApp
{
    public class MongodbHelper
    {
        //Mongodb客户端
        MongoClient _mongoClient;
        public MongodbHelper(string conStr= "mongodb://localhost:27017")
        {
            _mongoClient = new MongoClient(conStr);
        } 

        /// <summary>
        /// 获取数据库
        /// 如果不存在就创建 存在就读取
        /// </summary>
        /// <param name="databaseName">数据库名称</param>
        /// <returns></returns>
        public IMongoDatabase getDatabase(string databaseName)
        {
            return _mongoClient.GetDatabase(databaseName);
        }



        //public void init()
        //{
        //    // To directly connect to a single MongoDB server
        //    // (this will not auto-discover the primary even if it's a member of a replica set)
        //    var client = new MongoClient();

        //    // or use a connection string
        //    var client = new MongoClient("mongodb://localhost:27017");

        //    // or, to connect to a replica set, with auto-discovery of the primary, supply a seed list of members
        //    var client = new MongoClient("mongodb://localhost:27017,localhost:27018,localhost:27019");
        //}
       

    }
}
