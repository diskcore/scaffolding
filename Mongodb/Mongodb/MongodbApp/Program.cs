﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;

namespace MongodbApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            #region Mongodb新增
            MongodbHelper mongodb = new MongodbHelper();
            var zzb = mongodb.getDatabase("zhanzebin");
            //{ name:"zhanzebin",age:15}
            var record = new Person { name="zhanzebin",age= 15};
            //cel.InsertOne(record);
            #endregion

            #region Mondb查询
            //Fileter用于过滤，如查询name = zhanzebin的第一条记录
            var filter = Builders<Person>.Filter;
            //添加记录
            var cel = zzb.GetCollection<Person>("celzhan");
            
            var data = cel.Find<Person>(filter.Empty).ToList();
            #endregion


          

        }

        [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
        public class Person
        {
            public ObjectId _id { get; set; }
            public string name { get; set; }
            public int age { get; set; }
        }
    }
}
