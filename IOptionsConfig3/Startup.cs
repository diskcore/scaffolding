using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace IOptionsConfig3
{
    public class Startup
    {
        /// <summary>
        /// 系统内置：
        /// Microsoft.Extensions.Configuration.Abstractions
        /// Microsoft.Extensions.Configuration
        /// 以key-value字符串键值对的方式抽象了配置
        /// 支持从不同的数据源读取数据
        /// IConfiguration
        /// IConfigurationRoot
        /// IConfigurationSection
        /// IConfigurationBuilder
        /// 扩展点：
        /// IConfigurationSource
        /// IConfigurationProvider
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
