using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FakeXieCheng.API.Database;
using FakeXieCheng.API.Dtos;
using FakeXieCheng.API.Models;
using FakeXieCheng.API.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;

namespace FakeXieCheng.API
{
    /// <summary>
    /// REST理论知识
    /// 无状态
    /// 面向资源 api/v1/touristRoutes 对   api/v1/GettouristRoutes 错 
    /// GET POST PUT PATCH DELETE  处理动作
    /// 增删改查好用 
    /// 面向过程 登录不好用
    /// REST6约束
    /// Client-Server约束：前后端数据分离
    /// 无状态：请求独立
    /// 分层系统：后端系统使用分层进行系统架构
    /// 统一接口约束：API前后端数据格式一致 自我发现的功能
    /// 可缓存约束：防止客户端频繁与服务端交互
    /// 按需代码：不重要 
    /// 
    /// HTTP请求的方法与资源交互
    /// </summary>
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //Identity框架
            services.AddIdentity<ApplicationUser, IdentityRole>()
               .AddEntityFrameworkStores<AppDbContext>();

            //JWT
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
               .AddJwtBearer(options =>
               {
                   var secretByte = Encoding.UTF8.GetBytes(Configuration["Authentication:SecretKey"]);
                   options.TokenValidationParameters = new TokenValidationParameters()
                   {
                       
                       ValidateIssuer = true,
                       ValidIssuer = Configuration["Authentication:Issuer"],

                       ValidateAudience = true,
                       ValidAudience = Configuration["Authentication:Audience"],

                       //验证是否过期
                       ValidateLifetime = true,

                       //验证私钥
                       IssuerSigningKey = new SymmetricSecurityKey(secretByte)
                   };
               });

           
            //内容协商机制
            services.AddControllers(setupAction =>
            {
                //忽略请求头的Accept 返回默认格式
                //setupAction.ReturnHttpNotAcceptable = false;
                ///Accept application/xml
                ///Accept  application/json
                ///Api还不能处理 application/xml
                ///返回406Not Acceptable
                setupAction.ReturnHttpNotAcceptable = true;
                //.NET Core 1.x 2.x的处理方式
                //setupAction.OutputFormatters.Add(
                //    new XmlDataContractSerializerOutputFormatter()
                //    );
                //.NET Core 3.x的处理方式
                //实现内容协商
            })
           
            .AddNewtonsoftJson(setupAction => {
                //配置JsonPatchDocument解析   必须添加该配置
                setupAction.SerializerSettings.ContractResolver =
                    new CamelCasePropertyNamesContractResolver();
            }).AddXmlDataContractSerializerFormatters()

            .ConfigureApiBehaviorOptions(setupAction =>
            {
                //验证失败不应该返回400 而应该是422
                /*
                自定义
                {
                    "type": "无所谓",
                    "title": "数据验证失败",
                    "status": 422,
                    "detail": "请看详细说明",
                    "instance": "/api/touristRoutes",
                    "traceId": "0HM3I8RQUN6A7:00000001",
                    "errors": {
                        "TouristRouteForCreationDto": [
                            "路线名称必须与路线描述不同"
                        ]
                    }
                }
                正常
                {
                    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
                    "title": "One or more validation errors occurred.",
                    "status": 400,
                    "traceId": "|7d48052f-442598453c392540.",
                    "errors": {
                        "TouristRouteForCreationDto": [
                            "路线名称必须与路线描述不同"
                        ]
                    }
                }
                 */
                //非法模型状态响应工厂
                setupAction.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetail = new ValidationProblemDetails(context.ModelState)
                    {
                        Type = "无所谓",
                        Title = "数据验证失败",
                        Status = StatusCodes.Status422UnprocessableEntity,
                        Detail = "请看详细说明",
                        Instance = context.HttpContext.Request.Path
                    };
                    problemDetail.Extensions.Add("traceId", context.HttpContext.TraceIdentifier);
                    return new UnprocessableEntityObjectResult(problemDetail)
                    {
                        ContentTypes = { "application/problem+json" }
                    };
                };
            }); ;

           


            //每次请求都实例化一个全新的仓库【数据仓库的数据互不影响】
            services.AddTransient<ITouristRouteRepository, TouristRouteRepository>();

            //注入SQL上下文
            services.AddDbContext<AppDbContext>(options => {
                //EntityFrameworkCore.SqlServer扩展框架 能够数据库解耦
                options.UseSqlServer(Configuration["DbContext:ConnectionString"]);
                //options.UseMySql(Configuration["DbContext:ConnectionString"]);
            });

            //AutoMapper
            // 扫描profile文件
            //AutoMapper.Extensions.Microsoft.DependencyInjection
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddHttpClient();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            //排序
            services.AddTransient<IPropertyMappingService, PropertyMappingService>();

            //services.Configure<MvcOptions>(config =>
            //{
            //    var outputFormatter = config.OutputFormatters
            //        .OfType<NewtonsoftJsonOutputFormatter>()?.FirstOrDefault();

            //    if (outputFormatter != null)
            //    {
            //        outputFormatter.SupportedMediaTypes
            //        .Add("application/vnd.aleks.hateoas+json");

            //        outputFormatter.SupportedMediaTypes
            //       .Add("application/xml");
            //    }
            //});
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // 你在哪？
            app.UseRouting();
            // 你是谁？
            app.UseAuthentication();
            // 你可以干什么？有什么权限？
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGet("/", async context =>
                //{
                //    await context.Response.WriteAsync("Hello World!");
                //});
                endpoints.MapControllers();
            });
        }
    }
}
