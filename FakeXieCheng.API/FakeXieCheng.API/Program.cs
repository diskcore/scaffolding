using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FakeXieCheng.API
{
    /// <summary>
    /// Docker操作
    /// 拉取mssql镜像
    /// docker pull microsoft/mssql-server-linux
    /// 运行镜像：docker run -e "ACCEPT_EULA=Y" -e 'SA_PASSWORD=PaSSword12!' -p 1433:1433 -d microsoft/mssql-server-linux
    /// "ConnectionString": "server=localhost; Database=FakeXiechengDb; User Id=sa; Password=PaSSword12!;"
    /// 
    /// 拉取Mysql镜像
    /// docker pull mysql:latest
    /// docker run -itd --name mysql-test -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql
    /// "ConnectionString": "server=localhost; Database=FakeXiechengDb; uid=root; pwd=123456;"
    /// 
    /// 
    /// EFCore
    /// add-migration initialMigration
    /// update-database
    /// </summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
