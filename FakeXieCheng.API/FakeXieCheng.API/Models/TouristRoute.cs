﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXieCheng.API.Models
{
    /// <summary>
    /// 旅游路线
    /// </summary>
    public class TouristRoute
    {
        [Key]
        public Guid Id { get; set; }
        //名称
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        //简介
        [Required]
        [MaxLength(1500)]
        public string Description { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        //原价
        public decimal OriginalPrice { get; set; }
        //折扣
        [Range(0.0, 1.0)]
        public double? DiscountPresent { get; set; }
        //发布时间
        public DateTime CreateTime { get; set; }
        //更新时间
        public DateTime? UpdateTime { get; set; }
        //发团时间
        public DateTime? DepartureTime { get; set; }
        //卖点介绍
        [MaxLength]
        public string Features { get; set; }
        //费用说明
        [MaxLength]
        public string Fees { get; set; }
        //备注
        [MaxLength]
        public string Notes { get; set; }
        //记得使用复数
        public ICollection<TouristRoutePicture> TouristRoutePictures { get; set; } = new List<TouristRoutePicture>();
        //评分
        public double? Rating { get; set; }
        //旅行天数
        public TravelDays? TravelDays { get; set; }
        //旅游类型
        public TripType? TripType { get; set; }
        //出发地
        public DepartureCity? DepartureCity { get; set; }
    }
}
