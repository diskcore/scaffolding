﻿using FakeXieCheng.API.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXieCheng.API.Dtos
{
    //类级别校验
    //[TouristRouteTitleMustBeDifferentFromDescription]
    //测试数据
    /*
     {
        "title": "180度海景/网红康年/希尔顿国际品牌/网红哈曼 酒店精选",
         "description": "180度海景/网红康年/希尔顿国际品牌/网红哈曼 酒店精选",
         "originalPrice": 5998.9,
         "discountPercent": 0.9
      }
     */
    public class TouristRouteForCreationDto: TouristRouteForManipulationDto
    {
        //[Required(ErrorMessage = "Title 不可为空")]
        //原始返回结果 Status Code 400  配合 [ApiController] 才能实现验证 否则不进行验证
        /*
        {
            "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
            "title": "One or more validation errors occurred.",
            "status": 400,
            "traceId": "|d822bbbe-45504f65552d33e6.",
            "errors": {
                "Title": [
                    "Title 不可为空"
                ]
             }
        }
         */
        //[MaxLength(100)]
        //public string Title { get; set; }
        //[Required]
        //[MaxLength(1500)]
        //public string Description { get; set; }
        //public decimal Price { get; set; }
        ////public decimal OriginalPrice { get; set; }
        ////public double? DiscountPresent { get; set; }
        //public DateTime CreateTime { get; set; }
        //public DateTime? UpdateTime { get; set; }
        //public DateTime? DepartureTime { get; set; }
        //public string Features { get; set; }
        //public string Fees { get; set; }
        //public string Notes { get; set; }
        //public double? Rating { get; set; }
        //public string TravelDays { get; set; }
        //public string TripType { get; set; }
        //public string DepartureCity { get; set; }
        //public ICollection<TouristRoutePictureForCreationDto> TouristRoutePictures { get; set; }
        //    = new List<TouristRoutePictureForCreationDto>();
    }
}
