﻿using AutoMapper;
using FakeXieCheng.API.Dtos;
using FakeXieCheng.API.Models;
using FakeXieCheng.API.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXieCheng.API.Controllers
{
    [Route("api/touristRoutes/{touristRouteId}/pictures")]
    [ApiController]
    public class TouristRoutePicturesController : ControllerBase
    {
        private ITouristRouteRepository _touristRouteRepository;
        private IMapper _mapper;

        public TouristRoutePicturesController(
            ITouristRouteRepository touristRouteRepository,
            IMapper mapper
        )
        {
            _touristRouteRepository = touristRouteRepository ??
                throw new ArgumentNullException(nameof(touristRouteRepository));
            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// 获取嵌套对象关系型数据
        /// URL:https://localhost:5001/api/TouristRoutes/fb6d4f10-79ed-4aff-a915-4ce29dc9c7e1/pictures
        /// </summary>
        /// <param name="touristRouteId"></param>
        /// <returns></returns>
        [HttpGet(Name = "GetPictureListForTouristRoute")]
        public async Task<IActionResult> GetPictureListForTouristRoute(Guid touristRouteId)
        {
            if (!await _touristRouteRepository.TouristRouteExistsAsync(touristRouteId))
            {
                return NotFound("旅游线路不存在");
            }

            var picturesFromRepo =await  _touristRouteRepository.GetPicturesByTouristRouteIdAsync(touristRouteId);
            if (picturesFromRepo == null || picturesFromRepo.Count() <= 0)
            {
                return NotFound("照片不存在");
            }

            return Ok(_mapper.Map<IEnumerable<TouristRoutePictureDto>>(picturesFromRepo));

        }

        /// <summary>
        /// RESTFUL 先获取父资源再获取子资源
        /// URL:https://localhost:5001/api/TouristRoutes/fb6d4f10-79ed-4aff-a915-4ce29dc9c7e1/pictures/1
        /// </summary>
        /// <param name="touristRouteId"></param>
        /// <param name="pictureId"></param>
        /// <returns></returns>
        [HttpGet("{pictureId}", Name = "GetPicture")]
        public async Task<IActionResult> GetPicture(Guid touristRouteId, int pictureId)
        {
            if (!await _touristRouteRepository.TouristRouteExistsAsync(touristRouteId))
            {
                return NotFound("旅游线路不存在");
            }

            var pictureFromRepo =await  _touristRouteRepository.GetPictureAsync(pictureId);
            if (pictureFromRepo == null)
            {
                return NotFound("相片不存在");
            }
            return Ok(_mapper.Map<TouristRoutePictureDto>(pictureFromRepo));
        }
        /// <summary>
        /// 创建旅游路线图片
        /// URL https://localhost:5001/api/TouristRoutes/39996f34-013c-4fc6-b1b3-0c1036c47115/pictures
        /// Body
        /// {"url": "../../assets/images/osaka-castle-1398116_640.jpg"}
        ///
        ///
        /// </summary>
        /// <param name="touristRouteId"></param>
        /// <param name="touristRoutePictureForCreationDto"></param>
        /// <returns></returns>
        [HttpPost(Name = "CreateTouristRoutePicture")]
        public async Task<IActionResult> CreateTouristRoutePicture(
           [FromRoute] Guid touristRouteId,
           [FromBody] TouristRoutePictureForCreationDto touristRoutePictureForCreationDto
       )
        {
            if (!await _touristRouteRepository.TouristRouteExistsAsync(touristRouteId))
            {
                return NotFound("旅游线路不存在");
            }

            var pictureModel = _mapper.Map<TouristRoutePicture>(touristRoutePictureForCreationDto);
            _touristRouteRepository.AddTouristRoutePicture(touristRouteId, pictureModel);
            await  _touristRouteRepository.SaveAsync();
            var pictureToReturn = _mapper.Map<TouristRoutePictureDto>(pictureModel);
            return CreatedAtRoute(
                "GetPicture",
                new
                {
                    touristRouteId = pictureModel.TouristRouteId,
                    pictureId = pictureModel.Id
                },
                pictureToReturn
            );
            //注意不要漏掉： [HttpGet("{pictureId}", Name = "GetPicture")]
        }

        /// <summary>
        /// 删除旅游路线子资源
        /// URL:https://localhost:3001/api/touristRoutes/39996f34-013c-4fc6-b1b3-0c1036c47111/pictures/32
        /// </summary>
        /// <param name="touristRouteId"></param>
        /// <param name="pictureId"></param>
        /// <returns></returns>
        [HttpDelete("{pictureId}")]
        public async Task<IActionResult> DeletePicture(
          [FromRoute] Guid touristRouteId,
          [FromRoute] int pictureId
      )
        {
            if (!await _touristRouteRepository.TouristRouteExistsAsync(touristRouteId))
            {
                return NotFound("旅游线路不存在");
            }

            var picture =await  _touristRouteRepository.GetPictureAsync(pictureId);
            _touristRouteRepository.DeleteTouristRoutePicture(picture);
            await _touristRouteRepository.SaveAsync();

            return NoContent();
        }

    }
}
