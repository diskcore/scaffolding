﻿using AutoMapper;
using FakeXieCheng.API.Dtos;
using FakeXieCheng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXieCheng.API.Profiles
{
    public class TouristRouteProfile : Profile
    {
        public TouristRouteProfile()
        {
            //输入：TouristRoute   输出：TouristRouteDto
            CreateMap<TouristRoute, TouristRouteDto>()
                .ForMember(
                    dest => dest.Price,
                    opt => opt.MapFrom(src => src.OriginalPrice * (decimal)(src.DiscountPresent ?? 1))
                )
                ///枚举类型转为字符串
                /// "rating": 3.5,
                ///"travelDays": "EightPlus",
                ///"tripType": "HotelAndAttractions",
                ///"departureCity": "Beijing"
                .ForMember(
                    dest => dest.TravelDays,
                    opt => opt.MapFrom(src => src.TravelDays.ToString())
                )
                .ForMember(
                    dest => dest.TripType,
                    opt => opt.MapFrom(src => src.TripType.ToString())
                )
                .ForMember(
                    dest => dest.DepartureCity,
                    opt => opt.MapFrom(src => src.DepartureCity.ToString())
                );

            //创建旅游路线Dto
            //输入：TouristRouteForCreationDto   输出：TouristRoute
            CreateMap<TouristRouteForCreationDto, TouristRoute>()
              .ForMember(
                  dest => dest.Id,
                  opt => opt.MapFrom(src => Guid.NewGuid())
              );

            //修改旅游路线Dto
            //输入：TouristRouteForUpdateDto   输出：TouristRoute
            CreateMap<TouristRouteForUpdateDto, TouristRoute>();

            CreateMap<TouristRoute, TouristRouteForUpdateDto>();
        }
    }
}
