﻿using FakeXiecheng.API.Helper;
using FakeXieCheng.API.Database;
using FakeXieCheng.API.Dtos;
using FakeXieCheng.API.Helper;
using FakeXieCheng.API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXieCheng.API.Services
{

    public class TouristRouteRepository : ITouristRouteRepository
    {
        private readonly AppDbContext _context;
        private readonly IPropertyMappingService _propertyMappingService;

        public TouristRouteRepository(
            AppDbContext appDbContext,
            IPropertyMappingService propertyMappingService
        )
        {
            _context = appDbContext;
            _propertyMappingService = propertyMappingService;
        }

        public void AddTouristRoute(TouristRoute touristRoute)
        {
            if (touristRoute == null)
            {
                throw new ArgumentNullException(nameof(touristRoute));
            }
            _context.TouristRoutes.Add(touristRoute);
        }
        /// <summary>
        /// 创建旅游路线图片
        /// </summary>
        /// <param name="touristRouteId"></param>
        /// <param name="touristRoutePicture"></param>
        public void AddTouristRoutePicture(Guid touristRouteId, TouristRoutePicture touristRoutePicture)
        {
            if (touristRouteId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(touristRouteId));
            }
            if (touristRoutePicture == null)
            {
                throw new ArgumentNullException(nameof(touristRoutePicture));
            }
            //外键自动关联主表
            touristRoutePicture.TouristRouteId = touristRouteId;
            _context.TouristRoutePictures.Add(touristRoutePicture);
        }

        //public TouristRoutePicture GetPicture(int pictureId)
        //{
        //    return _context.TouristRoutePictures.Where(p => p.Id == pictureId).FirstOrDefault();
        //     // return _context.TouristRoutes.Include(t => t.TouristRoutePictures).FirstOrDefault(n => n.Id == touristRouteId);
        //}
        public async Task<TouristRoutePicture> GetPictureAsync(int pictureId)
        {
            return await _context.TouristRoutePictures.Where(p => p.Id == pictureId).FirstOrDefaultAsync();
        }


        public async Task<TouristRoute> GetTouristRouteAsync(Guid touristRouteId)
        {
            return await _context.TouristRoutes.Include(t => t.TouristRoutePictures).FirstOrDefaultAsync(n => n.Id == touristRouteId);
        }

        //public IEnumerable<TouristRoutePicture> GetPicturesByTouristRouteId(Guid touristRouteId)
        //{
        //    //return _context.TouristRoutePictures.Include(x => x.TouristRoute)
        //    //    .Where(p => p.TouristRouteId == touristRouteId).ToList();

        //    return _context.TouristRoutePictures
        //       .Where(p => p.TouristRouteId == touristRouteId).ToList();
        //}

        public async Task<IEnumerable<TouristRoutePicture>> GetPicturesByTouristRouteIdAsync(Guid touristRouteId)
        {
            return await _context.TouristRoutePictures
                .Where(p => p.TouristRouteId == touristRouteId).ToListAsync();
        }

        public TouristRoute GetTouristRoute(Guid touristRouteId)
        {
            //return _context.TouristRoutes.FirstOrDefault(n => n.Id == touristRouteId);
            return _context.TouristRoutes.Include(t => t.TouristRoutePictures).FirstOrDefault(n => n.Id == touristRouteId);
        }

        //public IEnumerable<TouristRoute> GetTouristRoutes(string keyword,
        //    string ratingOperator,
        //    int? ratingValue)
        //{
        //    //return _context.TouristRoutes;
        //    //return _context.TouristRoutes.Include(t => t.TouristRoutePictures);
        //    //Linq to SQL的核心对象：IQueryable
        //    ///IQueryable：延迟执行 目的为后续动态表达提供可能
        //    ///聚合操作：ToList(),SingleOrDefault(),Count() 真正执行访问数据
        //    ///生成了SQL语句还没有执行
        //    IQueryable<TouristRoute> result = _context
        //       .TouristRoutes
        //       .Include(t => t.TouristRoutePictures);
        //    if (!string.IsNullOrWhiteSpace(keyword))
        //    {
        //        keyword = keyword.Trim();
        //        result = result.Where(t => t.Title.Contains(keyword));
        //    }
        //    if (ratingValue >= 0)
        //    {
        //        result = ratingOperator switch
        //        {
        //            "largerThan" => result.Where(t => t.Rating >= ratingValue),
        //            "lessThan" => result.Where(t => t.Rating <= ratingValue),
        //             _ => result.Where(t => t.Rating == ratingValue),
        //        };
        //    }
        //    // include vs join
        //    //ToList执行数据库的访问
        //    return result.ToList();
        //}

        // public async Task<IEnumerable<TouristRoute>> GetTouristRoutesAsync(
        //    string keyword,
        //    string ratingOperator,
        //    int? ratingValue
        //)
        // {
        //     IQueryable<TouristRoute> result = _context
        //         .TouristRoutes
        //         .Include(t => t.TouristRoutePictures);
        //     if (!string.IsNullOrWhiteSpace(keyword))
        //     {
        //         keyword = keyword.Trim();
        //         result = result.Where(t => t.Title.Contains(keyword));
        //     }
        //     if (ratingValue >= 0)
        //     {
        //         result = ratingOperator switch
        //         {
        //             "largerThan" => result.Where(t => t.Rating >= ratingValue),
        //             "lessThan" => result.Where(t => t.Rating <= ratingValue),
        //             _ => result.Where(t => t.Rating == ratingValue),
        //         };
        //     }
        //     // include vs join
        //     return await result.ToListAsync();
        // }



        public async Task<PaginationList<TouristRoute>> GetTouristRoutesAsync(
            string keyword,
            string ratingOperator,
            int? ratingValue,
            int pageSize,
            int pageNumber,
            string orderBy
        )
        {
            IQueryable<TouristRoute> result = _context
                .TouristRoutes
                .Include(t => t.TouristRoutePictures);
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                keyword = keyword.Trim();
                result = result.Where(t => t.Title.Contains(keyword));
            }
            if (ratingValue >= 0)
            {
                result = ratingOperator switch
                {
                    "largerThan" => result.Where(t => t.Rating >= ratingValue),
                    "lessThan" => result.Where(t => t.Rating <= ratingValue),
                    _ => result.Where(t => t.Rating == ratingValue),
                };
            }
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                //if (orderBy.ToLowerInvariant() == "originalprice")
                //{
                //    result = result.OrderBy(t => t.OriginalPrice);
                //}
                var touristRouteMappingDictionary = _propertyMappingService
                   .GetPropertyMapping<TouristRouteDto, TouristRoute>();

                result = result.ApplySort(orderBy, touristRouteMappingDictionary);
                //result.ApplySort(orderBy, _mappingDictionary);
            }

            // include vs join
            return await PaginationList<TouristRoute>.CreateAsync(pageNumber, pageSize, result);
        }

        //public bool Save()
        //{
        //    return (_context.SaveChanges() >= 0);
        //}
        public async Task<bool> SaveAsync()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }
        //public bool TouristRouteExists(Guid touristRouteId)
        //{
        //    return _context.TouristRoutes.Any(t => t.Id == touristRouteId);
        //}

        public async Task<bool> TouristRouteExistsAsync(Guid touristRouteId)
        {
            return await _context.TouristRoutes.AnyAsync(t => t.Id == touristRouteId);
        }
        public void DeleteTouristRoute(TouristRoute touristRoute)
        {
            _context.TouristRoutes.Remove(touristRoute);
        }
        public void DeleteTouristRoutePicture(TouristRoutePicture picture)
        {
            _context.TouristRoutePictures.Remove(picture);
        }


        //public IEnumerable<TouristRoute> GetTouristRoutesByIDList(IEnumerable<Guid> ids)
        //{
        //    return _context.TouristRoutes.Where(t => ids.Contains(t.Id)).ToList();
        //}
        public async Task<IEnumerable<TouristRoute>> GetTouristRoutesByIDListAsync(IEnumerable<Guid> ids)
        {
            return await _context.TouristRoutes.Where(t => ids.Contains(t.Id)).ToListAsync();
        }



        public void DeleteTouristRoutes(IEnumerable<TouristRoute> touristRoutes)
        {
            _context.TouristRoutes.RemoveRange(touristRoutes);
        }

        public async Task<ShoppingCart> GetShoppingCartByUserId(string userId)
        {
            return await _context.ShoppingCarts
                .Include(s => s.User)
                .Include(s => s.ShoppingCartItems).ThenInclude(li => li.TouristRoute)
                .Where(s => s.UserId == userId)
                .FirstOrDefaultAsync();
        }

        public async Task CreateShoppingCart(ShoppingCart shoppingCart)
        {
            await _context.ShoppingCarts.AddAsync(shoppingCart);
        }
        public async Task AddShoppingCartItem(LineItem lineItem)
        {
            await _context.LineItems.AddAsync(lineItem);
        }
        //删除商品
        public void DeleteShoppingCartItem(LineItem lineItem)
        {
            _context.LineItems.Remove(lineItem);
        }
        public async Task<LineItem> GetShoppingCartItemByItemId(int lineItemId)
        {
            return await _context.LineItems
                .Where(li => li.Id == lineItemId)
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<LineItem>> GeshoppingCartsByIdListAsync(
           IEnumerable<int> ids)
        {
            return await _context.LineItems
                .Where(li => ids.Contains(li.Id))
                .ToListAsync();
        }
        public void DeleteShoppingCartItems(IEnumerable<LineItem> lineItems)
        {
            _context.LineItems.RemoveRange(lineItems);
        }
        public async Task AddOrderAsync(Order order)
        {
            await _context.Orders.AddAsync(order);
        }
        //public async Task<IEnumerable<Order>> GetOrdersByUserId(string userId)
        //{
        //    return await _context.Orders.Where(o => o.UserId == userId).ToListAsync();
        //}
        public async Task<PaginationList<Order>> GetOrdersByUserId(
           string userId, int pageSize, int pageNumber)
        {
            //return await _context.Orders.Where(o => o.UserId == userId).ToListAsync();
            IQueryable<Order> result = _context.Orders.Where(o => o.UserId == userId);
            return await PaginationList<Order>.CreateAsync(pageNumber, pageSize, result);
        }
        public async Task<Order> GetOrderById(Guid orderId)
        {
            return await _context.Orders
                .Include(o => o.OrderItems).ThenInclude(oi => oi.TouristRoute)
                .Where(o => o.Id == orderId)
                .FirstOrDefaultAsync();
        }
    }
}
