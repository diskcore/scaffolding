﻿using FakeXieCheng.API.Helper;
using FakeXieCheng.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FakeXieCheng.API.Services
{
    public interface ITouristRouteRepository
    {
        //IEnumerable<TouristRoute> GetTouristRoutes();
        //关键词查询
        //IEnumerable<TouristRoute> GetTouristRoutes(string keyword);
        //路线分数筛选
        //IEnumerable<TouristRoute> GetTouristRoutes(string keyword, string ratingOperator, int? ratingValue);
        //Task<IEnumerable<TouristRoute>> GetTouristRoutesAsync(string keyword, string ratingOperator, int? ratingValue);
        //Task<IEnumerable<TouristRoute>> GetTouristRoutesAsync(
        //    string keyword, string ratingOperator, int? ratingValue
        //    , int pageSize, int pageNumber);

        //Task<PaginationList<TouristRoute>> GetTouristRoutesAsync(
        //   string keyword, string ratingOperator, int? ratingValue
        //   , int pageSize, int pageNumber);

        //排序
        Task<PaginationList<TouristRoute>> GetTouristRoutesAsync(
          string keyword, string ratingOperator, int? ratingValue
          , int pageSize, int pageNumber,
          string orderBy);

        //TouristRoute GetTouristRoute(Guid touristRouteId);
        Task<TouristRoute> GetTouristRouteAsync(Guid touristRouteId);

        //bool TouristRouteExists(Guid touristRouteId);
        Task<bool> TouristRouteExistsAsync(Guid touristRouteId);
        //IEnumerable<TouristRoutePicture> GetPicturesByTouristRouteId(Guid touristRouteId);
        Task<IEnumerable<TouristRoutePicture>> GetPicturesByTouristRouteIdAsync(Guid touristRouteId);
        //TouristRoutePicture GetPicture(int pictureId);
        Task<TouristRoutePicture> GetPictureAsync(int pictureId);
        //创建旅游路线
        void AddTouristRoute(TouristRoute touristRoute);
    
        //创建旅游路线图片
        void AddTouristRoutePicture(Guid touristRouteId, TouristRoutePicture touristRoutePicture);

        //删除旅游路线 父资源
        void DeleteTouristRoute(TouristRoute touristRoute);
        //删除旅游路线 子资源
        void DeleteTouristRoutePicture(TouristRoutePicture picture);
        //批量删除资源
        void DeleteTouristRoutes(IEnumerable<TouristRoute> touristRoutes);
        //批量获取字子元
        //IEnumerable<TouristRoute> GetTouristRoutesByIDList(IEnumerable<Guid> ids);
        Task<IEnumerable<TouristRoute>> GetTouristRoutesByIDListAsync(IEnumerable<Guid> ids);

        //购物车
        Task<ShoppingCart> GetShoppingCartByUserId(string userId);
        Task CreateShoppingCart(ShoppingCart shoppingCart);

        //添加商品
        Task AddShoppingCartItem(LineItem lineItem);
        //删除商品
        Task<LineItem> GetShoppingCartItemByItemId(int lineItemId);
        void DeleteShoppingCartItem(LineItem lineItem);
        //批量删除商品
        Task<IEnumerable<LineItem>> GeshoppingCartsByIdListAsync(IEnumerable<int> ids);
        void DeleteShoppingCartItems(IEnumerable<LineItem> lineItems);
        //bool Save();
        //创建订单
        Task AddOrderAsync(Order order);

        //获取用户所有订单
        //Task<IEnumerable<Order>> GetOrdersByUserId(string userId);
        Task<PaginationList<Order>> GetOrdersByUserId(string userId, int pageSize, int pageNumber);
        //获取指定订单
        Task<Order> GetOrderById(Guid orderId);
        Task<bool> SaveAsync();
    }
}
