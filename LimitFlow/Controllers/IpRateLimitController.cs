﻿////using AspNetCoreRateLimit;
//using AspNetCoreRateLimit;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Options;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace LimitFlow.Controllers
//{
//    [Route("[controller]")]
//    public class IpRateLimitController : Controller
//    {
//        private readonly IpRateLimitOptions _options;
//        private readonly IIpPolicyStore _ipPolicyStore;

//        public IpRateLimitController(IOptions<IpRateLimitOptions> optionsAccessor, IIpPolicyStore ipPolicyStore)
//        {
//            _options = optionsAccessor.Value;
//            _ipPolicyStore = ipPolicyStore;
//        }

//        [HttpGet]
//        public   Task<IpRateLimitPolicies> Get()
//        {
//            return   _ipPolicyStore.Get(_options.IpPolicyPrefix);
//            //return "";
//        }

//        [HttpPost]
//        public async void Post()
//        {
//            var pol = await _ipPolicyStore.GetAsync(_options.IpPolicyPrefix);

//            pol.IpRules.Add(new IpRateLimitPolicy
//            {
//                Ip = "8.8.4.4",
//                Rules = new List<RateLimitRule>(new RateLimitRule[] {
//                new RateLimitRule {
//                    Endpoint = "*:/api/testupdate",
//                    Limit = 100,
//                    Period = "1d" }
//            })
//            });

//            await  _ipPolicyStore.SetAsync(_options.IpPolicyPrefix, pol);
//        }
//    }
//}
