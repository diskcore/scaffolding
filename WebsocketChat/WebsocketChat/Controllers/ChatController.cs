﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.WebSockets;

namespace WebsocketChat.Controllers
{
    [RoutePrefix("api/chat")]
    public class ChatController : ApiController
    {
        //所有WebSocket
        private static List<WebSocket> _socket = new List<WebSocket>();

        //
        static Dictionary<string, WebSocket> sWeb = new Dictionary<string, WebSocket>();

        //
        static ConcurrentDictionary<int, string> conDic = new ConcurrentDictionary<int, string>();

        static ChatController()
        {
            Action action = async () =>
            {
                while (true)
                {
                    Thread.Sleep(1000);
                    for (int i = 0; i < conDic.Keys.Count; i++)
                    {
                        var item = conDic.ElementAt(i);
                        string[] strArr = item.Value.Split('-');

                        string send = strArr[0];

                        string message = strArr[1];

                        string receive = strArr[2];

                        var recvBytes = Encoding.UTF8.GetBytes(send + "对你说：" + message);
                        var sendBuffer = new ArraySegment<byte>(recvBytes);
                        if (sWeb.Keys.Contains(receive) && sWeb[receive].State == WebSocketState.Open)
                        {
                            await sWeb[receive].SendAsync(sendBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                            //conQueue.TryDequeue(out string res);

                            conDic.TryRemove(item.Key, out string res);
                            break;
                        }
                    }
                    //conQueue.TryPeek(out string recvMsg);
                    ////信息解析
                    //string[] strArr = recvMsg.Split('-');
                    //string receive = strArr[0];
                    //string message = strArr[1];
                    //string send = strArr[2];
                    //var recvBytes = Encoding.UTF8.GetBytes(message);
                    //var sendBuffer = new ArraySegment<byte>(recvBytes);
                    //if (sWeb[receive] != null && sWeb[receive].State == WebSocketState.Open)
                    //{
                    //    await sWeb[receive].SendAsync(sendBuffer, WebSocketMessageType.Text, true, CancellationToken.None);
                    //    conQueue.TryDequeue(out string res);
                    //}
                    //conQueue.GetEnumerator();
                }
            };
            action.BeginInvoke(null, null);
        }

        [Route]
        [HttpGet]
        public HttpResponseMessage Connect(string userName)
        {
            //在服务端接受web socket请求，传入的函数作为web socket的处理函数，待web socket建立后该函数会被调用，
            //在该函数中可以对web socket进行消息收发
            HttpContext.Current.AcceptWebSocketRequest(ProcessRequest);

            //构造同意切换至web socket的response
            return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
        }

        public void MessageSend()
        {

        }
        static int Num = 1;

        public async Task ProcessRequest(AspNetWebSocketContext context)
        {
            WebSocket socket;
            string curUser = context.QueryString["userName"];
            if (!sWeb.Keys.Contains(curUser))
            {
                sWeb.Add(context.QueryString["userName"], context.WebSocket);
                socket = sWeb[curUser];
            }
            else
            {
                socket = sWeb[curUser];
            }
            //= context.WebSocket;//传入的context中当前的web socket对象

            //_socket.Add(socket);//此处将web socket对象加入一个静态列表中


            //进入一个无限循环,当web socket close是循环结束
            while (true)
            {
                Thread.Sleep(1000);
                var buffer = new ArraySegment<byte>(new byte[1024]);
                //对web socket进行异步接收数据
                try
                {
                    if (socket.State == WebSocketState.Open)
                    {
                        var receviedResult = await socket.ReceiveAsync(buffer, CancellationToken.None);
                        //关闭信息
                        if (receviedResult.MessageType == WebSocketMessageType.Close)
                        {
                            ////如果client发起close请求,对client进行ack
                            await socket.CloseAsync(WebSocketCloseStatus.Empty, string.Empty, CancellationToken.None);
                            sWeb.Remove(curUser);
                            //_socket.Remove(socket);
                            break;
                        }
                        //0 1 2
                        //获取发送信息 接受者-信息-发送者的格式
                        string recvMsg = Encoding.UTF8.GetString(buffer.Array, 0, receviedResult.Count);
                        //消息入队
                        if (recvMsg != "")
                        {
                            conDic.TryAdd(Num, recvMsg);
                            Num++;

                        }



                        ////信息收集
                        //string[] strArr=recvMsg.Split('-');

                        //string receive = strArr[0];
                        //string message = strArr[1];
                        //string send = strArr[2];

                        //信息入队

                        //var resModel = new Model { name = "新闻" + Guid.NewGuid().ToString(), date = DateTime.Now.Date, Detail = "查看" + Guid.NewGuid().ToString() };
                        //string resStr = JsonConvert.SerializeObject(resModel);
                        //var recvBytes = Encoding.UTF8.GetBytes(resStr);
                        //var sendBuffer = new ArraySegment<byte>(recvBytes);
                        ////当接收到文本消息时，对当前服务器上所有web socket链接进行广播
                        //foreach (var innerSocket in _socket)
                        //{
                        //    if (innerSocket == socket)
                        //    {
                        //        await innerSocket.SendAsync(sendBuffer, WebSocketMessageType.Text, true, CancellationToken.None);

                        //    }
                        //}
                    }


                }

                catch (Exception ex)
                {

                    throw ex;
                }
                //Thread.Sleep(2000);
            }
        }
    }
    public class Model
    {
        public string name { get; set; }
        public DateTime date { get; set; }
        public string Detail { get; set; }
    }
}
