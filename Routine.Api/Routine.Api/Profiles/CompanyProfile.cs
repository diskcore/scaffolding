﻿using AutoMapper;
using Routine.Api.Controllers;
using Routine.Api.Entities;
using Routine.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Routine.Api.Profiles
{
    public class CompanyProfile:Profile
    {
        //映射关系
        //AutoMap基于约定
        public CompanyProfile()
        {
            CreateMap<Company, CompanyDto>().ForMember(dest=>dest.CompanyName,
                opt=>opt.MapFrom(src=>src.Name));

            CreateMap<CompanyAddDto, Company>();
            CreateMap<Company, CompanyFullDto>();
            CreateMap<CompanyAddWithBankruptTimeDto, Company>();
        }
    }
}
