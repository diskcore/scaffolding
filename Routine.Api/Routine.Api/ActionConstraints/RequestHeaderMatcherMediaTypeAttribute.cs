﻿using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Routine.Api.ActionConstraints
{
    [AttributeUsage(AttributeTargets.All,Inherited =true,AllowMultiple =true)]
    public class RequestHeaderMatcherMediaTypeAttribute :Attribute, IActionConstraint
    {
        private readonly string _requestHeaderToMatch;
        private readonly MediaTypeCollection _mediaTypes = new MediaTypeCollection();
        public RequestHeaderMatcherMediaTypeAttribute(string requestHeaderToMath
            ,string mediaType
            ,params string[] ohterMediaTypes)
        {
            _requestHeaderToMatch = requestHeaderToMath ??
                throw new ArgumentNullException(nameof(requestHeaderToMath));

            if(MediaTypeHeaderValue.TryParse(mediaType,out MediaTypeHeaderValue parsedMediaType))
            {
                _mediaTypes.Add(parsedMediaType);
            }
            else
            {
                throw new ArgumentException(nameof(mediaType));
            }
            foreach (var ohterMediaType in ohterMediaTypes)
            {
                if (MediaTypeHeaderValue.TryParse(ohterMediaType, out MediaTypeHeaderValue parsedohterMediaType))
                {
                    _mediaTypes.Add(parsedohterMediaType);
                }
                else
                {
                    throw new ArgumentException(nameof(ohterMediaTypes));
                }
            }

        }
        public int Order => 0;

        public bool Accept(ActionConstraintContext context)
        {
            var requestHeaders = context.RouteContext.HttpContext.Request.Headers;
            if (!requestHeaders.ContainsKey(_requestHeaderToMatch))
            {
                return false;
            }
            var parsedRequestMediaType = new MediaType(requestHeaders[_requestHeaderToMatch]);
            foreach (var mediaType in _mediaTypes)
            {
                var parsedMediaType = new MediaType(mediaType);
                if (parsedRequestMediaType.Equals(parsedMediaType))
                {
                    return true;
                }
            }
            return false;   
        }
    }
}
