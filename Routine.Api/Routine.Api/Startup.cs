using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using Marvin.Cache.Headers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using Routine.Api.Data;
using Routine.Api.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace Routine.Api
{
    public class Startup
    {
        private readonly string Any = "Any";
        public Startup(IConfiguration configuration)
        {
            Configuration =    configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //注册ETag缓存
            //全局配置
            services.AddHttpCacheHeaders(expires=> {
                //过期模型
                expires.MaxAge = 60;
                expires.CacheLocation = CacheLocation.Private;
            },validation=> {
                //验证模型
                validation.MustRevalidate = true;
            });
            //注册缓存服务
            services.AddResponseCaching();

            //注册跨域
            services.AddCors(
             options => options.AddPolicy(Any, builder =>
             //允许任何主机来源
             builder.SetIsOriginAllowed(_ => true)
                .AllowAnyMethod()
                .AllowAnyHeader()
                //指定处理cookie
                .AllowCredentials()
                )
            );


            //添加输入和输出xml格式器
            services.AddControllers(setup =>
            {
                //客户端 Accept请求XML格式
                //服务端只有JSON格式 会返回406
                setup.ReturnHttpNotAcceptable = true;
                //默认格式是Json 加了支持XML
                //setup.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                //客户端没有指定格式 默认返回XML
                //setup.OutputFormatters.Insert(0, new XmlDataContractSerializerOutputFormatter());

                //缓存配置
                setup.CacheProfiles.Add("120sCacheProfile", new CacheProfile
                {
                    Duration = 120
                }) ;
            }).AddNewtonsoftJson(setup=> {
                setup.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }).AddXmlDataContractSerializerFormatters()
            .ConfigureApiBehaviorOptions(setup => {
                //错误报告  
                //JsonPatchDocument Model验证
                setup.InvalidModelStateResponseFactory = context =>
                {
                    var problemDetails = new ValidationProblemDetails(context.ModelState)
                    {
                        Type = "http://www.baidu.com",
                        Title = "有错误",
                        Status = StatusCodes.Status422UnprocessableEntity,
                        Detail = "请看详细信息",
                        Instance = context.HttpContext.Request.Path
                    };
                    problemDetails.Extensions.Add("traceId", context.HttpContext.TraceIdentifier);
                    return new UnprocessableEntityObjectResult(problemDetails)
                    {
                        ContentTypes = {"application/problem+json"}
                    };
                };
            });

            //配置特殊的输出格式
            services.Configure<MvcOptions>(config =>
            {
                var newtonSoftJsonOutputFormatter = config.OutputFormatters.
                OfType<NewtonsoftJsonOutputFormatter>()?.FirstOrDefault();

                if (newtonSoftJsonOutputFormatter != null)
                {
                    //全局注册
                    newtonSoftJsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.company.hateoas+json");
                    //newtonSoftJsonOutputFormatter.SupportedMediaTypes.Add("application/vnd.company.company.full.hateoas+json");
                }
            });

            // 页面通过 https://localhost:5001/swagger 查看相应API
            // 注册Swagger服务
            services.AddSwaggerGen(c =>
            {
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                // 添加文档信息 2.x使用
                //c.SwaggerDoc("v1", new Info { Title = "CoreWebApi", Version = "v1" });
                // 添加文档信息 3.x使用基础配置
                //c.SwaggerDoc("v1", new OpenApiInfo { Title = "CoreWebApi", Version = "v1" });

                //添加文档信息 3.x使用高级配置
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "CoreWebApi",
                    Version = "v1",
                    Description = "手脚架API Swagger主页面",
                    Contact = new OpenApiContact
                    {
                        Email = "75425817@qq.com",
                        Name = "个人邮箱",
                        Url = new Uri("http://www.netcore.pub/")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "许可证名称",
                        Url = new Uri("http://www.netcore.pub/")
                    }
                });
                // 使用反射获取xml文件。并构造出文件的路径
                //var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                //var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                // 启用xml注释. 该方法第二个参数启用控制器的注释，默认为false.
                //c.IncludeXmlComments(xmlPath, true);
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //var filePath = Path.Combine(System.AppContext.BaseDirectory, "Routine.Api.xml");
                c.IncludeXmlComments(xmlPath, true);
            });

            //AutoMapper配置
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            //请求生命周期 依赖注入
            services.AddScoped<ICompanyRepository, CompanyRepository>();

            //数据库配置
            services.AddDbContext<RoutineDbContext>(options =>
            {
                options.UseSqlite("Data Source=routine.db");
            });

            //注册Sort封装
            services.AddTransient<IPropertyMappingService, PropertyMappingService>();
            services.AddTransient<IPropertyCheckerService, PropertyCheckerService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //异常处理
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("Unexpected Error");
                    });
                });
            }

           

            //缓存中间件 没有实现ETag验证模型
            app.UseResponseCaching();

            //注册ETag缓存必须在缓存中间件之后
            app.UseHttpCacheHeaders();


            app.UseHttpsRedirection();

            //添加访问静态文件
            app.UseStaticFiles();

            //静态文件适配图片
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "imgs")),
                RequestPath = "/Mimall"
            });

            //静态文件适配音频
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "Video")),
                RequestPath = "/Video"
            });
            // 路由
            app.UseRouting();

            //跨域
            app.UseCors(Any);

            //权限
            app.UseAuthorization();

            // 启用Swagger中间件
            app.UseSwagger();

            // 配置SwaggerUI
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoreWebApi");
                //c.RoutePrefix = string.Empty;
            });


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
