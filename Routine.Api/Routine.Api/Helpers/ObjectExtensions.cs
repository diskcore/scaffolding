﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Routine.Api.Helpers
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// 数据塑形
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public static ExpandoObject ShapeData<TSource>(this TSource source,string fields)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            var expandoObj = new ExpandoObject();
            //NULL 所有字段
            if (string.IsNullOrWhiteSpace(fields))
            {
                var propertyInfos =
                    typeof(TSource).GetProperties(BindingFlags.IgnoreCase
                    | BindingFlags.Public
                    | BindingFlags.Instance);
                foreach (var propertyInfo in propertyInfos)
                {
                    var propertyValue = propertyInfo.GetValue(source);
                    ((IDictionary<string, object>)expandoObj).Add(propertyInfo.Name, propertyValue);
                }
            }
            // //部分字段
            else
            {
                var fieldsAfterSplit = fields.Split(",");
                foreach (var field in fieldsAfterSplit)
                {
                    var propertyName = field.Trim();
                    var propertyInfo =
                                      typeof(TSource).GetProperty(propertyName,BindingFlags.IgnoreCase
                                      | BindingFlags.Public
                                      | BindingFlags.Instance);
                    if(propertyInfo == null)
                    {
                        throw new Exception($"Property:{propertyName}没有找到：{typeof(TSource)}");
                    }
                    var propertyValue = propertyInfo.GetValue(source);
                   ((IDictionary<string, object>)expandoObj).Add(propertyInfo.Name, propertyValue);
                }
            }
            return expandoObj;
        }
    }
}
