﻿using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Routine.Api.Entities;
using Routine.Api.Helpers;
using Routine.Api.Models;
using Routine.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Routine.Api.Controllers
{
    [ApiController]
    [Route("api/companycollections")]
    [EnableCors("Any")]
    public class CompanyCollectionsController:ControllerBase
    {
        public IMapper _mapper { get; }
        public ICompanyRepository _companyRepository { get; }
        public CompanyCollectionsController(IMapper mapper,ICompanyRepository companyRepository)
        {
            _mapper = mapper?? throw new ArgumentNullException(nameof(mapper));
            _companyRepository = companyRepository?? throw new ArgumentNullException(nameof(companyRepository));
        }
        [HttpGet("{ids}",Name =nameof(GetCompanyCollection))]
        public async Task<IActionResult> GetCompanyCollection([FromRoute] 
        [ModelBinder(BinderType =typeof(ArrayModelBinder))]
        IEnumerable<Guid> ids)
        {
            if (ids == null)
            {
                return BadRequest();
            }
            var entities = await _companyRepository.GetCompaniesAsync(ids);
            if (ids.Count() != entities.Count())
            {
                return NotFound();
            }
            var  dtoToReturn = _mapper.Map<IEnumerable<CompanyDto>>(entities);
            return Ok(dtoToReturn);

        }

        /// <summary>
        /// 创建一组公司
        /// </summary>
        /// <param name="companyCollection"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<IEnumerable<CompanyDto>>> CreateCompanyCollection(IEnumerable<CompanyAddDto> companyCollection)
        {
            var companyEntities = _mapper.Map<IEnumerable<Company>>(companyCollection);
            foreach (var company in companyEntities)
            {
                _companyRepository.AddCompany(company);
            }
           await _companyRepository.SaveAsync();
           
            var dtoToReturn = _mapper.Map<IEnumerable<CompanyDto>>(companyEntities);
            var idsString = string.Join(",", dtoToReturn.Select(x => x.Id));
            //Headers-Location:http://localhost:5000/api/companycollections/7415ea84-f13f-42a4-96e6-64759674401b,8e2e5c7a-70dc-4e12-8955-ca9057c481f8,aae7eedc-f7a9-45d1-beb8-28e464fa6a64
            return CreatedAtRoute(nameof(GetCompanyCollection),new { ids=idsString}, dtoToReturn);
        }
       
    }
}
