﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Net.Http.Headers;
using Routine.Api.ActionConstraints;
//using Newtonsoft.Json;
using Routine.Api.DtoParameters;
using Routine.Api.Entities;
using Routine.Api.Helpers;
using Routine.Api.Models;
using Routine.Api.Services;

namespace Routine.Api.Controllers
{
    /// <summary>
    /// 公司操作控制器
    /// </summary>
    [ApiController] //自动处理验证400错误
    [Route("api/companies")]
    [EnableCors("Any")]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;
        private readonly IPropertyMappingService _propertyMappingService;
        private readonly IPropertyCheckerService _propertyCheckerService;
        public CompaniesController(ICompanyRepository companyRepository,
            IMapper mapper,
            IPropertyMappingService propertyMappingService,
            IPropertyCheckerService propertyCheckerService)
        {
            _companyRepository = companyRepository ??
                throw new ArgumentNullException(nameof(companyRepository));
            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
            _propertyMappingService= propertyMappingService ??
                throw new ArgumentNullException(nameof(propertyMappingService));
            _propertyCheckerService= propertyCheckerService ??
                throw new ArgumentNullException(nameof(propertyCheckerService));

        }

        /// <summary>
        /// 获取所有公司
        /// 参数配合框架swagger使用 类型更加明确
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name =nameof(GetCompanies))]
        [HttpHead]
        public async Task<IActionResult> GetCompanies(
            [FromQuery]CompanyDtoParameters parameters)
        {
            //验证排序字段的正确性
            if (!_propertyMappingService.ValidMappingExcistsFor<CompanyDto, Company>(parameters.OrderBy))
            {
                return BadRequest();
            }
            //检查塑形字段的正确性
            if (!_propertyCheckerService.TypeHasProperties<CompanyDto>(parameters.Fields))
            {
                return BadRequest();
            }
            var companies = await _companyRepository.GetCompanyiesAsync(parameters);

            //var previousPageLink = companies.HasPrevious ?
            //    CreateCompaniesResourceUri(parameters, ResourceUriType.PreviousPage) : null;

            //var nextPageLink = companies.HasNext ?
            //   CreateCompaniesResourceUri(parameters, ResourceUriType.NextPage) : null;

            var paginationMetadata = new
            {
                totalCount=companies.TotalCount,
                pageSize=companies.PageSize,
                currentPage=companies.CurrentPage,
                totalPages=companies.TotalPages,
                //previousPageLink,
                //nextPageLink
            };
            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMetadata,new JsonSerializerOptions { 
            Encoder=System.Text.Encodings.Web.JavaScriptEncoder.UnsafeRelaxedJsonEscaping}));
            //var companyDtos = new List<CompanyDto>();
            //对象映射器AutoMapper
            //foreach (var company in companies)
            //{
            //    companyDtos.Add(new CompanyDto
            //    {
            //        Id=company.Id,
            //        Name=company.Name
            //    });
            //}

           

            var companyDtos = _mapper.Map<IEnumerable<CompanyDto>>(companies);
            var shapeData = companyDtos.ShapeData(parameters.Fields);
            //上一页 下一页  当前页
            var links = CreateLinksForCompany(parameters, companies.HasPrevious, companies.HasNext);


            //公司的额外操作
            var shapedCompaniesWithLinks = shapeData.Select(c =>
            {
                var companyDict = c as IDictionary<string, object>;
                var companyLinks = CreateLinksForCompany((Guid)companyDict["Id"], null);
                companyDict.Add("links", companyLinks);
                return companyDict;
            });

            var linkedCollectionResource = new
            {
                value = shapedCompaniesWithLinks,
                links
            };

            return Ok(/*companyDtos.ShapeData(parameters.Fields)*/linkedCollectionResource);
        }
        /// <summary>
        /// 获取单个公司
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="fields"></param>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        [HttpGet("{companyId}",Name =nameof(GetCompany))]
        //ContentType： 告诉服务器当前发送的数据是什么格式
        //Accept ： 用来告诉服务器，客户端能认识哪些格式,最好返回这些格式
        //produces: 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回；
        [Produces("application/json"
            , "application/vnd.company.hateoas+json"
            , "application/vnd.company.company.full.hateoas+json")]
        public async Task<IActionResult> GetCompany([FromRoute]Guid companyId
            ,string fields
            ,[FromHeader(Name ="Accept")] string mediaType)
        {
            if (!MediaTypeHeaderValue.TryParse(mediaType,out MediaTypeHeaderValue parsedMediaType))
            {
                return BadRequest();
            }
            if (!_propertyCheckerService.TypeHasProperties<CompanyDto>(fields))
            {
                return BadRequest();
            }
            var company = await _companyRepository.GetCompanyAsync(companyId);
            if(company==null)
            {
                return NotFound();
            }

            var IncludeLinks = parsedMediaType.SubTypeWithoutSuffix.EndsWith("hateoas",
                StringComparison.InvariantCultureIgnoreCase);


            IEnumerable<LinkDto> myLinks = new List<LinkDto>();
            if (IncludeLinks)
            {
                myLinks = CreateLinksForCompany(companyId, fields);
            }

            var primaryMediaType = IncludeLinks ?
                parsedMediaType.SubTypeWithoutSuffix.Substring(0, parsedMediaType.SubTypeWithoutSuffix.Length - 8) :
                parsedMediaType.SubTypeWithoutSuffix;

            if (primaryMediaType == "vnd.company.company.full")
            {
                var full = _mapper.Map<CompanyFullDto>(company).ShapeData(fields)
                    as IDictionary<string, object>;
                if (IncludeLinks)
                {
                    full.Add("links", myLinks);
                }
                return Ok(full);
            }
            
            var friendly = _mapper.Map<CompanyDto>(company).ShapeData(fields) as IDictionary<string, object>;
            if (IncludeLinks)
            {
                friendly.Add("links", myLinks);
            }
            return Ok(friendly);
            //if (parsedMediaType.MediaType == "application/vnd.company.hateoas+json")
            //{
            //    var links = CreateLinksForCompany(companyId, fields);
            //    var linkedDict = _mapper.Map<CompanyDto>(company).ShapeData(fields) as IDictionary<string, object>;


            //    linkedDict.Add("links", links);
            //    return Ok(/*_mapper.Map<CompanyDto>(company).ShapeData(fields)*/linkedDict);
            //}
            //return Ok(_mapper.Map<CompanyDto>(company).ShapeData(fields));
           
        }
      
        /// <summary>
        /// 创建公司
        /// </summary>
        /// <param name="company">反序列化公司</param>
        /// <returns></returns>
        [HttpPost(Name = nameof(CreateCompanyWithBankruptTime))]
        [RequestHeaderMatcherMediaType("Content-Type"         
            , "application/vnd.company.companyforcreationwithbankrupttime+json")]
        //ContentType： 告诉服务器当前发送的数据是什么格式
        //Accept ： 用来告诉服务器，客户端能认识哪些格式,最好返回这些格式
        //consumes： 指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;
        [Consumes( "application/vnd.company.companyforcreationwithbankrupttime+json")]
        public async Task<ActionResult<CompanyDto>> CreateCompanyWithBankruptTime([FromBody] 
        CompanyAddWithBankruptTimeDto company)
        {
            var entity = _mapper.Map<Company>(company);
            _companyRepository.AddCompany(entity);
            await _companyRepository.SaveAsync();
            var returnDto = _mapper.Map<CompanyDto>(entity);

            var links = CreateLinksForCompany(returnDto.Id, null);

            var linkedDict = returnDto.ShapeData(null) as IDictionary<string, object>;

            linkedDict.Add("links", links);

            //第三个参数返回响应的body
            //我想告诉消费者我的api关于新创建的对象的位置
            //https://www.cnblogs.com/zhaoshujie/p/12306481.html
            //返回结果在Header的Location
            return CreatedAtRoute(nameof(GetCompany), new { companyId = linkedDict["Id"] }, linkedDict);

        }
        /// <summary>
        /// 创建公司
        /// </summary>
        /// <param name="company">反序列化公司</param>
        /// <returns></returns>
        [HttpPost(Name = nameof(CreateCompany))]
        [RequestHeaderMatcherMediaType("Content-Type"
            , "application/json"
            , "application/vnd.company.companyforcreation+json")]

        ///筛选Content-Type
        [Consumes("application/json"
            , "application/vnd.company.companyforcreation+json")]
        public async Task<ActionResult<CompanyDto>> CreateCompany([FromBody] CompanyAddDto company)
        {
            var entity = _mapper.Map<Company>(company);
            _companyRepository.AddCompany(entity);
            await _companyRepository.SaveAsync();
            var returnDto = _mapper.Map<CompanyDto>(entity);

            var links = CreateLinksForCompany(returnDto.Id, null);

            var linkedDict = returnDto.ShapeData(null) as IDictionary<string, object>;

            linkedDict.Add("links", links);

            //第三个参数返回响应的body
            return CreatedAtRoute(nameof(GetCompany), new { companyId = linkedDict["Id"] }, linkedDict);

        }

        /// <summary>
        /// 删除一个公司
        /// URL:http://localhost:5000/api/companies/a5454227-022a-4ddc-8b42-3ce838641a1a
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpDelete("{companyId}",Name=nameof(DeleteCompany))]
        public async Task<IActionResult> DeleteCompany(Guid companyId)
        {
            var companyEntity = await _companyRepository.GetCompanyAsync(companyId);
            if(companyEntity == null)
            {
                return NotFound();
            }
            await _companyRepository.GetEmployeesAsync(companyId, null);
            _companyRepository.DeleteCompany(companyEntity);
            await  _companyRepository.SaveAsync();
            return NoContent();
        }

        [HttpOptions]
        public IActionResult GetCompaniesOptions()
        {
            Response.Headers.Add("Allow", "GET,POST,OPTIONS");
            return Ok();
        }
        private string CreateCompaniesResourceUri(CompanyDtoParameters parameters,ResourceUriType type)
        {
            switch (type)
            {
                case ResourceUriType.PreviousPage:
                    return Url.Link(nameof(GetCompanies), new {
                    fields = parameters.Fields,
                    orderBy =parameters.OrderBy,
                    pageNumber=parameters.PageNumber-1,
                    pageSize=parameters.PageSize,
                    companyName = parameters.CompanyName,
                    searchTerm = parameters.SearchTerm
                    });
                case ResourceUriType.NextPage:
                    return Url.Link(nameof(GetCompanies), new
                    {
                        fields = parameters.Fields,
                        orderBy = parameters.OrderBy,
                        pageNumber = parameters.PageNumber + 1,
                        pageSize = parameters.PageSize,
                        companyName = parameters.CompanyName,
                        searchTerm = parameters.SearchTerm
                    }) ;
                case ResourceUriType.CurrentPage:
                default:
                    return Url.Link(nameof(GetCompanies), new
                    {
                        fields = parameters.Fields,
                        orderBy = parameters.OrderBy,
                        pageNumber = parameters.PageNumber ,
                        pageSize = parameters.PageSize,
                        companyName = parameters.CompanyName,
                        searchTerm = parameters.SearchTerm
                    });
            }
         
        }
        /// <summary>
        /// 第二个参数塑形字段
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        private IEnumerable<LinkDto> CreateLinksForCompany(Guid companyId,string fields)
        {
            var links = new List<LinkDto>();
            if (string.IsNullOrWhiteSpace(fields))
            {
                links.Add(new LinkDto(Url.Link(nameof(GetCompany), new { companyId })
                    , "self", "GET"));
            }
            else
            {
                links.Add(new LinkDto(Url.Link(nameof(GetCompany), new { companyId,fields })
                   , "self", "GET"));
            }
            links.Add(new LinkDto(Url.Link(nameof(DeleteCompany), new { companyId })
                  , "delete_company", "DELETE"));

            links.Add(new LinkDto(Url.Link(nameof(EmployeesController.CreateEmployeeForCompany), new { companyId })
                 , "create_employee_for_company", "POST"));

            links.Add(new LinkDto(Url.Link(nameof(EmployeesController.GetEmployeesForCompany), new { companyId })
                 , "employees", "GET"));
            return links;
        }

        private IEnumerable<LinkDto> CreateLinksForCompany(CompanyDtoParameters parameters,bool hasPrevious,bool hasNext)
        {
            var links = new List<LinkDto>();

            links.Add(new LinkDto(CreateCompaniesResourceUri(parameters,ResourceUriType.CurrentPage)
                  , "self", "GET"));
            if (hasPrevious)
            {
                links.Add(new LinkDto(CreateCompaniesResourceUri(parameters, ResourceUriType.PreviousPage)
                  , "previous_page", "GET"));
            }
            if (hasNext)
            {
                links.Add(new LinkDto(CreateCompaniesResourceUri(parameters, ResourceUriType.NextPage)
                  , "next_page", "GET"));
            }
            return links;
        }


    }
}
