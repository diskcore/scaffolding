﻿using AutoMapper;
using Marvin.Cache.Headers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Routine.Api.DtoParameters;
using Routine.Api.Entities;
using Routine.Api.Models;
using Routine.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Routine.Api.Controllers
{
    /// <summary>
    /// 员工控制器
    /// </summary>
    [ApiController]
    [Route("api/companies/{companyId}/employees")]
    //[ResponseCache(CacheProfileName = "120sCacheProfile")]
    //缓存
    [HttpCacheExpiration(CacheLocation = CacheLocation.Public)]
    [HttpCacheValidation(MustRevalidate = true)]
    //跨域
    [EnableCors("Any")]
    public class EmployeesController:ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepository;

        public EmployeesController(IMapper mapper,ICompanyRepository companyRepository)
        {
            _mapper = mapper?? throw new ArgumentNullException(nameof(mapper));
            _companyRepository = companyRepository??throw new ArgumentNullException(nameof(companyRepository));
        }
        /// <summary>
        /// 获取指定公司所有员工
        /// URL:http://localhost:5000/api/companies?orderBy=country,companyName desc&PageNumber=1&pageSize=2
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        [HttpGet(Name =nameof(GetEmployeesForCompany))]
        public async Task<ActionResult<IEnumerable<EmployeeDto>>> GetEmployeesForCompany(Guid companyId,
          [FromQuery] EmployeeDtoParameters parameters /*[FromQuery(Name ="gender")] string genderDisplay,string q*/)
        {
            if(! await _companyRepository.CompanyExistsAsync(companyId))
            {
                return NotFound();
            }
            var employees = await _companyRepository.GetEmployeesAsync(companyId, parameters/*genderDisplay,q*/);
            var employeeDtos = _mapper.Map<IEnumerable<EmployeeDto>>(employees);
            return Ok(employeeDtos);
        }
        /// <summary>
        /// 获取指定公司指定员工
        /// URL:http://localhost:5000/api/companies/87B6B176-9F13-4246-A06D-7B1AFEABB15B/employees/1bc1f9fd-967d-4e50-8bd8-99238a2e6297
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpGet("{employeeId}",Name =nameof(GetEmployeeForCompanyd))]
        //只是设置了缓存头 没有实际作用
        //[ResponseCache(Duration =60)]
        [HttpCacheExpiration(CacheLocation=CacheLocation.Public,MaxAge =180)]
        [HttpCacheValidation(MustRevalidate =false)]
        public async Task<ActionResult<EmployeeDto>> GetEmployeeForCompanyd(Guid companyId,Guid employeeId)
        {
            if (!await _companyRepository.CompanyExistsAsync(companyId))
            {
                return NotFound();
            }
            var employee = await _companyRepository.GetEmployeeAsync(companyId,employeeId);
            if (employee == null)
            {
                return NotFound();
            }
            var employeeDto = _mapper.Map<EmployeeDto>(employee);
            return Ok(employeeDto);
        }
        /// <summary>
        /// 创建一个员工
        /// URL：
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost(Name =nameof(CreateEmployeeForCompany))]
        public async Task<ActionResult<EmployeeDto>> CreateEmployeeForCompany(Guid companyId,EmployeeAddDto employee)
        {
            if(! await _companyRepository.CompanyExistsAsync(companyId))
            {
                return NotFound();
            }
            var entity = _mapper.Map<Employee>(employee);
            _companyRepository.AddEmployee(companyId, entity);
            await _companyRepository.SaveAsync();
            var dtoToReturn = _mapper.Map<EmployeeDto>(entity);
            return CreatedAtRoute(nameof(GetEmployeeForCompanyd),new { companyId= dtoToReturn .CompanyId,
                employeeId=dtoToReturn.Id
            }, dtoToReturn);
        }
        /// <summary>
        /// 员工整体更新信息
        /// 不安全 幂等
        /// URL：http://localhost:5000/api/companies/A5454227-022A-4DDC-8B42-3CE838641A1A/employees/9D522C3D-FC71-48DF-86E0-B1DE8FA09FFD
        /// {
        ///    "employeeNo":"2020010601",
        ///    "firstName":"ximiao",
        ///    "lastName":"ximiao",
        ///    "gender":2,
        ///    "dataOfBirth":"1989-12-31"
        ///}
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="employeeId"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPut("{employeeId}")]
        public async Task<ActionResult<EmployeeDto>> UpdateEmployeeForCompany(
            Guid companyId,
            Guid employeeId,
            EmployeeUpdateDto employee)
        {
            if(!await _companyRepository.CompanyExistsAsync(companyId))
            {
                return NotFound();
            }
            var employeeEntity = await _companyRepository.GetEmployeeAsync(companyId, employeeId);
            if (employeeEntity == null)
            {
                //return NotFound();
                var employeeToAddEntity = _mapper.Map<Employee>(employee);
                employeeToAddEntity.Id = employeeId;
                _companyRepository.AddEmployee(companyId, employeeToAddEntity);
                await _companyRepository.SaveAsync();
                var dtoToReturn = _mapper.Map<EmployeeDto>(employeeToAddEntity);
                //不存在员工就进行新建
                return CreatedAtRoute(nameof(GetEmployeeForCompanyd), new
                {
                    companyId = dtoToReturn.CompanyId,
                    employeeId = dtoToReturn.Id
                }, dtoToReturn);
            }
            //从employee 映射到employeeEntity
            _mapper.Map(employee, employeeEntity);

            _companyRepository.UpdateEmployee(employeeEntity);

            await _companyRepository.SaveAsync();

            return NoContent();

        }
        /// <summary>
        /// 局部更新或新增
        /// Content-Type ： application/json-patch+json
        /// URL：http://localhost:5000/api/companies/a5454227-022a-4ddc-8b42-3ce838641a1a/employees/a5454227-022a-4ddc-8b42-3ce838641a1a
        /// 测试值：body:
        /// [{
        ///"op":"replace",
        ///"path":"/employeeNo",
        ///"value":"1111122222"
       /// }]
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="employeeId"></param>
        /// <param name="patchDocument"></param>
        /// <returns></returns>
        [HttpPatch("{employeeId}")]
        public async Task<IActionResult> PartialyUpdateEmployeeForCompany(
            Guid companyId,
            Guid employeeId,
            JsonPatchDocument<EmployeeUpdateDto> patchDocument
            )
        {
            if (!await _companyRepository.CompanyExistsAsync(companyId))
            {
                return NotFound();
            }
            var employeeEntity = await _companyRepository.GetEmployeeAsync(companyId, employeeId);
            if (employeeEntity == null)
            {
                var employeeDto = new EmployeeUpdateDto();
                patchDocument.ApplyTo(employeeDto, ModelState);
                //走JsonPatchDocument参数验证
                if (!TryValidateModel(employeeDto))
                {
                    return ValidationProblem(ModelState);
                }
                var employeeToAdd=_mapper.Map<Employee>(employeeDto);
                employeeToAdd.Id = employeeId;
                _companyRepository.AddEmployee(companyId, employeeToAdd);
                await _companyRepository.SaveAsync();

                var dtoToReturn = _mapper.Map<EmployeeDto>(employeeToAdd);
                return CreatedAtRoute(nameof(GetEmployeeForCompanyd), new
                {
                    companyId = dtoToReturn.CompanyId,
                    employeeId = dtoToReturn.Id
                }, dtoToReturn);
                //return NotFound();
            }
            var dtoToPatch = _mapper.Map<EmployeeUpdateDto>(employeeEntity);

            //需要处理验证处理
            patchDocument.ApplyTo(dtoToPatch,ModelState);

            //走JsonPatchDocument参数验证
            if (!TryValidateModel(dtoToPatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(dtoToPatch, employeeEntity);

            _companyRepository.UpdateEmployee(employeeEntity);

            await _companyRepository.SaveAsync();

            return NoContent();
        }
        /// <summary>
        /// 删除一个员工
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [HttpDelete("{employeeId}")]
        public async Task<IActionResult> DeleteEmployeeForCompany(Guid companyId,Guid employeeId)
        {
            if (!await _companyRepository.CompanyExistsAsync(companyId))
            {
                return NotFound();
            }
            var employeeEntity = await _companyRepository.GetEmployeeAsync(companyId, employeeId);
            if (employeeEntity == null)
            {
                return NotFound();
            }
            _companyRepository.DeleteEmployee(employeeEntity);
            await _companyRepository.SaveAsync();
            return NoContent();

        }

        public override ActionResult ValidationProblem([ActionResultObjectValue] ModelStateDictionary modelStateDictionary)
        {
            var options = HttpContext.RequestServices.GetRequiredService<IOptions<ApiBehaviorOptions>>();
            return (ActionResult)options.Value.InvalidModelStateResponseFactory(ControllerContext);
        }

    }
}
