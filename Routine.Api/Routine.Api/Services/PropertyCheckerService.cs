﻿
using System.Reflection;


namespace Routine.Api.Services
{
    public class PropertyCheckerService: IPropertyCheckerService
    {
        /// <summary>
        /// 判断是否有效属性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fields"></param>
        /// <returns></returns>
        public bool TypeHasProperties<T>(string fields)
        {
            if (string.IsNullOrWhiteSpace(fields))
            {
                return true;
            }
            var fieldAfterSplit = fields.Split(",");
            foreach (var field in fieldAfterSplit)
            {
                var propertyName = field.Trim();
                var proopertyInfo = typeof(T).GetProperty(propertyName,
                      BindingFlags.IgnoreCase
                    | BindingFlags.Public
                    | BindingFlags.Instance);
                if (proopertyInfo == null)
                {
                    return false; 
                }
            }
            return true;
        }
    }
}
