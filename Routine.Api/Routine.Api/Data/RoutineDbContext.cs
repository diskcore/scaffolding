﻿using Microsoft.EntityFrameworkCore;
using Routine.Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Routine.Api.Data
{
    public class RoutineDbContext : DbContext
    {
        public RoutineDbContext(DbContextOptions<RoutineDbContext> options) : base(options)
        {

        }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Employee> Employees { get; set; }

        //生成表结构
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>()
                .Property(x => x.Name)
                .IsRequired().HasMaxLength(100);

            modelBuilder.Entity<Company>()
                .Property(x => x.Country)
                .HasMaxLength(50);

            modelBuilder.Entity<Company>()
                .Property(x => x.Industry)
                .HasMaxLength(50);

            modelBuilder.Entity<Company>()
                .Property(x => x.Product)
                .HasMaxLength(100);

            modelBuilder.Entity<Company>()
               .Property(x => x.Introduction)
               .HasMaxLength(500);

            modelBuilder.Entity<Employee>()
              .Property(x => x.EmployeeNo)
              .IsRequired().HasMaxLength(10);

            modelBuilder.Entity<Employee>()
              .Property(x => x.FirstName)
              .IsRequired().HasMaxLength(50);

            modelBuilder.Entity<Employee>()
              .Property(x => x.LastName)
              .IsRequired().HasMaxLength(50);

            modelBuilder.Entity<Employee>()
             .HasOne(navigationExpression: x => x.Company)
             .WithMany(navigationExpression: x => x.Employees)
             .HasForeignKey(x => x.CompanyId).OnDelete(DeleteBehavior.Cascade);


            //设置Company数据
            modelBuilder.Entity<Company>().HasData(
                new Company
                {
                    Id = Guid.Parse("A5454227-022A-4DDC-8B42-3CE838641A1A"),
                    Name = "Microsoft",
                    Introduction = "Great  Company",
                    Country = "USA",
                    Industry = "Internet",
                    Product = "Software"
                },
                 new Company
                 {
                     Id = Guid.Parse("87B6B176-9F13-4246-A06D-7B1AFEABB15B"),
                     Name = "Google",
                     Introduction = "Don't be evil",
                     Country="USA",
                     Industry="Software",
                     Product= "Software"

                 },
                  new Company
                  {
                      Id = Guid.Parse("784CC59A-52B3-4507-B540-4C7D37938751"),
                      Name = "Alipapa",
                      Introduction = "Fubao Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Software"
                  },
                  new Company
                  {
                      //[Guid("B82D6F84-2C9D-4961-B971-A692B6F6CC06")]
                      Id = Guid.Parse("B82D6F84-2C9D-4961-B971-A692B6F6CC06"),
                      Name = "Bai Du",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Bai Du"
                  },
                  new Company
                  {
                      //[Guid("20E179DE-2169-42EE-AC5C-1BA688C78168")]
                      Id = Guid.Parse("20E179DE-2169-42EE-AC5C-1BA688C78168"),
                      Name = "Mei Tuan",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Mei Tuan"
                  },
                  new Company
                  {
                      //[Guid("AFD55E56-9548-45B7-899B-EEDCD0763779")]
                      Id = Guid.Parse("AFD55E56-9548-45B7-899B-EEDCD0763779"),
                      Name = "Eleme",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Eleme"
                  },
                  new Company
                  {
                      //[Guid("A5ACDE36-237C-4482-8B43-A9EE29CFAB3A")]
                      Id = Guid.Parse("A5ACDE36-237C-4482-8B43-A9EE29CFAB3A"),
                      Name = "JingDong",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "JingDong"
                  },
                  new Company
                  {
                      //[Guid("726C7B98-E1E2-42DF-9150-D681E9195A76")]
                      Id = Guid.Parse("726C7B98-E1E2-42DF-9150-D681E9195A76"),
                      Name = "TianMao",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "TianMao"
                  },
                  new Company
                  {
                      Id = Guid.Parse("A5474227-022A-4DDC-8B42-3CE838641A1A"),
                      Name = "Zhi Fu Bao",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Zhi Fu Bao"
                  },
                  new Company
                  {
                      //[Guid("DCE6C8A4-465A-48DB-9E0E-74DA91D819B7")]
                      Id = Guid.Parse("DCE6C8A4-465A-48DB-9E0E-74DA91D819B7"),
                      Name = "Microsoft",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Microsoft"
                  },
                  new Company
                  {
                      //[Guid("AF4743F0-EBA0-4934-8281-AC5592A59789")]
                      Id = Guid.Parse("AF4743F0-EBA0-4934-8281-AC5592A59789"),
                      Name = "Dou Ying",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Dou Ying"
                  },
                  new Company
                  {
                      //[Guid("43BD3E7E-7EB1-4B58-9FE1-8F138941E48F")]
                      Id = Guid.Parse("43BD3E7E-7EB1-4B58-9FE1-8F138941E48F"),
                      Name = "Jin Ri Tou Tiao",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Jin Ri Tou Tiao"
                  },
                  new Company
                  {
                      //
                      Id = Guid.Parse("A5454237-022A-4DDC-8B42-3CE838641A1A"),
                      Name = "Apple",
                      Introduction = "Great  Company",
                      Country = "USA",
                      Industry = "Internet",
                      Product = "Apple"
                  },
                  new Company
                  {
                      //[Guid("C295771D-B02B-4A28-95A0-0991C14DC6C6")]
                      Id = Guid.Parse("C295771D-B02B-4A28-95A0-0991C14DC6C6"),
                      Name = "Hua Wei",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Hua Wei"
                  },
                  new Company
                  {
                      //[Guid("971CAB58-BF41-49F9-A6ED-621B463F309A")]
                      Id = Guid.Parse("971CAB58-BF41-49F9-A6ED-621B463F309A"),
                      Name = "Xiao Mi",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Xiao Mi"
                  },
                  new Company
                  {
                      //[Guid("DFD3241F-6EFD-44B3-8F69-439A8C5071A9")]
                      Id = Guid.Parse("DFD3241F-6EFD-44B3-8F69-439A8C5071A9"),
                      Name = "DI DI",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "DI DI"
                  },
                  new Company
                  {
                      //[Guid("DF6A6DF0-C982-49AF-9710-E7CB4C684E26")]
                      Id = Guid.Parse("DF6A6DF0-C982-49AF-9710-E7CB4C684E26"),
                      Name = "PING DUO DUO",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "PING DUO DUO"
                  },
                  new Company
                  {
                      //[Guid("0F582B03-D902-40D3-81E9-55FF97722793")]
                      Id = Guid.Parse("0F582B03-D902-40D3-81E9-55FF97722793"),
                      Name = "AI QI YI",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "AI QI YI"
                  },
                  new Company
                  {
                      //[Guid("E2F51543-96A9-47C1-B0B2-2A1C60557D52")]
                      Id = Guid.Parse("E2F51543-96A9-47C1-B0B2-2A1C60557D52"),
                      Name = "XING LANG",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "XING LANG"
                  }
                  );

            //设置Employee数据
            modelBuilder.Entity<Employee>().HasData(
             new Employee
             {
                 Id = Guid.Parse("01ECE19E-5D64-42A5-808A-9C4112CB47B8"),
                 CompanyId = Guid.Parse("87B6B176-9F13-4246-A06D-7B1AFEABB15B"),
                 DateOfBirth = new DateTime(1986, 11, 4),
                 EmployeeNo = "G003",
                 FirstName = "Mary",
                 LastName = "King",
                 Gender = Gender.女
             },
             new Employee
             {
                 Id= Guid.Parse("1BC1F9FD-967D-4E50-8BD8-99238A2E6297"),
                 CompanyId = Guid.Parse("87B6B176-9F13-4246-A06D-7B1AFEABB15B"),
                 DateOfBirth = new DateTime(1977, 4, 6),
                 EmployeeNo = "G097",
                 FirstName = "Kevin",
                 LastName = "Richardson",
                 Gender = Gender.男
             }
            );
        }
    }
}

