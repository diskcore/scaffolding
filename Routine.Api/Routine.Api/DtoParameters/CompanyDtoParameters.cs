﻿using System;


namespace Routine.Api.DtoParameters
{
    /// <summary>
    /// 封装Webapi参数类DtoParameter
    /// </summary>
    public class CompanyDtoParameters
    {
        //限制分页最大数量
        const int MaxPageSize = 20;
        public string CompanyName { get; set; }
        public string SearchTerm { get; set; }
        public string OrderBy { get; set; } = "CompanyName";
        public int PageNumber { get; set; } = 1;
        public string Fields { get; set; }
        private int _pageSize=5;

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }

    }
}
