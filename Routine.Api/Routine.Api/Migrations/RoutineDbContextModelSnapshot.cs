﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Routine.Api.Data;

namespace Routine.Api.Migrations
{
    [DbContext(typeof(RoutineDbContext))]
    partial class RoutineDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.1");

            modelBuilder.Entity("Routine.Api.Entities.Company", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<DateTime?>("BankruptTime")
                        .HasColumnType("TEXT");

                    b.Property<string>("Country")
                        .HasColumnType("TEXT")
                        .HasMaxLength(50);

                    b.Property<string>("Industry")
                        .HasColumnType("TEXT")
                        .HasMaxLength(50);

                    b.Property<string>("Introduction")
                        .HasColumnType("TEXT")
                        .HasMaxLength(500);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasMaxLength(100);

                    b.Property<string>("Product")
                        .HasColumnType("TEXT")
                        .HasMaxLength(100);

                    b.HasKey("Id");

                    b.ToTable("Companies");

                    b.HasData(
                        new
                        {
                            Id = new Guid("a5454227-022a-4ddc-8b42-3ce838641a1a"),
                            Country = "USA",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Microsoft",
                            Product = "Software"
                        },
                        new
                        {
                            Id = new Guid("87b6b176-9f13-4246-a06d-7b1afeabb15b"),
                            Country = "USA",
                            Industry = "Software",
                            Introduction = "Don't be evil",
                            Name = "Google",
                            Product = "Software"
                        },
                        new
                        {
                            Id = new Guid("784cc59a-52b3-4507-b540-4c7d37938751"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Fubao Company",
                            Name = "Alipapa",
                            Product = "Software"
                        },
                        new
                        {
                            Id = new Guid("b82d6f84-2c9d-4961-b971-a692b6f6cc06"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Bai Du",
                            Product = "Bai Du"
                        },
                        new
                        {
                            Id = new Guid("20e179de-2169-42ee-ac5c-1ba688c78168"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Mei Tuan",
                            Product = "Mei Tuan"
                        },
                        new
                        {
                            Id = new Guid("afd55e56-9548-45b7-899b-eedcd0763779"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Eleme",
                            Product = "Eleme"
                        },
                        new
                        {
                            Id = new Guid("a5acde36-237c-4482-8b43-a9ee29cfab3a"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "JingDong",
                            Product = "JingDong"
                        },
                        new
                        {
                            Id = new Guid("726c7b98-e1e2-42df-9150-d681e9195a76"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "TianMao",
                            Product = "TianMao"
                        },
                        new
                        {
                            Id = new Guid("a5474227-022a-4ddc-8b42-3ce838641a1a"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Zhi Fu Bao",
                            Product = "Zhi Fu Bao"
                        },
                        new
                        {
                            Id = new Guid("dce6c8a4-465a-48db-9e0e-74da91d819b7"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Microsoft",
                            Product = "Microsoft"
                        },
                        new
                        {
                            Id = new Guid("af4743f0-eba0-4934-8281-ac5592a59789"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Dou Ying",
                            Product = "Dou Ying"
                        },
                        new
                        {
                            Id = new Guid("43bd3e7e-7eb1-4b58-9fe1-8f138941e48f"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Jin Ri Tou Tiao",
                            Product = "Jin Ri Tou Tiao"
                        },
                        new
                        {
                            Id = new Guid("a5454237-022a-4ddc-8b42-3ce838641a1a"),
                            Country = "USA",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Apple",
                            Product = "Apple"
                        },
                        new
                        {
                            Id = new Guid("c295771d-b02b-4a28-95a0-0991c14dc6c6"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Hua Wei",
                            Product = "Hua Wei"
                        },
                        new
                        {
                            Id = new Guid("971cab58-bf41-49f9-a6ed-621b463f309a"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "Xiao Mi",
                            Product = "Xiao Mi"
                        },
                        new
                        {
                            Id = new Guid("dfd3241f-6efd-44b3-8f69-439a8c5071a9"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "DI DI",
                            Product = "DI DI"
                        },
                        new
                        {
                            Id = new Guid("df6a6df0-c982-49af-9710-e7cb4c684e26"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "PING DUO DUO",
                            Product = "PING DUO DUO"
                        },
                        new
                        {
                            Id = new Guid("0f582b03-d902-40d3-81e9-55ff97722793"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "AI QI YI",
                            Product = "AI QI YI"
                        },
                        new
                        {
                            Id = new Guid("e2f51543-96a9-47c1-b0b2-2a1c60557d52"),
                            Country = "China",
                            Industry = "Internet",
                            Introduction = "Great  Company",
                            Name = "XING LANG",
                            Product = "XING LANG"
                        });
                });

            modelBuilder.Entity("Routine.Api.Entities.Employee", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<Guid>("CompanyId")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("DateOfBirth")
                        .HasColumnType("TEXT");

                    b.Property<string>("EmployeeNo")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasMaxLength(10);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasMaxLength(50);

                    b.Property<int>("Gender")
                        .HasColumnType("INTEGER");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("Employees");

                    b.HasData(
                        new
                        {
                            Id = new Guid("01ece19e-5d64-42a5-808a-9c4112cb47b8"),
                            CompanyId = new Guid("87b6b176-9f13-4246-a06d-7b1afeabb15b"),
                            DateOfBirth = new DateTime(1986, 11, 4, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            EmployeeNo = "G003",
                            FirstName = "Mary",
                            Gender = 1,
                            LastName = "King"
                        },
                        new
                        {
                            Id = new Guid("1bc1f9fd-967d-4e50-8bd8-99238a2e6297"),
                            CompanyId = new Guid("87b6b176-9f13-4246-a06d-7b1afeabb15b"),
                            DateOfBirth = new DateTime(1977, 4, 6, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            EmployeeNo = "G097",
                            FirstName = "Kevin",
                            Gender = 0,
                            LastName = "Richardson"
                        });
                });

            modelBuilder.Entity("Routine.Api.Entities.Employee", b =>
                {
                    b.HasOne("Routine.Api.Entities.Company", "Company")
                        .WithMany("Employees")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
