﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Routine.Api.Migrations
{
    public partial class AddCompanyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Companies",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Industry",
                table: "Companies",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Product",
                table: "Companies",
                maxLength: 100,
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("0f582b03-d902-40d3-81e9-55ff97722793"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "AI QI YI" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("20e179de-2169-42ee-ac5c-1ba688c78168"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Mei Tuan" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("43bd3e7e-7eb1-4b58-9fe1-8f138941e48f"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Jin Ri Tou Tiao" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("726c7b98-e1e2-42df-9150-d681e9195a76"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "TianMao" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("784cc59a-52b3-4507-b540-4c7d37938751"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Software" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("87b6b176-9f13-4246-a06d-7b1afeabb15b"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "USA", "Software", "Software" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("971cab58-bf41-49f9-a6ed-621b463f309a"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Xiao Mi" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("a5454227-022a-4ddc-8b42-3ce838641a1a"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "USA", "Internet", "Software" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("a5454237-022a-4ddc-8b42-3ce838641a1a"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "USA", "Internet", "Apple" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("a5474227-022a-4ddc-8b42-3ce838641a1a"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Zhi Fu Bao" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("a5acde36-237c-4482-8b43-a9ee29cfab3a"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "JingDong" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("af4743f0-eba0-4934-8281-ac5592a59789"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Dou Ying" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("afd55e56-9548-45b7-899b-eedcd0763779"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Eleme" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("b82d6f84-2c9d-4961-b971-a692b6f6cc06"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Bai Du" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("c295771d-b02b-4a28-95a0-0991c14dc6c6"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Hua Wei" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("dce6c8a4-465a-48db-9e0e-74da91d819b7"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "Microsoft" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("df6a6df0-c982-49af-9710-e7cb4c684e26"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "PING DUO DUO" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("dfd3241f-6efd-44b3-8f69-439a8c5071a9"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "DI DI" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: new Guid("e2f51543-96a9-47c1-b0b2-2a1c60557d52"),
                columns: new[] { "Country", "Industry", "Product" },
                values: new object[] { "China", "Internet", "XING LANG" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Country",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Industry",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Product",
                table: "Companies");
        }
    }
}
