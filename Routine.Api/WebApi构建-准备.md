### WebApi构建-准备

#### 1.上下文准备

##### 1.1上下文实体

- Company代码

```c#
public class Company
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string Country { get; set; }
    public string Industry { get; set; }
    public string Product { get; set; }
    public string Introduction { get; set; }
    public DateTime? BankruptTime { get; set; }
    public ICollection<Employee> Employees { get; set; }
}
```

- Employee代码

```c#
public class Employee
{
    public Guid Id { get; set; }
    public Guid CompanyId { get; set; }
    public string EmployeeNo { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public Gender Gender { get; set; }
    public DateTime DateOfBirth { get; set; }
    public Company Company { get; set; }
}
```

##### 1.2上下文配置

- RoutineDbContext代码

```c#
public class RoutineDbContext : DbContext
{
    public RoutineDbContext(DbContextOptions<RoutineDbContext> options) : 	base(options)
    {

    }
    public DbSet<Company> Companies { get; set; }
    public DbSet<Employee> Employees { get; set; }
     //生成表结构
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>()
                .Property(x => x.Name)
                .IsRequired().HasMaxLength(100);

            modelBuilder.Entity<Company>()
                .Property(x => x.Country)
                .HasMaxLength(50);

            modelBuilder.Entity<Company>()
                .Property(x => x.Industry)
                .HasMaxLength(50);

            modelBuilder.Entity<Company>()
                .Property(x => x.Product)
                .HasMaxLength(100);

            modelBuilder.Entity<Company>()
               .Property(x => x.Introduction)
               .HasMaxLength(500);

            modelBuilder.Entity<Employee>()
              .Property(x => x.EmployeeNo)
              .IsRequired().HasMaxLength(10);

            modelBuilder.Entity<Employee>()
              .Property(x => x.FirstName)
              .IsRequired().HasMaxLength(50);

            modelBuilder.Entity<Employee>()
              .Property(x => x.LastName)
              .IsRequired().HasMaxLength(50);

            modelBuilder.Entity<Employee>()
             .HasOne(navigationExpression: x => x.Company)
             .WithMany(navigationExpression: x => x.Employees)
             .HasForeignKey(x => x.CompanyId).OnDelete(DeleteBehavior.Cascade);


            //设置Company数据
            modelBuilder.Entity<Company>().HasData(
                new Company
                {
                    Id = Guid.Parse("A5454227-022A-4DDC-8B42-3CE838641A1A"),
                    Name = "Microsoft",
                    Introduction = "Great  Company",
                    Country = "USA",
                    Industry = "Internet",
                    Product = "Software"
                },
                 new Company
                 {
                     Id = Guid.Parse("87B6B176-9F13-4246-A06D-7B1AFEABB15B"),
                     Name = "Google",
                     Introduction = "Don't be evil",
                     Country="USA",
                     Industry="Software",
                     Product= "Software"

                 },
                  new Company
                  {
                      Id = Guid.Parse("784CC59A-52B3-4507-B540-4C7D37938751"),
                      Name = "Alipapa",
                      Introduction = "Fubao Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Software"
                  },
                  new Company
                  {
                      //[Guid("B82D6F84-2C9D-4961-B971-A692B6F6CC06")]
                      Id = Guid.Parse("B82D6F84-2C9D-4961-B971-A692B6F6CC06"),
                      Name = "Bai Du",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Bai Du"
                  },
                  new Company
                  {
                      //[Guid("20E179DE-2169-42EE-AC5C-1BA688C78168")]
                      Id = Guid.Parse("20E179DE-2169-42EE-AC5C-1BA688C78168"),
                      Name = "Mei Tuan",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Mei Tuan"
                  },
                  new Company
                  {
                      //[Guid("AFD55E56-9548-45B7-899B-EEDCD0763779")]
                      Id = Guid.Parse("AFD55E56-9548-45B7-899B-EEDCD0763779"),
                      Name = "Eleme",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Eleme"
                  },
                  new Company
                  {
                      //[Guid("A5ACDE36-237C-4482-8B43-A9EE29CFAB3A")]
                      Id = Guid.Parse("A5ACDE36-237C-4482-8B43-A9EE29CFAB3A"),
                      Name = "JingDong",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "JingDong"
                  },
                  new Company
                  {
                      //[Guid("726C7B98-E1E2-42DF-9150-D681E9195A76")]
                      Id = Guid.Parse("726C7B98-E1E2-42DF-9150-D681E9195A76"),
                      Name = "TianMao",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "TianMao"
                  },
                  new Company
                  {
                      Id = Guid.Parse("A5474227-022A-4DDC-8B42-3CE838641A1A"),
                      Name = "Zhi Fu Bao",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Zhi Fu Bao"
                  },
                  new Company
                  {
                      //[Guid("DCE6C8A4-465A-48DB-9E0E-74DA91D819B7")]
                      Id = Guid.Parse("DCE6C8A4-465A-48DB-9E0E-74DA91D819B7"),
                      Name = "Microsoft",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Microsoft"
                  },
                  new Company
                  {
                      //[Guid("AF4743F0-EBA0-4934-8281-AC5592A59789")]
                      Id = Guid.Parse("AF4743F0-EBA0-4934-8281-AC5592A59789"),
                      Name = "Dou Ying",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Dou Ying"
                  },
                  new Company
                  {
                      //[Guid("43BD3E7E-7EB1-4B58-9FE1-8F138941E48F")]
                      Id = Guid.Parse("43BD3E7E-7EB1-4B58-9FE1-8F138941E48F"),
                      Name = "Jin Ri Tou Tiao",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Jin Ri Tou Tiao"
                  },
                  new Company
                  {
                      //
                      Id = Guid.Parse("A5454237-022A-4DDC-8B42-3CE838641A1A"),
                      Name = "Apple",
                      Introduction = "Great  Company",
                      Country = "USA",
                      Industry = "Internet",
                      Product = "Apple"
                  },
                  new Company
                  {
                      //[Guid("C295771D-B02B-4A28-95A0-0991C14DC6C6")]
                      Id = Guid.Parse("C295771D-B02B-4A28-95A0-0991C14DC6C6"),
                      Name = "Hua Wei",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Hua Wei"
                  },
                  new Company
                  {
                      //[Guid("971CAB58-BF41-49F9-A6ED-621B463F309A")]
                      Id = Guid.Parse("971CAB58-BF41-49F9-A6ED-621B463F309A"),
                      Name = "Xiao Mi",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "Xiao Mi"
                  },
                  new Company
                  {
                      //[Guid("DFD3241F-6EFD-44B3-8F69-439A8C5071A9")]
                      Id = Guid.Parse("DFD3241F-6EFD-44B3-8F69-439A8C5071A9"),
                      Name = "DI DI",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "DI DI"
                  },
                  new Company
                  {
                      //[Guid("DF6A6DF0-C982-49AF-9710-E7CB4C684E26")]
                      Id = Guid.Parse("DF6A6DF0-C982-49AF-9710-E7CB4C684E26"),
                      Name = "PING DUO DUO",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "PING DUO DUO"
                  },
                  new Company
                  {
                      //[Guid("0F582B03-D902-40D3-81E9-55FF97722793")]
                      Id = Guid.Parse("0F582B03-D902-40D3-81E9-55FF97722793"),
                      Name = "AI QI YI",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "AI QI YI"
                  },
                  new Company
                  {
                      //[Guid("E2F51543-96A9-47C1-B0B2-2A1C60557D52")]
                      Id = Guid.Parse("E2F51543-96A9-47C1-B0B2-2A1C60557D52"),
                      Name = "XING LANG",
                      Introduction = "Great  Company",
                      Country = "China",
                      Industry = "Internet",
                      Product = "XING LANG"
                  }
                  );

            //设置Employee数据
            modelBuilder.Entity<Employee>().HasData(
             new Employee
             {
                 Id = Guid.Parse("01ECE19E-5D64-42A5-808A-9C4112CB47B8"),
                 CompanyId = Guid.Parse("87B6B176-9F13-4246-A06D-7B1AFEABB15B"),
                 DateOfBirth = new DateTime(1986, 11, 4),
                 EmployeeNo = "G003",
                 FirstName = "Mary",
                 LastName = "King",
                 Gender = Gender.女
             },
             new Employee
             {
                 Id= Guid.Parse("1BC1F9FD-967D-4E50-8BD8-99238A2E6297"),
                 CompanyId = Guid.Parse("87B6B176-9F13-4246-A06D-7B1AFEABB15B"),
                 DateOfBirth = new DateTime(1977, 4, 6),
                 EmployeeNo = "G097",
                 FirstName = "Kevin",
                 LastName = "Richardson",
                 Gender = Gender.男
             }
            );
        }
}
```

RoutineDbContext初始化

​	 :1st_place_medal:每次服务重启都删除原有数据进行配置初始化。

```c#
  public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            using (var scope=host.Services.CreateScope())
            {
                //每次服务重启都删除原有数据进行配置初始化
                try
                {
                    var dbContext = scope.ServiceProvider.GetService<RoutineDbContext>();
                    dbContext.Database.EnsureDeleted();
                    dbContext.Database.Migrate();
                }
                catch (Exception e) 
                {
                    var logger = 	 scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
                    logger.LogError(e, "Database Migration Error");
                }

            }
            host.Run();
        }
```

#####  1.3 上下文注册

```c#
services.AddDbContext<RoutineDbContext>(options =>
{
    options.UseSqlite("Data Source=routine.db");
});
```

#### 2.仓库准备

#####  2.1抽象仓库

ICompanyRepository代码

-  单一公司查询
-  多个公司查询
-  根据条件查询公司
-  添加公司
-  修改公司
-  判断公司是否存在
- 获取公司员工
- 添加员工
- 删除员工
- 修改员工

```c#
public interface ICompanyRepository
{
    Task<PageList<Company>> GetCompanyiesAsync(CompanyDtoParameters parameters);
    Task<Company> GetCompanyAsync(Guid companyId);
    Task<IEnumerable<Company>> GetCompaniesAsync(IEnumerable<Guid> companyIds);
    void AddCompany(Company company);
    void UpdateEmployee(Employee employee);
    void UpdateCompany(Company company);
    void DeleteCompany(Company company);
    void DeleteEmployee(Employee employee);
    Task<bool> CompanyExistsAsync(Guid companyId);
    Task<IEnumerable<Employee>> GetEmployeesAsync(Guid companyId,EmployeeDtoParameters parameters/*string genderDisplay,string q*/);
    Task<Employee> GetEmployeeAsync(Guid companyId, Guid employeeId);
    void AddEmployee(Guid companyId, Employee employee);

    Task<bool> SaveAsync();
}
```

- CompanyRepository代码

```c#
  public class CompanyRepository : ICompanyRepository
    {
        private readonly RoutineDbContext _context;
        private readonly IPropertyMappingService _propertyMappingService;

        public CompanyRepository(RoutineDbContext context, IPropertyMappingService propertyMappingService)
        {
            _context = context ?? throw new ArgumentException(nameof(context));
            _propertyMappingService= propertyMappingService?? throw new ArgumentException(nameof(propertyMappingService));
        }
        //添加公司
        public void AddCompany(Company company)
        {
            if (company == null)
            {
                throw new ArgumentNullException(nameof(company));
            }
            company.Id = Guid.NewGuid();
            if (company.Employees != null)
            {
                foreach (var employee in company.Employees)
                {
                    employee.Id = Guid.NewGuid();
                }
            }
           
            _context.Companies.Add(company);
        }
        //公司添加一个员工
        public void AddEmployee(Guid companyId, Employee employee)
        {
            if (companyId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(companyId));
            }
            if (employee == null)
            {
                throw new ArgumentNullException(nameof(employee));
            }
            employee.CompanyId = companyId;
            _context.Employees.Add(employee);


        }
        //判断是否存在
        public async  Task<bool> CompanyExistsAsync(Guid companyId)
        {
            if (companyId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(companyId));
            }
            return await _context.Companies.AnyAsync(predicate:x => x.Id == companyId);
        }
        //删除公司
        public void DeleteCompany(Company company)
        {
            if (company == null)
            {
                throw new ArgumentNullException(nameof(company));
            }
            _context.Companies.Remove(company);
        }

        public void DeleteEmployee(Employee employee)
        {
            _context.Employees.Remove(employee);
        }

        //批量获取公司
        public async Task<IEnumerable<Company>> GetCompaniesAsync(IEnumerable<Guid> companyIds)
        {
            if (companyIds == null)
            {
                throw new ArgumentNullException(nameof(companyIds));
            }
            return await _context.Companies
                .Where(x => companyIds.Contains(x.Id))
                .OrderBy(x => x.Name)
                .ToListAsync();
        }
        //获取指定公司
        public async Task<Company> GetCompanyAsync(Guid companyId)
        {
            if (companyId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(companyId));
            }
            return await _context.Companies.FirstOrDefaultAsync(predicate: x => x.Id == companyId);
        }
        //获取所有公司
        public async Task<PageList<Company>> GetCompanyiesAsync(CompanyDtoParameters parameters)
        {
            if (parameters == null)
            {
                throw new ArgumentNullException(nameof(parameters));
            }
            //if(string.IsNullOrWhiteSpace(parameters.CompanyName)&&
            //    string.IsNullOrWhiteSpace(parameters.SearchTerm))
            //{
            //    return await _context.Companies.ToListAsync() ;
            //}
            var queryExpression = _context.Companies as IQueryable<Company>;

            if (!string.IsNullOrWhiteSpace(parameters.CompanyName))
            {
                parameters.CompanyName = parameters.CompanyName.Trim();
                queryExpression = queryExpression.Where(x => x.Name == parameters.CompanyName);
            }
            if (!string.IsNullOrWhiteSpace(parameters.SearchTerm))
            {
                parameters.SearchTerm = parameters.SearchTerm.Trim();
                queryExpression = queryExpression.Where(x => x.Name.Contains(parameters.SearchTerm) ||
                  x.Introduction.Contains(parameters.SearchTerm));
            }
            //queryExpression = queryExpression.Skip(parameters.PageSize * (parameters.PageNumber - 1))
            //    .Take(parameters.PageSize);
            var mappingDictionary = _propertyMappingService.GetPropertyMapping<CompanyDto, Company>();
            queryExpression = queryExpression.ApplySort(parameters.OrderBy, mappingDictionary);
            return await PageList<Company>.CreateAsync(queryExpression, parameters.PageNumber, parameters.PageSize);//queryExpression.ToListAsync();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public async  Task<Employee> GetEmployeeAsync(Guid companyId, Guid employeeId)
        {
            if (companyId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(companyId));
            }
            if (employeeId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(employeeId));
            }
            return await
                _context.Employees.Where(x => x.CompanyId == companyId && x.Id == employeeId)
                .FirstOrDefaultAsync();
        }
        /// <summary>
        /// 获取公司所有员工 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="genderDisplay"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Employee>> GetEmployeesAsync(Guid companyId,EmployeeDtoParameters parameters/*string genderDisplay,string q*/)
        {
            if (companyId == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(companyId));
            }
            //if (string.IsNullOrWhiteSpace(parameters.Gender) && string.IsNullOrWhiteSpace(parameters.Q))
            //{
            //    return await _context.Employees
            //  .Where(x => x.CompanyId == companyId)
            //  .OrderBy(x => x.EmployeeNo)
            //  .ToListAsync();
            //}

            //组装SQL
            var items = _context.Employees.Where(x => x.CompanyId == companyId);

            if (!string.IsNullOrWhiteSpace(parameters.Gender))
            {
                parameters.Gender = parameters.Gender.Trim();
                var gender = Enum.Parse<Gender>(parameters.Gender);
                items = items.Where(x => x.Gender == gender);
            }

            if (!string.IsNullOrWhiteSpace(parameters.Q))
            {
                parameters.Q = parameters.Q.Trim();
                items = items.Where(x => x.EmployeeNo.Contains(parameters.Q) ||
                  x.FirstName.Contains(parameters.Q)
                  || x.LastName.Contains(parameters.Q));
            }
            //if (!string.IsNullOrWhiteSpace(parameters.OrderBy))
            //{
            //    if (parameters.OrderBy.ToLowerInvariant() == "name")
            //    {
            //        items = items.OrderBy(x => x.FirstName).ThenBy(x => x.LastName);
            //    }
            //}
            //扩展排序
            //items.ApplySort(parameters.OrderBy,mappingDictionary);
            var mappingDictionary = _propertyMappingService.GetPropertyMapping<EmployeeDto, Employee>();
            items=items.ApplySort(parameters.OrderBy, mappingDictionary);
            return await items/*.OrderBy(x => x.EmployeeNo)*/.ToListAsync();

        }
        //保存
        public async Task<bool> SaveAsync()
        {
            return await _context.SaveChangesAsync() >= 0;
        }

        public void UpdateCompany(Company company)
        {
            //throw new NotImplementedException();
        }

        public void UpdateEmployee(Employee employee)
        {
            //throw new NotImplementedException();
        }
    }
```



##### 	2.2仓库注册

   ```c#
//请求生命周期 依赖注入
services.AddScoped<ICompanyRepository, CompanyRepository>();
   ```



#### 3.AutoMapper准备

#####  3.1AutoMapper注册

```c#
//AutoMapper配置
services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
```



##### 3.2CompanyProfile协议类

```c#
public class CompanyProfile:Profile
{
    //映射关系
    //AutoMap基于约定
    public CompanyProfile()
    {
       //TSource, TDestination
       //含义：上层可以将Company适配为CompanyDto
        CreateMap<Company, CompanyDto>().ForMember(dest=>dest.CompanyName,
            opt=>opt.MapFrom(src=>src.Name));
       //var returnDto = _mapper.Map<CompanyDto>(entity);
        CreateMap<CompanyAddDto, Company>();
        CreateMap<Company, CompanyFullDto>();
        CreateMap<CompanyAddWithBankruptTimeDto, Company>();
    }
}
```

- EmployeeProfile协议类

```c#
 public class EmployeeProfile:Profile
    {
        public EmployeeProfile()
        {
            //含义：上层可以将Employee适配为EmployeeDto
            CreateMap<Employee, EmployeeDto>()
                    .ForMember(dest => dest.Name,
                                    opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"))
                    .ForMember(dest => dest.GenderDisplay, 
                                    opt => opt.MapFrom(src => src.Gender.ToString()))
                    .ForMember(dest => dest.Age,
                                     opt => opt.MapFrom(src => DateTime.Now.Year - src.DateOfBirth.Year));

            //含义：上层可以将EmployeeAddDto适配为Employee
            CreateMap<EmployeeAddDto, Employee>();

            //含义：上层可以将EmployeeUpdateDto适配为Employee
            CreateMap<EmployeeUpdateDto, Employee>();

           //含义：上层可以将Employee适配为EmployeeUpdateDto
            CreateMap<Employee, EmployeeUpdateDto>();
        }
    }
```



#### 4.输入输出格式

###### 4.1输入输出格式注册

```c#
//添加输入和输出xml格式器
            services.AddControllers(setup =>
            {
                //客户端 Accept请求XML格式
                //服务端只有JSON格式 会返回406
                setup.ReturnHttpNotAcceptable = true;
                //默认格式是Json 加了支持XML
                //setup.OutputFormatters.Add(new XmlDataContractSerializerOutputFormatter());
                //客户端没有指定格式 默认返回XML
                //setup.OutputFormatters.Insert(0, new XmlDataContractSerializerOutputFormatter());

                //缓存配置
                setup.CacheProfiles.Add("120sCacheProfile", new CacheProfile
                {
                    Duration = 120
                }) ;
            }).AddNewtonsoftJson(setup=> {
                setup.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }).AddXmlDataContractSerializerFormatters();  //添加了输入输出
```

