﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WindowsServiceEntry
{
    public partial class OnlyServer : ServiceBase
    {   
        private readonly string _logFilePath = @"D:\OnlyServerLog.txt";

        public OnlyServer()
        {
            InitializeComponent();
        }
        
        protected override void OnStart(string[] args)
        {
            using (FileStream stream = new FileStream(_logFilePath, FileMode.Append))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.WriteLine($"{DateTime.Now},服务启动！");
            }
        }

        protected override void OnStop()
        {
            using (FileStream stream = new FileStream(_logFilePath, FileMode.Append))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.WriteLine($"{DateTime.Now},服务停止！");
            }
        }
    }
}
