﻿namespace WindowsServiceClient
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Install = new System.Windows.Forms.Button();
            this.btn_Start = new System.Windows.Forms.Button();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.btn_UnInstall = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Install
            // 
            this.btn_Install.Location = new System.Drawing.Point(28, 29);
            this.btn_Install.Name = "btn_Install";
            this.btn_Install.Size = new System.Drawing.Size(124, 39);
            this.btn_Install.TabIndex = 0;
            this.btn_Install.Text = "安装服务";
            this.btn_Install.UseVisualStyleBackColor = true;
            this.btn_Install.Click += new System.EventHandler(this.btn_Install_Click);
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(168, 29);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(124, 39);
            this.btn_Start.TabIndex = 1;
            this.btn_Start.Text = "启动服务";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // btn_Stop
            // 
            this.btn_Stop.Location = new System.Drawing.Point(309, 29);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(124, 39);
            this.btn_Stop.TabIndex = 2;
            this.btn_Stop.Text = "停止服务";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // btn_UnInstall
            // 
            this.btn_UnInstall.Location = new System.Drawing.Point(448, 29);
            this.btn_UnInstall.Name = "btn_UnInstall";
            this.btn_UnInstall.Size = new System.Drawing.Size(124, 39);
            this.btn_UnInstall.TabIndex = 3;
            this.btn_UnInstall.Text = "卸载服务";
            this.btn_UnInstall.UseVisualStyleBackColor = true;
            this.btn_UnInstall.Click += new System.EventHandler(this.btn_UnInstall_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 90);
            this.Controls.Add(this.btn_UnInstall);
            this.Controls.Add(this.btn_Stop);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.btn_Install);
            this.Name = "Form1";
            this.Text = "服务操作";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Install;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.Button btn_UnInstall;
    }
}

