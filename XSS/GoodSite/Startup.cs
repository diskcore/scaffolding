using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GoodSite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                {
                    options.LoginPath = "/home/login";
                    //可以通过脚本读取Cookie
                    options.Cookie.HttpOnly = true;
                });
            services.AddControllersWithViews();


            //防止跨站脚本攻击 在Header的值传入Cookie的值
            services.AddAntiforgery(options =>
            {
                options.HeaderName = "X-CSRF-TOKEN";
            });


            //开启全局AntiforgeryToken验证  全局范围内防跨站脚本攻击 POST
            //services.AddMvc(options => options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute()));




            services.AddCors(options =>
            {
                //配置api的跨域策略
                options.AddPolicy("api", builder =>
                 {
                     //允许任何域就不能允许携带身份验证
                     builder.WithOrigins("https://localhost:5001")  //允许跨域站点
                     .AllowAnyHeader()                              //允许任何Header
                     .AllowCredentials()                            //允许携带证书
                     .WithExposedHeaders("abc");                    //允许脚本能访问响应的Header的列表

                     
                     //允许任何源允许携带身份信息允许任何请求头
                     builder.SetIsOriginAllowed(orgin => true).AllowCredentials().AllowAnyHeader();
                 });
                //全局api 不用设置属性头
                //options.AddDefaultPolicy
            });
            #endregion
            //内存缓存
            services.AddMemoryCache();
            //redis缓存
            services.AddStackExchangeRedisCache(options =>
            {
                Configuration.GetSection("RedisCache").Bind(options);
            });
            //ASP.NET Core 响应缓存
            services.AddResponseCaching();
            //EasyCaching缓存
            services.AddEasyCaching(options =>
            {
                options.UseRedis(Configuration, name: "easycaching");
            });

        }

        //public void ConfigureContainer(ContainerBuilder builder)
        //{

        //}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            //ASP.NET Core 响应中间件  启用AddResponseCaching 与身份认证冲突的
            app.UseResponseCaching();

            //跨域的中间件 在Routing之后 身份认证之前
            app.UseCors();

            //包含身份认证的请求 ResponseCaching是不会生效的
            //app.UseAuthentication();
            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
