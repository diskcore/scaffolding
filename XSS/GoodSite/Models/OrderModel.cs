﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoodSite.Models
{
    public class OrderModel
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }
    }
}
