﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JWT
{
    public class JWTBase
    {
        private Claim[] _claims;
        //密钥
        private SymmetricSecurityKey _symmetricSecurityKey;
        //证书
        private SigningCredentials _signingCredentials;
        //Token
        private JwtSecurityToken _jwtSecurityToken;
        //加密算法
        private string _algorithm = SecurityAlgorithms.HmacSha256;
        public JWTBase(string secreKey, string algorithm)
        {
             _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secreKey));
            _signingCredentials= new SigningCredentials(_symmetricSecurityKey, algorithm);
            
        }
        public string Sing(Claim[] claims,string issuer,string Audience, int outTime = 720)
        {
            _jwtSecurityToken = new JwtSecurityToken(issuer, Audience,
                claims,
                DateTime.Now,
                DateTime.Now.AddMinutes(outTime), _signingCredentials);
            return new JwtSecurityTokenHandler().WriteToken(_jwtSecurityToken);
        }
    }
}
