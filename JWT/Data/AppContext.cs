﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWT.Data
{
    public class AppContext:DbContext
    {
        DbSet<User> Users { get; set; }
        DbSet<UserRefreshToken> UserRefreshTokens { get; set; }
    }
}
