﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JWT.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        [HttpGet("api/[controller]")]
        public string GetCurrentTime()
        {
            return DateTime.Now.ToString("yyyy-MM-dd");
        }
    }
}
