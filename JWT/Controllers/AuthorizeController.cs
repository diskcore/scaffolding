﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;

namespace JWT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizeController : ControllerBase
    {
        private JWTSettings _jWTSettings;
        private readonly IHttpContextAccessor _context;


        public AuthorizeController(IOptions<JWTSettings> _jWTSettingsAccesser,IHttpContextAccessor context)
        {
            _jWTSettings = _jWTSettingsAccesser.Value;
            _context = context;
        }
        [HttpPost]
        [Route("Token")]
        public IActionResult Token(LoginViewModel viewModel)
        {
             if (ModelState.IsValid)
            {
                if (!(viewModel.User == "zhanzebin" && viewModel.Password == "123456"))
                {
                    return BadRequest();
                }
                var claims = new Claim[]
                {
                    new Claim(ClaimTypes.Name,"zhanzebin"),
                    new Claim(ClaimTypes.Role,"admin"),
                    new Claim("Id","9527"),
                    new Claim("Company","GreenWork")
                };
                JWTBase jWTBase = new JWTBase(_jWTSettings.SecreKey, SecurityAlgorithms.HmacSha256);
                return Ok(new { token = jWTBase.Sing(claims,_jWTSettings.Issuer, _jWTSettings.Audience)/*new JwtSecurityTokenHandler().WriteToken(token)*/ });
            }
            return BadRequest();
        }
        [Microsoft.AspNetCore.Authorization.Authorize]
        [HttpPost]
        [Route("Login")]
        public IActionResult Login()
        {
            //解析Claim参数
            var id = _context.HttpContext.User.FindFirst("Id");
            var company = _context.HttpContext.User.FindFirst("Company");
            var role = _context.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role);
            return Ok();
        }
    }
}