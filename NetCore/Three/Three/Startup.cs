using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Three.Services;

namespace Three
{
    public class Startup
    {
        private IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            Console.WriteLine("Startup");
            _configuration = configuration;
        }



        /// <summary>
        /// 服务注册
        /// 依赖注入的核心类
        /// IServiceCollection
        /// ServiceDescriptor
        /// IServiceProvider
        /// IServiceScope 子容器的生命周期
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            #region 注册不同生命周期的服务
            //单例
            //services.AddSingleton<IMySingletonService, MySingletonService>();
            //上下文
            //services.AddScoped<IMyScopeService, MyScopeService>();
            //瞬时
            //services.AddTransient<IMyTransientService, MyTransientService>();
            #endregion


            #region 花式注册
            //services.AddSingleton<IOrderService>(new OrderService());
            //services.AddSingleton<IOrderService, OrderService>();
            //工厂方式注册
            //services.AddSingleton<IOrderService>(serviceProcider =>
            //{
            //    return new OrderServiceEx();
            //});
            //services.AddTransient<IOrderService>(serviceProcider =>
            //{
            //    //可以从容器中获取
            //    //serviceProcider.GetService<>
            //    return new OrderServiceEx();
            //});
            #endregion

            #region 尝试注册
            //服务如果已经注册过了 就不再注册了
            //services.TryAddSingleton<IOrderService, OrderService>();
            //避免重复注册
            //services.TryAddEnumerable(ServiceDescriptor.Singleton<IOrderService, OrderService>());
            // services.TryAddEnumerable(ServiceDescriptor.Singleton<IOrderService, OrderServiceEx>());
            //services.TryAddEnumerable(ServiceDescriptor.Singleton<IOrderService>(new OrderService()));
            //services.TryAddEnumerable(ServiceDescriptor.Singleton<IOrderService>(p=> { 
            // return new OrderServiceEx();
            //}));
            #endregion

            #region 移除和替换注册
            //services.RemoveAll<IOrderService>();
            //services.Replace(ServiceDescriptor.Singleton<IOrderService, OrderServiceEx>());
            #endregion

            #region 注册泛型模板
            //services.AddSingleton((typeof(IGenericService<>)), (typeof(GenericService<>)));
            #endregion

            #region Disposable
            services.AddTransient<IOrderService, OrderService>();
            //services.AddScoped<IOrderService, OrderService>();
            //var ser = new OrderService();
            //services.AddSingleton<IOrderService>(ser);
            //services.AddSingleton<IOrderService, OrderService>();
            #endregion
            //Console.WriteLine("Startup.ConfigureServices");
            //MVC
            services.AddControllersWithViews();
            //API
            //services.AddControllers();
            services.AddSingleton<IClock, ChinaClock>();
        }

        /// <summary>
        /// 中间件
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //根容器获取 一直堆积不释放  注意区分根容器和子容器(请求容器)
            var s = app.ApplicationServices.GetService<IOrderService>();
            //结果如下
            //Application is shutting down...
            //DisposableOrderService Disposable:61799993
            Console.WriteLine("Startup.Configure");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //静态文件访问
            app.UseStaticFiles();

            app.UseHttpsRedirection();

            //身份认证
            app.UseAuthentication();

            //路由中间件
            app.UseRouting();

            //端点中间件
            app.UseEndpoints(endpoints =>
            {
                //默认形式
                //endpoints.MapGet("/", async context =>
                //{
                //    await context.Response.WriteAsync("Hello World!");
                //});
                //MVC模式使用路由表
                endpoints.MapControllerRoute("default",
                    "{controller=Home}/{action=GetServiceList}/{id?}");
                //不使用路由表
                endpoints.MapControllers();
            });
        }
    }
}
