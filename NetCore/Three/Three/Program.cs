using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Three.Services;

namespace Three
{
    /// <summary>
    /// DI只负责释放由其创建的对象实例
    /// DI在容器或子容器释放时，释放由其创建的对象实例
    /// 建议：
    /// 避免在根容器获取实现了IDisposable接口的瞬时服务
    /// 避免手动创建实现了IDisposable对象，应该使用容器来管理其生命周期
    /// 
    /// </summary>
    public class Program
    {
        /// <summary>
        /// 掌握启动流程
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
        /// <summary>
        /// 启动顺序
        /// 1.ConfigureWebHostDefaults
        /// 2.ConfigureHostConfiguration
        /// 3.ConfigureAppConfiguration 自己的配置文件
        /// 4.ConfigureServices
        /// 5.Startup.ConfigureServices
        /// 6.Startup
        /// 7.Startup.ConfigureServices.
        /// 8.Startup.Configure 注入中间件
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(builder =>
                {
                    Console.WriteLine("ConfigureAppConfiguration");
                })
                .ConfigureServices(service =>
                {
                    Console.WriteLine("ConfigureServices");
                })
                .ConfigureHostConfiguration(builder =>
                {
                    Console.WriteLine("ConfigureHostConfiguration");
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    Console.WriteLine("ConfigureWebHostDefaults");
                    webBuilder.UseStartup<Startup>();
                //    webBuilder.ConfigureServices(services =>
                //    {
                //        Console.WriteLine("Startup.ConfigureServices");
                //        services.AddControllersWithViews();
                //        //services.AddControllers();
                //        services.AddSingleton<IClock, ChinaClock>();
                //    });
                //    webBuilder.Configure(app =>
                //    {
                //        Console.WriteLine("Startup.Configure");
                     

                //        //静态文件访问
                //        app.UseStaticFiles();

                //        app.UseHttpsRedirection();

                //        //身份认证
                //        app.UseAuthentication();

                //        //路由中间件
                //        app.UseRouting();

                //        //端点中间件
                //        app.UseEndpoints(endpoints =>
                //        {
                //            //默认形式
                //            //endpoints.MapGet("/", async context =>
                //            //{
                //            //    await context.Response.WriteAsync("Hello World!");
                //            //});
                //            //MVC模式使用路由表
                //            endpoints.MapControllerRoute("default",
                //                "{controller=Home}/{action=Index}/{id?}");
                //            //不使用路由表
                //            endpoints.MapControllers();
                //        });
                //    });
                });
    }
}
