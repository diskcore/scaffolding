﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Three.Services;

namespace Three.Controllers
{
    public class HomeController:Controller
    {
        //IGenericService<IOrderService> _genericService;
        //IOrderService _orderService;

        /// <summary>
        /// 构造函数注入
        /// </summary>
        /// <param name="clock"></param>
        //public HomeController(IOrderService  orderService,IGenericService<IOrderService> genericService)
        //{
        //    _genericService = genericService;
        //    _orderService = orderService;
        //}
        public HomeController()
        {
            //_genericService = genericService;
            //_orderService = orderService;
        }
        /// <summary>
        /// FromServices：从容器获取对象
        /// </summary>
        /// <param name="singleton1"></param>
        /// <param name="singleton2"></param>
        /// <param name="transient1"></param>
        /// <param name="transient2"></param>
        /// <param name="scoped1"></param>
        /// <param name="scoped2"></param>
        /// <returns></returns>
        public string Index([FromServices]IMySingletonService singleton1,
            [FromServices] IMySingletonService singleton2,
            [FromServices] IMyTransientService transient1,
            [FromServices] IMyTransientService transient2,
            [FromServices] IMyScopeService scoped1,
            [FromServices] IMyScopeService scoped2)
        {
            Console.WriteLine($"singleton1:{singleton1.GetHashCode()}");
            Console.WriteLine($"singleton2:{singleton2.GetHashCode()}");

            Console.WriteLine($"transient1:{transient1.GetHashCode()}");
            Console.WriteLine($"transient2:{transient2.GetHashCode()}");

            Console.WriteLine($"scoped1:{scoped1.GetHashCode()}");
            Console.WriteLine($"scoped2:{scoped2.GetHashCode()}");

            //结果
            //singleton1: 14645893
            //singleton2: 14645893
            //transient1: 57902434
            //transient2: 26140383
            //scoped1: 65122748
            //scoped2: 65122748
            //请求结束
            //singleton1:14645893
            //singleton2: 14645893
            //transient1: 41348378
            //transient2: 17561922
            //scoped1: 48950176
            //scoped2: 48950176
            //请求结束


            //Console.WriteLine("请求结束");
           
            return "Hello Word";
        }

        //public int GetServiceList([FromServices]IEnumerable<IOrderService> services)
        //{
        //    foreach (var item in services)
        //    {
        //        Console.WriteLine($"获取到服务实例：{item.ToString()}:{item.GetHashCode()}");
        //    }
        //    return 1;
        //}
        public int GetServiceList(
            //[FromServices] IOrderService orderService1,
            //[FromServices] IOrderService orderService2,
            [FromServices] IHostApplicationLifetime hostApplicationLifetime,  //管理生命周期
            [FromQuery]bool stop=false)
        {
            //Console.WriteLine("=========1=========");
            ////当前请求的容器 是一个子容器
            //using (IServiceScope scope=HttpContext.RequestServices.CreateScope())
            //{
            //    //孙子容器
            //    var service = scope.ServiceProvider.GetService<IOrderService>();
            //    var service2 = scope.ServiceProvider.GetService<IOrderService>();
            //}
            //Console.WriteLine("=========2=========");

            //Transient结果如下：// 避免在根容器获取实现了IDisposable接口的瞬时服务
            //========= 1 =========
            //DisposableOrderService Disposable: 7167227
            //DisposableOrderService Disposable:16294043
            //=========2=========
            //DisposableOrderService Disposable:64254500
            //DisposableOrderService Disposable:55993668
            //scope结果如下
            //========= 1 =========
            //DisposableOrderService Disposable: 54172779
            //=========2=========
            //DisposableOrderService Disposable:21170186
            if (stop)
            {
                //关掉应用程序
                hostApplicationLifetime.StopApplication();
                //输出结果
                //var ser = new OrderService();
                //services.AddSingleton<IOrderService>(ser);
                //info: Microsoft.Hosting.Lifetime[0]
                //Application is shutting down...

                //输出结果
                //services.AddSingleton<IOrderService, OrderService>();
                //Application is shutting down...
                //DisposableOrderService Disposable:7167227
            }
            //sington结果
            //========= 1 =========
            //=========2=========
            //Console.WriteLine("接口请求处理完毕！");
            return 1;
        }
    }
}
