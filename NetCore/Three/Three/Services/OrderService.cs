﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Three.Services
{
    public interface IOrderService { }
    public class OrderService : IOrderService, IDisposable
    {
        public void Dispose()
        {
            Console.WriteLine($"DisposableOrderService Disposable:{this.GetHashCode()}");
            //DisposableOrderService Disposable:13896890
            //DisposableOrderService Disposable:23399238
        }
    }
    public class OrderServiceEx : IOrderService
    {
    }
}
