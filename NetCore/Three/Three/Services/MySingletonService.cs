﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Three.Services
{
    /// <summary>
    /// 单例
    /// </summary>
    public interface IMySingletonService { }
    public class MySingletonService: IMySingletonService
    {
    }
}
