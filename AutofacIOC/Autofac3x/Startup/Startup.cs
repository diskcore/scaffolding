using System;
using System.Collections.Generic;
using System.Linq;
//using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Autofac;
using Autofac3x.Configuration;
using AutoMapper;
using IGeekFan.AspNetCore.Knife4jUI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
//using Newtonsoft.Json.Serialization;
using Zebin.Aop.Filter;
using Zebin.Extensions;
using Zebin.Startup.Configuration;
using Zebin.v1.Books;

namespace Zebin.Startup
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddFreeSql(Configuration);
            services.AddAutoMapper(typeof(BookProfile).Assembly);
            services.AddControllers(options=> {
                options.Filters.Add<LinCmsExceptionFilter>();
            }).AddNewtonsoftJson(opt =>
            {
                //opt.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:MM:ss";
                // 设置自定义时间戳格式
                opt.SerializerSettings.Converters = new List<JsonConverter>()
                    {
                        new LinCmsTimeConverter()
                    };
                // 设置下划线方式，首字母是小写
                //opt.SerializerSettings.ContractResolver = new DefaultContractResolver()
                //{
                //    NamingStrategy = new SnakeCaseNamingStrategy()
                //    {
                //        ProcessDictionaryKeys = true
                //    },
                //};
            });

            services.AddSwaggerGenNewtonsoftSupport();
            services.AddSwaggerGen();


        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new RepositoryModule());
            builder.RegisterModule(new ServiceModule());
            builder.RegisterModule(new AutofacModule(Configuration));
            builder.RegisterModule(new DependencyModule());
        }



        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                c.SwaggerEndpoint("/swagger/cms/swagger.json", "cms");
                c.RoutePrefix = string.Empty;

                //c.OAuthClientId(Configuration["Service:ClientId"]);
                //c.OAuthClientSecret(Configuration["Service:ClientSecret"]);
                //c.OAuthAppName(Configuration["Service:Name"]);

                c.ConfigObject.DisplayOperationId = true;

            });

            app.UseKnife4UI(c =>
            {
                c.DocumentTitle = "LinCms博客模块";
                //c.InjectStylesheet("https://msg.cnblogs.com/dist/css/_layout.min.css?v=ezgneaXFURlAPIyljTcfnt1m6QVAsZbvftva5pFV8cM");
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                c.SwaggerEndpoint("/swagger/cms/swagger.json", "cms");
                //c.OAuthClientSecret(Configuration["Service:ClientSecret"]);
                //c.OAuthClientId(Configuration["Service:ClientId"]);
                //c.OAuthAppName(Configuration["Service:Name"]);
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
