﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zebin.Security
{
    public static class LinCmsClaimTypes
    {
        public static string Groups { get; set; } = "LinCmsClaimTypes.Groups";
        public static string IsActive { get; set; } = "LinCmsClaimTypes.IsActive";
        public static string IsAdmin { get; set; } = "LinCmsClaimTypes.IsAdmin";
    }
}
