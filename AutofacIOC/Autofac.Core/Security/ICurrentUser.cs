﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Zebin.Security
{
    public  interface ICurrentUser
    {
        long? Id { get; }

        string UserName { get; }
        long[] Groups { get; }


        Claim FindClaim(string claimType);

        Claim[] FindClaims(string claimType);

        Claim[] GetAllClaims();


        bool IsInGroup(long groupId);
    }
}
