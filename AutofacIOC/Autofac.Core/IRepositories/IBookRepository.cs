﻿using System;
using System.Collections.Generic;
using System.Text;
using Zebin.Entities;

namespace Zebin.IRepositories
{
    public interface IBookRepository : IAuditBaseRepository<Book>
    {

    }
}
