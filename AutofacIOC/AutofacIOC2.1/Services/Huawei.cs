﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutofacIOC.Services
{
    public class Huawei : MobilePhone
    {
        public Huawei()
        {
            Property = Guid.NewGuid();
        }
        public Guid Property { get; set; }

        public List<string> GetList(string version)
        {
            return new List<string> { "P30", "P90", "Novia5" };
        }
    }
}
