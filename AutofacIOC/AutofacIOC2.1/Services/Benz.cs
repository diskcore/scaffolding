﻿using Autofac.Extras.DynamicProxy;
using AutofacIOC.AOP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutofacIOC.Services
{
    [Intercept(typeof(HighAOP))]
    [Intercept(typeof(StandardAOP))]
    public class Benz : Car
    {
        public Benz()
        {
            Property = Guid.NewGuid();
        }
        public Guid Property { get; set; }

        public List<string> GetList(string version)
        {
            return new List<string> { "Benz v260", "Benz E30 ", "Benz c63" };
        }
    }
}
