﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutofacIOC.Services
{
    public interface Car
    {
        //获取属性
        Guid Property { get; }
        //获取子列表
        List<string> GetList(string version);
    }
}
