﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutofacIOC.AOP;
using AutofacIOC.IOCModule;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutofacIOC.Autofac
{
    //Nuget
    //1.Autofac.Extensions.DependencyInjection
    //2.Autofac.Extras.DynamicProxy
    public class AutofacIOCBase
    {
        //autofac容器
        private ContainerBuilder _containerBuilder = new ContainerBuilder();
        private IServiceCollection _services;

        //Service容器置换
        private AutofacServiceProvider _autofacServiceProvider;
        public AutofacIOCBase(IServiceCollection services)
        {
            _services = services;

        }
        //IOC模块注册默认模块注册
        public void RegisterDeafultModule()
        {
            _containerBuilder.RegisterModule<DeafultModule>();
        }

        //开启属性注入模式
        //1.用ServiceBasedControllerActivator替换DefaultControllerActivator
        //2.必须AddMvc()之前
        //如果启用这项 所有service是Register将无效

        public void SwitchReplace()
        {
            _services.Replace(ServiceDescriptor.Transient<IControllerActivator, ServiceBasedControllerActivator>());
        }

        //返回Autofac容器
        public AutofacServiceProvider GetAutofacServiceProvider()
        {
            _containerBuilder.Populate(_services);
            var container = _containerBuilder.Build();
            _autofacServiceProvider = new AutofacServiceProvider(container);
            return _autofacServiceProvider;
        }
        //瞬时注册 适合微服务 轻架构
        public void RegisterTransient()
        {
            //_services.AddTransient<Car, Benz>();
        }
        //作用域 注册
        public void RegisterScoped()
        {
            //_services.AddScoped<MobilePhone, Huawei>();
        }
        //全局唯一 注册
        public void RegisterSingleton()
        {
            //_servickes.AddSingleton<Car, Benz>();
        }
        //注册 AOP
        public void RegisterAOP()
        {
            _containerBuilder.RegisterType<HighAOP>();
            _containerBuilder.RegisterType<StandardAOP>();
        }
    }
}
