﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using AutofacIOC.Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AutofacIOC2._1
{
    /// <summary>
    /// Autofac
    /// 基于名称的注入
    /// 
    /// 属性注入
    /// 
    /// 子容器
    /// 
    /// 基于动态代理AOP
    /// 核心扩展点
    /// public interface IServerProviderFactory<TContainerBuilder>
    /// Autofac.Extensions.DependencyInjection
    /// Autofac.Extras.DynamicProxy
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            //IOC注入
            AutofacIOCBase autofacIOC = new AutofacIOCBase(services);
            //IOC注入默认模块
            autofacIOC.RegisterDeafultModule();
            //IOC注入AOP
            autofacIOC.RegisterAOP();
            return autofacIOC.GetAutofacServiceProvider();


        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
