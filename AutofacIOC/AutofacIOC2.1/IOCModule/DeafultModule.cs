﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using AutofacIOC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AutofacIOC.IOCModule
{
    public class DeafultModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Benz>().As<Car>().EnableInterfaceInterceptors();
        }
    }
}
