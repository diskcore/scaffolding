﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Autofac3.Services
{
    public interface IMyService
    {
        void ShowCode();
    }
    public class MyService : IMyService
    {
        public void ShowCode()
        {
            Console.WriteLine($"MyService.ShowCode:{GetHashCode()}");
        }
    }
    public class MyService2 : IMyService
    {
        public MyNameService NameService { get; set; }
        //没有属性注入的结果：
        //MyService2.ShowCode:1815156,NameService是否为空：True
        //有属性注入的结果
        //
        public void ShowCode()
        {
            Console.WriteLine($"MyService2.ShowCode:{GetHashCode()},NameService是否为空：{NameService == null}");
        }
    }
    public class MyNameService
    {

    }
}
