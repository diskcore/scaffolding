﻿using Castle.DynamicProxy;

namespace Autofac3
{
    public class MyInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            System.Console.WriteLine($"Intercept before,Method:{invocation.Method.Name}");
            invocation.Proceed();
            System.Console.WriteLine($"Intercept  after,Method:{invocation.Method.Name}");

        }
    }
}