using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Extras.DynamicProxy;
using Autofac3.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Autofac3
{
    /// <summary>
    /// Autofac.Extensions.DependencyInjection
    /// Autofac.Extras.DynamicProxy
    /// </summary>
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }
        public void ConfigureContainer(ContainerBuilder builder)
        {
            //普通注册
            //builder.RegisterType<MyService>().As<IMyService>();

            //命名注册
            //builder.RegisterType<MyService2>().Named<IMyService>("service2");

            //属性注册
            //builder.RegisterType<MyNameService>();
            //builder.RegisterType<MyService2>().As<IMyService>().PropertiesAutowired();

            //AOP注册
            //builder.RegisterType<MyInterceptor>();
            //builder.RegisterType<MyNameService>();
            //builder.RegisterType<MyService2>().As<IMyService>()
            //    .PropertiesAutowired()
            //    .InterceptedBy(typeof(MyInterceptor))
            //    .EnableInterfaceInterceptors();
            //运行结果
            //Intercept before, Method:ShowCode
            //MyService2.ShowCode:39567352,NameService是否为空：False
            //Intercept  after,Method: ShowCode

            //子容器
            builder.RegisterType<MyNameService>().InstancePerMatchingLifetimeScope("myscope");
        }
        public ILifetimeScope AutofacContainer { get; private set; }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //获取跟容器
            AutofacContainer = app.ApplicationServices.GetAutofacRoot();
            //命名
            //var serviceName = this.AutofacContainer.ResolveNamed<IMyService>("service2");
            //serviceName.ShowCode();
            //运行结果MyService2.ShowCode:1815156,NameService是否为空：True

            //非命名
            //var serviceNor = this.AutofacContainer.Resolve<IMyService>();
            //serviceNor.ShowCode();
            //运行结果：MyService.ShowCode:1815156

            //子容器  某一个范围是单例
            using (var myscope=AutofacContainer.BeginLifetimeScope("myscope"))
            {
                var service0 = myscope.Resolve<MyNameService>();
                using (var scope=myscope.BeginLifetimeScope())
                {
                    var service1 = scope.Resolve<MyNameService>();
                    var service2 = scope.Resolve<MyNameService>();
                    Console.WriteLine($"service1=service2:{service1 == service2}");
                    Console.WriteLine($"service1=service0:{service1 == service0}");
                }
            }
            //运行结果
            //service1 = service2:True
            //service1 = service0:True
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
