﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using Autofac.Core.IRepositories;
using Autofac3.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Autofac3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        //private readonly ILearningrouteRepository _learningrouteRepository;
        public WeatherForecastController(ILogger<WeatherForecastController> logger
            //ILearningrouteRepository learningrouteRepository
            )
        {
            _logger = logger;
           // _learningrouteRepository = learningrouteRepository;
        }
        /// <summary>
        /// [FromServices] IMyService opt
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            //opt.ShowCode();
            //运行结果
            //Intercept before, Method:ShowCode
            //MyService2.ShowCode:25675301,NameService是否为空：False
            //Intercept  after,Method: ShowCode
           // _learningrouteRepository.GetAll();
             var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
