﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Zebin.Entities;

namespace Zebin.v1.Books
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<CreateUpdateBookDto, Book>();
            CreateMap<Book, BookDto>();
        }
    }
}
