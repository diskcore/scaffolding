﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zebin.Data;

namespace Zebin.v1.Books
{
    public interface IBookService
    {

        Task<PagedResultDto<BookDto>> GetListAsync(PageDto pageDto);

        Task<BookDto> GetAsync(long id);

        Task CreateAsync(CreateUpdateBookDto inputDto);

        Task UpdateAsync(long id, CreateUpdateBookDto inputDto);

        Task DeleteAsync(long id);
    }
}
