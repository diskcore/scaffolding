﻿using FreeSql;
using OrmFreeSql.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrmFreeSql.Data
{
    public class StudentContext : DbContext
    {
        public DbSet<Student> Student { get; set; }
    }
}
