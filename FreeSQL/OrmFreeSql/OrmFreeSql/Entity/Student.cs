﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrmFreeSql.Entity
{
    [Table(Name = "Student")]
    public class Student
    {
        //唯一键标识,不然数据库表字段会根据实体重新生成
        [Column(IsIdentity = true)]
        public int StuNo { get; set; }

        //[Column(Name = "StuName")]
        public string StuName { get; set; }

        public int StuAge { get; set; }

        // [Column(Name = "ClassID")]
        public int ClassID { get; set; }

        public StuClass StuClass { get; set; }
    }
}
