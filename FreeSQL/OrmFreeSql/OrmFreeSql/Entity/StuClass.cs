﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrmFreeSql.Entity
{
    [Table(Name = "StuClass")]
    public class StuClass
    {
        //[Column(Name = "ID")]
        public int ID { get; set; }

        //[Column(Name = "ClassName")]
        public string ClassName { get; set; }
    }
}
