﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FreeSql;
using Microsoft.AspNetCore.Mvc;
using OrmFreeSql.Data;
using OrmFreeSql.Entity;

namespace OrmFreeSql.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private IFreeSql _fsOrm;
        private StudentContext _studentContext;
        public ValuesController(IFreeSql fsOrm,
            StudentContext studentContext)
        {
            _fsOrm = fsOrm;
            _studentContext = studentContext;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            AddStudent();
            return new string[] { "value1", "value2" };
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public Object GetStudentList()
        {
            ISelect<Student> select = _fsOrm.Select<Student>();
            //多表联合查询(无导航属性联表)
            List<Student> stuJoinDHList = select.LeftJoin<StuClass>((s, c) => s.ClassID == c.ID).ToList();
            //多表联合查询(导航属性联表：s.StuClass.ID为导航属性)
            List<Student> stuJoinList = select.LeftJoin(s => s.ClassID == s.StuClass.ID).ToList();
            //查询所有
            List<Student> stuList = select.ToList();
            //条件查询
            List<Student> stuWhereList = select.Where(s => s.ClassID == 2).ToList();
            //分页查询Page(页码:1开始,每页大小:如每页20条),OrderByDescending倒序,默认为正序
            List<Student> stuPageList = select.Where(s => s.ClassID == 2).Page(1, 20).OrderByDescending(o => o.StuNo).ToList();
            return Newtonsoft.Json.JsonConvert.SerializeObject(stuList);
        }
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <returns></returns>
        public Object AddStudent()
        {
            FreeSql.IInsert<Student> addStu = _fsOrm.Insert<Student>();
            //添加一行
            Student student = new Student();
            student.StuName = "王五";
            student.StuAge = 0;
            int count1 = addStu.AppendData(student).ExecuteAffrows();
            //批量添加
            List<Student> students = new List<Student>();
            students.Add(new Student()
            {
                StuName = "雨",
                StuAge = 1
            });
            students.Add(new Student()
            {
                StuName = "雪",
                StuAge = 1
            });
            int count2 = addStu.AppendData(students).ExecuteAffrows();
            return true;
        }
        /// <summary>
        /// 修改数据
        /// </summary>
        /// <returns></returns>
        public Object UpdateStudent()

        {
            IUpdate<Student> updateStu = _fsOrm.Update<Student>();
            //更新单列
            int count = updateStu.Set(s => s.StuAge, 0).Where(w => w.StuNo == 1).ExecuteAffrows();
            //更新多列
            Student item = new Student { StuNo = 1, StuAge = 1, StuName = "风" };
            int count1 = updateStu.SetSource(item).ExecuteAffrows();
            //批量跟新
            List<Student> students = new List<Student>();
            students.Add(new Student
            {
                StuNo = 1,
                StuName = "张飞",
                StuAge = 1
            });
            students.Add(new Student
            {
                StuNo = 2,
                StuName = "毕傲婵",
                StuAge = 0
            });
            int count2 = updateStu.SetSource(students).ExecuteAffrows();
            students.Clear();
            //保存实体忽略列
            students.Add(new Student
            {
                StuNo = 1,
                StuName = "张飞1",
                StuAge = 0
            });
            students.Add(new Student
            {
                StuNo = 2,
                StuName = "毕傲婵1",
                StuAge = 1
            });
            int count3 = updateStu.SetSource(students).IgnoreColumns(i => i.StuAge).ExecuteAffrows();
            students.Clear();
            //只更新指定的列
            students.Add(new Student
            {
                StuNo = 1,
                StuName = "张飞",
                StuAge = 0
            });
            students.Add(new Student
            {
                StuNo = 2,
                StuName = "毕傲婵",
                StuAge = 1
            });
            int count4 = updateStu.UpdateColumns(s => s.StuName).ExecuteAffrows();
            return count;
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <returns></returns>
        public Object DeleteStu()
        {
            IDelete<Student> delStu = _fsOrm.Delete<Student>();
            IDelete<StuClass> delStuClass = _fsOrm.Delete<StuClass>();
            //同线程事务
            _fsOrm.Transaction(() =>
            {
                int count = delStu.Where(w => w.ClassID == 1).ExecuteAffrows();
                if (count > 0)
                    count = delStuClass.Where(w => w.ID == 1).ExecuteAffrows();
                if (count < 1)
                    throw new Exception("处理失败,事务回滚");
            });
            //CreateUnitOfWork(UnitOfWork + Repository)
            using (IRepositoryUnitOfWork sw = _fsOrm.CreateUnitOfWork())
            {
                var delStu1 = sw.GetRepository<Student>();
                var delStuClass1 = sw.GetRepository<StuClass>();
                int count = delStu1.Delete(d => d.ClassID == 2);
                count = delStuClass1.Delete(d => d.ID == 5);
                if (count > 0)
                {
                    sw.Commit();
                }
                else
                {
                    sw.Rollback();
                    throw new Exception("处理失败,事务回滚");
                }
            };
            return true;
        }
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
