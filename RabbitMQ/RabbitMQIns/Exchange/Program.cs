﻿using RabbitMQHelper;
using System;

namespace Exchange
{
    /// <summary>
    /// Send端
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            #region 发布订阅Send
            //RabbiBase rabbiBase = new RabbiBase("zhanzebin", "123456");
            //rabbiBase.EstablishConnection();
            //rabbiBase.EstablishModel();
            //var exchangeName = "FirstExchange";
            //rabbiBase.ExchangeDeclare(exchangeName, "fanout");
            //while (true)
            //{
            //    Console.WriteLine("发布订阅--发送消息内容:");
            //    String message = Console.ReadLine();
            //    //路由名称
            //    //String routeKey = args[0];
            //    //交换机发送消息
            //    rabbiBase.BasicPublishExchange(message, exchangeName);
            //    Console.WriteLine("发布订阅--成功发送消息:" + message);
            //}
            #endregion


            #region 路由Send
            //RabbiBase rabbiBase = new RabbiBase("zhanzebin", "123456");
            //rabbiBase.EstablishConnection();
            //rabbiBase.EstablishModel();
            //var exchangeName = "SecondExchange";
            //rabbiBase.ExchangeDeclare(exchangeName, "direct");
            //while (true)
            //{
            //    Console.WriteLine("发布订阅--发送消息内容:");
            //    String message = Console.ReadLine();
            //    //路由名称
            //    String routeKey = args[0];
            //    //交换机发送消息
            //    rabbiBase.BasicPublishExchange(message, exchangeName, routeKey);
            //    Console.WriteLine("发布订阅--成功发送消息:" + message);

            //}
            #endregion

            #region 通配符Send
            RabbiBase rabbiBase = new RabbiBase("zhanzebin", "123456");
            rabbiBase.EstablishConnection();
            rabbiBase.EstablishModel();
            var exchangeName = "TopicExchange";
            rabbiBase.ExchangeDeclare(exchangeName, "topic");
            while (true)
            {
                Console.WriteLine("发布订阅--发送消息内容:");
                String message = Console.ReadLine();
                //路由名称
                String routeKey = args[0];
                //交换机发送消息
                rabbiBase.BasicPublishExchange(message, exchangeName, routeKey);
                Console.WriteLine("发布订阅--成功发送消息:" + message);

            }
            #endregion
        }
    }
}
