﻿using RabbitMQHelper;
using System;
using System.Text;

namespace ExchangeReceive
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 发布订阅Receive
            //RabbiBase rabbiBase = new RabbiBase("zhanzebin", "123456");
            ////创建一个随机数,以创建不同的消息队列
            //int random = rabbiBase.GetRandom(1,1000);
            //Console.WriteLine("Start:" + random.ToString());
            //rabbiBase.EstablishConnection();
            //rabbiBase.EstablishModel();
            //var exchangeName = "FirstExchange";
            //rabbiBase.EstablishBasicConsumer();
            //rabbiBase.ExchangeDeclare(exchangeName, "fanout");
            ////消息队列名称
            //string queueName = exchangeName + "_" + random.ToString();
            //rabbiBase.QueueDeclare(queueName);
            ////将队列与交换机进行绑定
            //rabbiBase.QueueBind(queueName, exchangeName);
            ////声明为手动确认
            //rabbiBase.GetModel().BasicQos(0, 1, false);
            ////开启监听
            //rabbiBase.StartListener((model, ea) =>
            //{
            //    byte[] message = ea.Body.ToArray();//接收到的消息
            //    Console.WriteLine("接收到信息为:" + Encoding.UTF8.GetString(message));
            //    //返回消息确认
            //    rabbiBase.GetModel().BasicAck(ea.DeliveryTag, true);
            //}, queueName);
            //Console.ReadKey();
            //rabbiBase.Dispose();
            #endregion

            #region 路由Receive
            //RabbiBase rabbiBase = new RabbiBase("zhanzebin", "123456");
            ////创建一个随机数,以创建不同的消息队列
            //int random = rabbiBase.GetRandom(1, 1000);
            //Console.WriteLine("Start:" + random.ToString());
            //rabbiBase.EstablishConnection();
            //rabbiBase.EstablishModel();
            //var exchangeName = "SecondExchange";
            //rabbiBase.EstablishBasicConsumer();
            ////fanout 发布订阅
            ////direct 路由
            //rabbiBase.ExchangeDeclare(exchangeName, "direct");
            ////消息队列名称
            //string queueName = exchangeName + "_" + random.ToString();
            //rabbiBase.QueueDeclare(queueName);
            ////将队列与交换机进行绑定
            //foreach (var routeKey in args)
            //{//匹配多个路由
            //    rabbiBase.QueueBind( queueName, exchangeName, routeKey);
            //}
            ////rabbiBase.QueueBind(queueName, exchangeName);
            ////声明为手动确认
            //rabbiBase.GetModel().BasicQos(0, 1, false);
            ////开启监听
            //rabbiBase.StartListener((model, ea) =>
            //{
            //    byte[] message = ea.Body.ToArray();//接收到的消息
            //    Console.WriteLine("接收到信息为:" + Encoding.UTF8.GetString(message));
            //    //返回消息确认
            //    rabbiBase.GetModel().BasicAck(ea.DeliveryTag, true);
            //}, queueName);
            //Console.ReadKey();
            //rabbiBase.Dispose();
            #endregion


            #region 通配符Receive
            RabbiBase rabbiBase = new RabbiBase("zhanzebin", "123456");
            //创建一个随机数,以创建不同的消息队列
            int random = rabbiBase.GetRandom(1, 1000);
            Console.WriteLine("Start:" + random.ToString());
            rabbiBase.EstablishConnection();
            rabbiBase.EstablishModel();
            var exchangeName = "TopicExchange";
            rabbiBase.EstablishBasicConsumer();
            //fanout 发布订阅
            //direct 路由
            rabbiBase.ExchangeDeclare(exchangeName, "topic");
            //消息队列名称
            string queueName = exchangeName + "_" + random.ToString();
            rabbiBase.QueueDeclare(queueName);
            //将队列与交换机进行绑定
            foreach (var routeKey in args)
            {//匹配多个路由
                rabbiBase.QueueBind(queueName, exchangeName, routeKey);
            }
            //rabbiBase.QueueBind(queueName, exchangeName);
            //声明为手动确认
            rabbiBase.GetModel().BasicQos(0, 1, false);
            //开启监听
            rabbiBase.StartListener((model, ea) =>
            {
                byte[] message = ea.Body.ToArray();//接收到的消息
                Console.WriteLine("接收到信息为:" + Encoding.UTF8.GetString(message));
                //返回消息确认
                rabbiBase.GetModel().BasicAck(ea.DeliveryTag, true);
            }, queueName);
            Console.ReadKey();
            rabbiBase.Dispose();
            #endregion
        }
    }
}
