﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace RabbitMQHelper
{
    public class RabbiBase
    {
        //RabbitMQ工厂
        private IConnectionFactory _connectionFactory;
        //Rabbit主机地址
        private string _hostName = "localhost";
        //Rabbit端口
        private int _port = 5672;
        //Rabbit连接
        private IConnection _connection;
        //Rabbit通道
        private IModel _model;
        //Rabbit消费者对象
        private EventingBasicConsumer _basicConsumer;
        public RabbiBase(string userName, string password)
        {
            _connectionFactory = new ConnectionFactory
            {
                HostName = _hostName,
                Port = _port,
                UserName = userName,
                Password = password,
            };
        }

        //创建连接
        public void EstablishConnection()
        {
            _connection = _connectionFactory.CreateConnection();
            //return _connection;
        }
        //关闭连接和管道
        public void Dispose()
        {
            _model.Dispose();
            _connection.Dispose();
        }
        //创建通道
        public void EstablishModel()
        {
            _model = _connection.CreateModel();
        }

        //获取管道
        public IModel GetModel()
        {
            return _model;
        }

        /// <summary>
        /// 声明队列
        /// </summary>
        public void QueueDeclare(string queueName)
        {
            _model.QueueDeclare(
                          queue: queueName,//消息队列名称
                          durable: false,//是否缓存
                          exclusive: false,
                          autoDelete: false,
                          arguments: null
            );
        }
        /// <summary>
        /// 声明交换机
        /// </summary>
        /// <param name="exchangeName"></param>
        /// <param name="type">fanout 发布订阅模式</param>
        public void ExchangeDeclare(string exchangeName, string type)
        {
            _model.ExchangeDeclare(exchangeName, type);
        }

        /// <summary>
        /// 将队列与交换机进行绑定
        /// </summary>
        /// <param name="queueName">队列名称</param>
        /// <param name="exchangeName">交换机名称</param>
        public void QueueBind(string queueName, string exchangeName,string routeKey="")
        {
            _model.QueueBind(queueName, exchangeName, routeKey);
        }


        //创建消费者对象
        public void EstablishBasicConsumer()
        {
            //能者多劳
            ////告诉Rabbit每次只能向消费者发送一条信息,再消费者未确认之前,不再向他发送信息
            _model.BasicQos(0, 1, false);
            _basicConsumer = new EventingBasicConsumer(_model);
        }

        /// <summary>
        /// 改变消息确认机制 实现能者多劳
        /// 自动确认Or手动确认
        /// </summary>
        /// <param name="basicDeliverEventArgs"></param>
        /// <param name="queueName"></param>
        public void StartListener(EventHandler<BasicDeliverEventArgs> basicDeliverEventArgs, string queueName)
        {
            _basicConsumer.Received += basicDeliverEventArgs;
            //消费者开启监听 autoAck：自动确认和手动确认
            _model.BasicConsume(queue: queueName, autoAck: false, consumer: _basicConsumer);

        }

        //Rabbit队列消息发送
        public void BasicPublishQueue(string message, string queueName)
        {
            byte[] body = Encoding.UTF8.GetBytes(message);
            _model.BasicPublish(exchange: "", routingKey: queueName, basicProperties: null, body: body);
        }
        //Rabbit交换机消息发送
        public void BasicPublishExchange(string message, string exchangeName ,string proutingKey="")
        {
            byte[] body = Encoding.UTF8.GetBytes(message);
            _model.BasicPublish(exchange: exchangeName, routingKey: proutingKey, basicProperties: null, body: body);
        }
        //获取随机数
        public int GetRandom(int min, int max)
        {
            int rtn = 0;
            Random r = new Random();
            byte[] buffer = Guid.NewGuid().ToByteArray();
            int iSeed = BitConverter.ToInt32(buffer, 0);
            r = new Random(iSeed);
            rtn = r.Next(min, max + 1);
            return rtn;
        }
    }
}
