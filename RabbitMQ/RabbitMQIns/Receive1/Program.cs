﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;

namespace Receive1
{
    class Program
    {
        static void Main(string[] args)
        {
            //    IConnectionFactory connFactory = new ConnectionFactory//创建连接工厂对象
            //    {
            //        HostName = "localhost",//IP地址
            //        Port = 5672,//端口号
            //        UserName = "zhanzebin",//用户账号
            //        Password = "123456"//用户密码
            //    };
            //    using (IConnection conn = connFactory.CreateConnection())
            //    {
            //        using (IModel channel = conn.CreateModel())
            //        {
            //            String queueName = String.Empty;
            //            if (args.Length > 0)
            //                queueName = args[0];
            //            else
            //                queueName = "FirstQueue";
            //            //声明一个队列
            //            channel.QueueDeclare(
            //              queue: queueName,//消息队列名称
            //              durable: false,//是否缓存
            //              exclusive: false,
            //              autoDelete: false,
            //              arguments: null
            //               );
            //            //创建消费者对象
            //            var consumer = new EventingBasicConsumer(channel);
            //            consumer.Received += (model, ea) =>
            //            {
            //                byte[] message = ea.Body.ToArray();//接收到的消息
            //                Console.WriteLine("接收到信息为:" + Encoding.UTF8.GetString(message));
            //            };
            //            //消费者开启监听
            //            channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);

            //            Console.ReadKey();
            //        }
            //    }

            RabbiBase rabbiBase = new RabbiBase("zhanzebin", "123456");
            rabbiBase.EstablishConnection();
            rabbiBase.EstablishModel();
            rabbiBase.QueueDeclare("FirstQueue");
            rabbiBase.EstablishBasicConsumer();
            rabbiBase.StartListener((model, ea) =>
                        {
                            
                            Thread.Sleep(3000);//等待3秒,
                            //Thread.Sleep(rabbiBase.GetRandom(1,8) * 1000);//随机等待,实现能者多劳,
                            byte[] message = ea.Body.ToArray();//接收到的消息
                            Console.WriteLine("接收到信息为:" + Encoding.UTF8.GetString(message));
                            //返回消息确认
                            rabbiBase.GetModel().BasicAck(ea.DeliveryTag, true);
                        }, "FirstQueue");
          
            Console.ReadKey();
            rabbiBase.Dispose();



        }
    }
}
