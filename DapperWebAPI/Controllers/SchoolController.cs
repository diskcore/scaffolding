﻿using DapperWebAPI.Models;
using DapperWebAPI.Repository;

using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DapperWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class SchoolController : Controller
    {
        private readonly SchoolRepository schoolRepository;
        public SchoolController()
        {
            schoolRepository = new SchoolRepository();
        }
        [HttpPost]
        public void Post([FromBody] School school)
        {
            if (ModelState.IsValid)
                schoolRepository.Add(school);
        }
    }
}
