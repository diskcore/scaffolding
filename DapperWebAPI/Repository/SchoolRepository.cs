﻿using Dapper;
using DapperWebAPI.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace DapperWebAPI.Repository
{
    //https://www.cnblogs.com/Cwj-XFH/p/5855489.html
    public class SchoolRepository
    {
        private string connectionString;
        public SchoolRepository()
        {
            connectionString = @"Database=Dapper;Data Source=localhost;User Id=jesse;Password=pwd123456;pooling=false;CharSet=utf8;port=3306;";
        }

        public IDbConnection Connection
        {
            get
            {
                return new MySqlConnection(connectionString);
            }
        }
        public void Add(School school)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = "insert into t_schools(Name,Address) values(@Name,@Address)";
                dbConnection.Open();
               var res= dbConnection.Execute(sQuery, school);
            }
        }
    }
}
