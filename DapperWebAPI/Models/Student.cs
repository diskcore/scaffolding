﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DapperWebAPI.Models
{
    //SchoolRepository
    public class Student
    {
        public string Name { set; get; }
        public string Number { set; get; }
        public int SchoolId { set; get; }
    }
}
