﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DapperWebAPI.Models
{
    public class School
    {
        /*
          若属性名和数据库字段不一致（不区分大小写）则查询不出数据，如果使用EF则可以通过Column特性
          建立属性和数据表字段之间的映射关系，Dapper则不行
        */
        //[Column("Name")]
        public string Name { set; get; }
        public string Address { set; get; }
    }
}
