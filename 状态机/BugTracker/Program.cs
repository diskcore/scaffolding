﻿using System;

namespace BugTracker
{
    //例子详情：https://www.cnblogs.com/lwqlun/p/10674018.html
    class Program
    {
        static void Main(string[] args)
        {
            Bug bug = new Bug("Hello World!");

            Console.WriteLine($"Current State: {bug.CurrentState}");

            bug.Assign("Lamond Lu");

            Console.WriteLine($"Current State: {bug.CurrentState}");
            Console.WriteLine($"Current Assignee: {bug.Assignee}");

            bug.Defer();

            Console.WriteLine($"Current State: {bug.CurrentState}");
            Console.WriteLine($"Current Assignee: {bug.Assignee}");

            bug.Assign("Lu Nan");

            Console.WriteLine($"Current State: {bug.CurrentState}");
            Console.WriteLine($"Current Assignee: {bug.Assignee}");

            bug.Close();

            Console.WriteLine($"Current State: {bug.CurrentState}");
        }
    }
}
