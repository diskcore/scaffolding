﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BugTracker
{
    ///<summary>
    ///下面我们列举一下Bug对象可能的状态变化。
    //每个Bug的初始状态是Open
    //如果当前Bug的状态是Open, 触发动作Assign, Bug的状态会变为Assigned
    //如果当前Bug的状态是Assigned, 触发动作Defer, Bug的状态会变为Deferred
    //如果当前Bug的状态是Assigned, 触发动作Close, Bug的状态会变为Closed
    //如果当前Bug的状态是Assigned, 触发动作Assign, Bug的状态会保持Assigned(变更Bug修改者的场景)
    //如果当前Bug的状态是Deferred, 触发动作Assign, Bug的状态会变为Assigned
    ///</summary>
    public enum State
    {
        Open,
        Assigned,
        Deferred,
        Closed
    }
    public enum Trigger
    {
        Assign,
        Defer,
        Close
    }
}
