﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace MediatorUnit
{
    class Program
    {
        async static Task Main(string[] args)
        {
            var services = new ServiceCollection();

            //扫描当前程序集
            services.AddMediatR(typeof(Program).Assembly);

            var serviceProvider = services.BuildServiceProvider();

            var mediator = serviceProvider.GetService<IMediator>();

            //命令的构造和命令的处理分离开
            //await mediator.Publish(new MyEvent { EventName = "event01" });
            //运行结果
            //MyEventHandler执行：event01
            //MyEventHandlerV2执行：event01

            await mediator.Send(new MyCommand { CommandName = "cmd01" });
            //运行结果
            //MyCommandHandler执行命令：cmd01
            Console.WriteLine("Hello World!");
        }
    }
    #region 
    /// <summary>
    /// IRequest：中介者要执行的命令
    /// </summary>
    internal class MyCommand : IRequest<long>
    { 
        public string CommandName { get; set; }
    }
    /// <summary>
    /// 扫描到第一个就不往后面扫描了
    /// </summary>
    internal class MyCommandHandler2 : IRequestHandler<MyCommand, long>
    {
        public Task<long> Handle(MyCommand request, CancellationToken cancellationToken)
        {
            Console.WriteLine($"MyCommandHandlerV2执行命令：{request.CommandName}");
            return Task.FromResult(10L);
        }
    }
    /// <summary>
    /// 命令处理器的定义MyCommandHandler 使用来处理MyCommand的 第二个参数是返回值
    /// </summary>
    internal class MyCommandHandler : IRequestHandler<MyCommand, long>
    {
        public Task<long> Handle(MyCommand request, CancellationToken cancellationToken)
        {
            Console.WriteLine($"MyCommandHandler执行命令：{request.CommandName}");
            return Task.FromResult(10L);
        }
    }
   
    #endregion
    #region
    internal class MyEvent : INotification
    { 
        public string EventName { get; set; }
    }

    
    //
    internal class MyEventHandler : INotificationHandler<MyEvent>
    {
        /// <summary>
        /// 会被mediator.Publish(new MyEvent { EventName = "event01" });执行
        /// </summary>
        /// <param name="notification"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task Handle(MyEvent notification, CancellationToken cancellationToken)
        {
            Console.WriteLine($"MyEventHandler执行：{notification.EventName}");
            return Task.CompletedTask;
        }
    }
    /// <summary>
    /// 会被mediator.Publish(new MyEvent { EventName = "event01" });执行
    /// </summary>
    internal class MyEventHandlerV2 : INotificationHandler<MyEvent>
    {
        public Task Handle(MyEvent notification, CancellationToken cancellationToken)
        {
            Console.WriteLine($"MyEventHandlerV2执行：{notification.EventName}");
            return Task.CompletedTask;
        }
    }

    #endregion
}
