﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MediatrWeb.Commands
{
    public class NewUserHandler : IRequestHandler<NewUser, bool>
    {
        public Task<bool> Handle(NewUser request, CancellationToken cancellationToken)
        { 
            return Task.FromResult(true);
        }
    }
}
