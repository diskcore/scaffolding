﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MediatrWeb.Event
{
    /// <summary>
    /// 日志处理事件
    /// </summary>
    public class LogHandler : INotificationHandler<NewUserEvent>
    {
        public Task Handle(NewUserEvent notification, CancellationToken cancellationToken)
        {
            //Send email  
            Debug.WriteLine(" ****  User save to log  *****");
            return Task.FromResult(true);
        }
    }
}
