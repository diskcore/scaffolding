﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MediatrWeb.Event
{
    //用户处理事件
    public class NewUserHandler : INotificationHandler<NewUserEvent>
    {
        public Task Handle(NewUserEvent notification, CancellationToken cancellationToken)
        { 
            Debug.WriteLine(" ****  Save user in database  *****");
            return Task.FromResult(true);
        }
    }
}
