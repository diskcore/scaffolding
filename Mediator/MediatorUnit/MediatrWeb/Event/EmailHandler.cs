﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MediatrWeb.Event
{
    //发送邮件事件
    public class EmailHandler : INotificationHandler<NewUserEvent>
    {
        public Task Handle(NewUserEvent notification, CancellationToken cancellationToken)
        {
            //Send email  
            Debug.WriteLine(" ****  Email sent to user  *****");
            return Task.FromResult(true);
        }
    }
}
