﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediatrWeb.Event
{
    public class NewUserEvent : INotification
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
