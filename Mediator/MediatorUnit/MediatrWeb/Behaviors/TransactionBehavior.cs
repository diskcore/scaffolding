﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MediatrWeb.Behaviors
{
    /// <summary>
    /// IRequestHandler 命令拦截器
    /// </summary>
    /// <typeparam name="TRequest"></typeparam>
    /// <typeparam name="TResponse"></typeparam>
    public class TransactionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        public async  Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var response = default(TResponse);
            //throw new NotImplementedException();
            Console.WriteLine($"IPipelineBehavior Start");
            response = await next();
            Console.WriteLine($"IPipelineBehavior End");
            return response;
        }
    }
}
