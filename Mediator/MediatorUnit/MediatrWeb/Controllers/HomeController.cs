﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using MediatrWeb.Commands;
using MediatrWeb.Event;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MediatrWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class HomeController : ControllerBase
    {
        

        private readonly ILogger<HomeController> _logger;
        private readonly IMediator _mediator;
        public HomeController(ILogger<HomeController> logger
            , IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

       
        [HttpPost]
        public IActionResult Register(NewUser user)
        {
            //命令
            bool result = _mediator.Send(user).Result;

            if (result)
                return Ok(true);

            return BadRequest();
        }
        [HttpPost]
        public ActionResult Login(NewUserEvent user)
        {
            //事件
            _mediator.Publish(user);
            return Ok("Login");
        }
    }
}
