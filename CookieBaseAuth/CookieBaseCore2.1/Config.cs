﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CookieBaseCore2
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetApiResource()
        {
            return new List<ApiResource>
            {
                new ApiResource("api","API Application")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client()
                {
                    ClientId = "mvc",
                    ClientName = "Mvc Client",
                    ClientUri = "http://localhost:5001",
                    //图标
                    LogoUri = "https://chocolatey.org/content/packageimages/dotnetcore-vs.2.0.3-Preview.png",
                    //AllowRememberConsent = true,
                    //AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,
                    ClientSecrets = {
                        new Secret("secret".Sha256())
                    },
                    //AllowOfflineAccess = true,
                    //AllowAccessTokensViaBrowser = true,



                    RequireConsent = true,
                    RedirectUris = {"http://localhost:5001/signin-oidc"},
                    PostLogoutRedirectUris = {"http://localhost:5001/signout-callback-oidc"},

                    //包含Role 头像 等信息返回
                    AlwaysIncludeUserClaimsInIdToken = true,

                    AllowedScopes = {
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OpenId,
                        //IdentityServerConstants.StandardScopes.Email,
                        //IdentityServerConstants.StandardScopes.OfflineAccess,
                        //"api1"
                    }
                    //AllowedScopes = {"api"}
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()//,
               // new IdentityResources.Email()
            };
        }

        /// <summary>
        /// 测试用户 IdentityServer用户和密码
        /// </summary>
        /// <returns></returns>
        public static List<TestUser> GetTestUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "jesse",
                    Password ="password",
                    Claims=new List<Claim>
                    {
                        new Claim("name","jesse"),
                        new Claim("website","video.jessetalk.cn")

                    }
                }
            };
        }

    }
}
