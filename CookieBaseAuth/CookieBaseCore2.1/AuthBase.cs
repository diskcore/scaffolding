﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace CookieBaseAuth
{
   
    public class AuthBase
    {
        //验证方式
        private  string _authenScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        //Cookie属性
        private AuthenticationProperties _cookiePro = new AuthenticationProperties { IsPersistent = true };
        //ClaimsIdentity属性
        private ClaimsIdentity _claimIdentity;

        //认证索要
        private List<Claim> _claimsList = new List<Claim>();
        
        //主要索要
        private ClaimsPrincipal _claimsPrincipal;
        //构造函数
        public AuthBase(DateTime expiresUtc)
        {
            _cookiePro.ExpiresUtc = expiresUtc;
        }
        //构造函数过期时间
        public AuthBase(string authenScheme, DateTime expiresUtc)
        {
            _authenScheme = authenScheme;
            _cookiePro.ExpiresUtc = expiresUtc;
        }
        //设置Cookie
        public void SignInAsyncSelf(List<Claim> claimList,HttpContext httpContext)
        {
            _claimIdentity = new ClaimsIdentity(claimList, _authenScheme);
            _claimsPrincipal = new ClaimsPrincipal(_claimIdentity);
            httpContext.SignInAsync(_authenScheme, _claimsPrincipal, _cookiePro);
        }
        //清除Cookie
        public void SignInAsyncSelf(HttpContext httpContext)
        {
            httpContext.SignOutAsync(_authenScheme);
        }
    }
}
