﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using CookieBaseCore2.Data;
using Microsoft.EntityFrameworkCore;
using CookieBaseCore2.Models;
using Microsoft.AspNetCore.Identity;
using CookieBaseCore2;
using IdentityServer4.Services;
using CookieBaseCore2.Services;
using IdentityServer4.EntityFramework;
using System.Reflection;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;

namespace CookieBaseAuth
{
    public class Startup
    {
        //验证失败重定向路径
        private const string LoginPath = "/Account/Login";


        //Cookie策略配置
        private const string PolicyName = "Zhanzebin";
        private const string PolicyKey = "Zhan";
        private const string PolicyValue = "zebin";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        //PersistedGrantDbContext
        //Add-Migration InitConfiguration -Context ConfigurationDbContext -OutPutDir  Data\Migrations\IdentityServer\ConfigurationDbContext
        //Add-Migration InitConfiguration -Context PersistedGrantDbContext -OutPutDir  Data\Migrations\IdentityServer\PersistedGrantDbContext
        public void ConfigureServices(IServiceCollection services)
        {
            const string connectionString = @"Server=LAPTOP-JDMA6KQD;Database=IdentityServer4.Quickstrat.EF2.0;Trusted_Connection=True;MultipleActiveResultSets=true";
           
            var migrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });
            services.AddIdentity<ApplicationUser, ApplicationUserRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            //基于IdentityServer4的
            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                //.AddInMemoryClients(Config.GetClients())
                //.AddTestUsers(Config.GetTestUsers())
                //.AddInMemoryApiResources(Config.GetApiResource())
                //.AddInMemoryIdentityResources(Config.GetIdentityResources())
                //存储Client 
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = builder =>
                    {
                        builder.UseSqlServer(connectionString,
                            sql => sql.MigrationsAssembly(migrationAssembly));
                    };
                })
                //行为操作的信息
               .AddOperationalStore(options =>
               {
                   options.ConfigureDbContext = builder =>
                   {
                       builder.UseSqlServer(connectionString,
                           sql => sql.MigrationsAssembly(migrationAssembly));
                   };
               })
                .AddAspNetIdentity<ApplicationUser>()
                //添加ProfileService 依赖注入
                .Services.AddScoped<IProfileService, ProfileService>()
                ;
            //IdentityServer4.EntityFramework.DbContexts.ConfigurationDbContext
            //IdentityServer4.EntityFramework.DbContexts.PersistedGrantDbContext
            //基于Identity的
            //services.AddDbContext<ApplicationDbContext>(options =>
            //{
            //    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            //});

            //services.AddIdentity<ApplicationUser, ApplicationUserRole>()
            //    .AddEntityFrameworkStores<ApplicationDbContext>()
            //    .AddDefaultTokenProviders();

            //Identity相关参数配置
            services.Configure<IdentityOptions>(options =>
            {
                // 密码设置
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 1;
                options.Password.RequiredUniqueChars = 1;

                // 锁定设置
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // 用户设置
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
            });

            ////cookie设置
            //services.ConfigureApplicationCookie(options =>
            //{
            //    // Cookie设置
            //    options.Cookie.HttpOnly = true;
            //    //设置cookie过期时间
            //    options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

            //    options.LoginPath = "/Account/Login";
            //    options.AccessDeniedPath = "/Home/Index";
            //    options.SlidingExpiration = true;
            //});

            #region CooikeBase
            ////配置权限
            //services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            //    .AddCookie(options =>
            //    {
            //        options.LoginPath = LoginPath;
            //    });
            ////配置策略
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy(PolicyName, builder =>
            //    {
            //        builder.RequireClaim(PolicyKey, PolicyValue);
            //    });
            //});
            //services.Configure<CookiePolicyOptions>(options =>
            //{
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;
            //});
            #endregion
            services.AddScoped<CookieBaseCore2.Services.ConsentService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            InitIdentityServerDataBase(app);
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            //添加权限验证
            //app.UseAuthentication();
            app.UseIdentityServer();

            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        //初始化数据库
        public void InitIdentityServerDataBase(IApplicationBuilder app)
        {
            //通过上下文获取相关服务
            using (var scope = app.ApplicationServices.CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
                var configurationDbContext = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();

                if (!configurationDbContext.Clients.Any())
                {
                    foreach (var client in Config.GetClients())
                    {
                        configurationDbContext.Clients.Add(client.ToEntity());
                    }
                    configurationDbContext.SaveChanges();
                }

                if (!configurationDbContext.ApiResources.Any())
                {
                    foreach (var api in Config.GetApiResource())
                    {
                        configurationDbContext.ApiResources.Add(api.ToEntity());
                    }
                    configurationDbContext.SaveChanges();
                }

                if (!configurationDbContext.IdentityResources.Any())
                {
                    foreach (var identity in Config.GetIdentityResources())
                    {
                        configurationDbContext.IdentityResources.Add(identity.ToEntity());
                    }
                    configurationDbContext.SaveChanges();
                }
            }
        }
    }
}
