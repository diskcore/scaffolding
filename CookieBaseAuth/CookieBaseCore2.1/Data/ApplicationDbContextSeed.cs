﻿using CookieBaseCore2.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CookieBaseCore2.Data
{
    public class ApplicationDbContextSeed
    {
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationUserRole> _roleManager;

        public async Task SeedAsync(ApplicationDbContext context,IServiceProvider service)
        {
            if (!context.Roles.Any())
            {
                _roleManager = service.GetRequiredService<RoleManager<ApplicationUserRole>>();
                var role = new ApplicationUserRole() { Name = "Adminstrators", NormalizedName = "Adminstrators" };
                var result= await _roleManager.CreateAsync(role);
                if (!result.Succeeded)
                {
                    throw new Exception("初始角色失败"+result.Errors.SelectMany(e=>e.Description));
                }

            }
            if (!context.Users.Any())
            {
                _userManager = service.GetRequiredService<UserManager<ApplicationUser>>();
               
                var defaultUser = new ApplicationUser
                {
                    UserName="Adminstrator",
                    Email="jesseatalk@163.com",
                    NormalizedUserName="admin",
                    SecurityStamp="admin",
                    //头像
                    Avatar= "https://chocolatey.org/content/packageimages/dotnetcore-vs.2.0.3-Preview.png"
                };
                

                var result=await _userManager.CreateAsync(defaultUser, "123456");
                //添加角色
                await _userManager.AddToRoleAsync(defaultUser, "Adminstrators");

                if (!result.Succeeded)
                {
                    throw new Exception("初始用户失败");
                }
            }
            //using (var scope=service.CreateScope())
            //{
            //    var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            //    userManager.
            //}
        }
    }
}
