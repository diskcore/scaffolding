﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CookieBaseAuth.Controllers.Models;
using CookieBaseCore2.Models;
using IdentityServer4.Services;
using IdentityServer4.Test;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CookieBaseAuth.Controllers
{
    public class AccountController : Controller
    {
        //设置Cookie过期时间一天
        AuthBase authBase = new AuthBase(DateTime.Now.AddDays(1));
        private const   string AuthenScheme = CookieAuthenticationDefaults.AuthenticationScheme;

        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        private IIdentityServerInteractionService _interaction;
        //有TestUserStore IResourceStore IClientStore 来访问ApiResource

        private readonly TestUserStore _users;

        public AccountController(
            UserManager<ApplicationUser> userManager, 
            SignInManager<ApplicationUser> signInManager,
            IIdentityServerInteractionService interaction )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _interaction = interaction;
        }

        //public AccountController(TestUserStore users)
        //{
        //    _users = users;
        //}
        private IActionResult RedirectToLoacl(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel viewModel, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                ViewData["ReturnUrl"] = returnUrl;
                var identityUser = new ApplicationUser
                {
                    Email = viewModel.Email,
                    UserName = viewModel.Email,
                    NormalizedUserName = viewModel.Email
                };
                var identityResult = await _userManager.CreateAsync(identityUser, viewModel.Password);
                if (identityResult.Succeeded)
                {
                    await _signInManager.SignInAsync(identityUser, new AuthenticationProperties
                    {
                        IsPersistent = true
                    });
                    return RedirectToLoacl(returnUrl);
                }
                else
                {
                    AddErrors(identityResult);
                }
            }
            return RedirectToLoacl(returnUrl);
            //return View();
        }
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel, string returnUrl = null)
        {
            //if (ModelState.IsValid)
            //{
            //    ViewData["ReturnUrl"] = returnUrl;
            //    var user = await _userManager.FindByEmailAsync(loginViewModel.Email);
            //    if (user == null)
            //    {
            //        return RedirectToAction("Index", "Home");
            //    }
            //    await _signInManager.SignInAsync(user, new AuthenticationProperties
            //    {
            //        IsPersistent = true
            //    });
            //    return RedirectToLoacl(returnUrl);
            //}
            //return RedirectToLoacl(returnUrl);

            if (ModelState.IsValid)
            {
                ViewData["ReturnUrl"] = returnUrl;
                var user =await _userManager.FindByEmailAsync(loginViewModel.Email);
                if (user == null)
                {
                    ModelState.AddModelError(nameof(loginViewModel.Email), "Email not exist");
                    // return RedirectToAction("Index", "Home");
                }
                else
                {
                    if (await _userManager.CheckPasswordAsync(user, loginViewModel.Password)
                        /*_users.ValidateCredentials(loginViewModel.Email, loginViewModel.Password)*/)
                    {
                        AuthenticationProperties props = null;
                        if(loginViewModel.RememberMe)
                        {
                             props = new AuthenticationProperties
                            {
                                IsPersistent = true,
                                ExpiresUtc = DateTime.UtcNow.Add(TimeSpan.FromMinutes(30))
                            };
                        }
                      
                        //HttpContext.SignInAsync(user.SubjectId, user.Username, props);
                        //await Microsoft.AspNetCore.Http.AuthenticationManagerExtensions.SignInAsync(
                        //     HttpContext,
                        //     user.SubjectId,
                        //     user.Username,
                        //     props);
                        await _signInManager.SignInAsync(user, props);
                        if (_interaction.IsValidReturnUrl(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        return Redirect("~/");
                        //需要Conset的时候不会直接跳转到客户端 而是到了自己的Consent页面
                        //return RedirectToLoacl(returnUrl);
                    }
                    ModelState.AddModelError(nameof(loginViewModel.Password), "Wrong Password");
                }
                //await _signInManager.SignInAsync(user, new AuthenticationProperties
                //{
                //    IsPersistent = true
                //});
                // return RedirectToLoacl(returnUrl);
            }
            return View(loginViewModel);
            //if (ModelState.IsValid)
            //{
            //    ViewData["ReturnUrl"] = returnUrl;
            //    var user =  _users.FindByUsername(loginViewModel.UserName);
            //    if (user == null)
            //    {
            //        ModelState.AddModelError(nameof(loginViewModel.UserName), "Email not exist");
            //       // return RedirectToAction("Index", "Home");
            //    }
            //    else
            //    {
            //        if (_users.ValidateCredentials(loginViewModel.UserName, loginViewModel.Password))
            //        {
            //            var props = new AuthenticationProperties
            //            {
            //                IsPersistent = true,
            //                ExpiresUtc = DateTime.UtcNow.Add(TimeSpan.FromMinutes(30))
            //            };
            //            //HttpContext.SignInAsync(user.SubjectId, user.Username, props);
            //           await Microsoft.AspNetCore.Http.AuthenticationManagerExtensions.SignInAsync(
            //                HttpContext,
            //                user.SubjectId, 
            //                user.Username,
            //                props);
            //            //return Redirect("~/");
            //            //需要Conset的时候不会直接跳转到客户端 而是到了自己的Consent页面
            //            return RedirectToLoacl(returnUrl);
            //        }
            //        ModelState.AddModelError(nameof(loginViewModel.Password), "Wrong Password");
            //    }
            //    //await _signInManager.SignInAsync(user, new AuthenticationProperties
            //    //{
            //    //    IsPersistent = true
            //    //});
            //   // return RedirectToLoacl(returnUrl);
            //}
            //return View();
        }
        //public IActionResult MakeLogin()
        //{
        //    //创建Claim
        //    var Claims = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.Name,"zhanzebin"),
        //        new Claim(ClaimTypes.Role,"admin")
        //    };
        //    authBase.SignInAsyncSelf(Claims, HttpContext);
        //    return View();
        //}
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            //return RedirectToAction("Index", "Home");
            //await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
            // authBase.SignInAsyncSelf(HttpContext);
            //return Ok();
        }
    }
}