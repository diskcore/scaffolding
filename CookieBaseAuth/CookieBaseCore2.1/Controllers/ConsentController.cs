﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CookieBaseCore2.Services;
using CookieBaseCore2.ViewModels;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Mvc;

namespace CookieBaseCore2.Controllers
{
    public class ConsentController : Controller
    {
        private readonly ConsentService _consentService;

        private readonly IClientStore _clientStore;
        private readonly IResourceStore _resourceStore;
        private readonly IIdentityServerInteractionService _identityServerInteractionService;
        private ScopeViewModel CreatScopeViewModel(Scope scope, bool check)
        {
            return new ScopeViewModel
            {
                Name = scope.Name,
                DisplayName = scope.DisplayName,
                Description = scope.Description,
                Checked = check || scope.Required,
                Required = scope.Required,
                Emphasize = scope.Emphasize
            };
        }
        private ScopeViewModel CreatScopeViewModel(IdentityResource identityResource, bool check)
        {
            return new ScopeViewModel
            {
                Name = identityResource.Name,
                DisplayName = identityResource.DisplayName,
                Description = identityResource.Description,
                Checked = check || identityResource.Required,
                Required = identityResource.Required,
                Emphasize = identityResource.Emphasize
            };
        }
        private ConsentViewModel CreateConsentViewModel(AuthorizationRequest request, Client client, Resources resources, InputConsentViewModel model)
        {
            var rememberConsent = model?.RememberConsent ?? true;
            var selectedScopes = model?.ScopesConsented ?? Enumerable.Empty<string>();
            var vm = new ConsentViewModel();
            vm.ClientName = client.ClientName;
            vm.ClientLogoUrl = client.LogoUri;
            vm.ClientUrl = client.ClientUri;
            vm.RememberConsent = rememberConsent;
            vm.IdentityScorpes = resources.IdentityResources.Select(i => CreatScopeViewModel(i, selectedScopes.Contains(i.Name) || model == null));
            vm.ResourceScorpes = resources.ApiResources.SelectMany(i => i.Scopes).Select(x => CreatScopeViewModel(x, selectedScopes.Contains(x.Name) || model == null));
            return vm;
        }
        public async Task<ConsentViewModel> BulidConsentViewModel(string returnUrl, InputConsentViewModel model = null)
        {

            var request = await _identityServerInteractionService.GetAuthorizationContextAsync(returnUrl);
            if (request == null)
                return null;
            var client = await _clientStore.FindEnabledClientByIdAsync(request.ClientId);
            var resoureces = await _resourceStore.FindEnabledResourcesByScopeAsync(request.ScopesRequested);

            var vm = CreateConsentViewModel(request, client, resoureces, model);
            vm.ReturnUrl = returnUrl;
            return vm;
        }
        public async Task<ProcessConsentResult> ProcessConent(InputConsentViewModel viewModel)
        {
            ConsentResponse consentResponse = null;
            var result = new ProcessConsentResult();
            if (viewModel.Button == "no")
            {
                consentResponse = ConsentResponse.Denied;
            }
            else if (viewModel.Button == "yes")
            {
                if (viewModel.ScopesConsented != null && viewModel.ScopesConsented.Any())
                {
                    consentResponse = new ConsentResponse
                    {
                        RememberConsent = viewModel.RememberConsent,
                        ScopesConsented = viewModel.ScopesConsented
                    };
                }

                result.ValidationError = "请至少选中一个权限";
            }

            if (consentResponse != null)
            {
                var request = await _identityServerInteractionService.GetAuthorizationContextAsync(viewModel.ReturnUrl);
                await _identityServerInteractionService.GrantConsentAsync(request, consentResponse);

                result.RedirectUrl = viewModel.ReturnUrl;
            }

            {
                var consentfViewModel = await BulidConsentViewModel(viewModel.ReturnUrl, viewModel);
                result.ViewModel = consentfViewModel;
            }
            return result;
        }
        //public ConsentController(
        //    IClientStore clientStore,
        //    IResourceStore resourceStore,
        //    IIdentityServerInteractionService identityServerInteractionService)
        //{
        //    _clientStore = clientStore;
        //    _resourceStore = resourceStore;
        //    _identityServerInteractionService = identityServerInteractionService;
        //}

        public ConsentController(ConsentService consentService)
        {
            _consentService = consentService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string returnUrl)
        {
             var model = await _consentService.BulidConsentViewModel(returnUrl);
           // var model = await BulidConsentViewModel(returnUrl);
            if (model == null)
            {

            }
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> Index(InputConsentViewModel viewModel)
        {
            var result = await _consentService.ProcessConent(viewModel);
            //var result = await ProcessConent(viewModel);
            if (result.IsRedirectUrl)
            {
                return Redirect(result.RedirectUrl);
            }
            if (!string.IsNullOrEmpty(result.ValidationError))
            {
                ModelState.AddModelError("", result.ValidationError);
            }
            return View(result.ViewModel);
        }
    }
}
