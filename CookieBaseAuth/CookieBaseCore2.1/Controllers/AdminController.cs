﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace CookieBaseAuth.Controllers
{
    //验证Cookie权限控制器
    //可配置验证策略 角色 和 验证方式
    [Authorize]
   
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.User.Identity.IsAuthenticated)  //判断用户是否通过认证
            {
                var userName = HttpContext.User.Claims.First().Value;
            }
            return View();
          
        }
    }
}