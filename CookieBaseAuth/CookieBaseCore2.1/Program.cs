﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CookieBaseCore2.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CookieBaseAuth
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args)
               .MigrateDbContext<ApplicationDbContext>((context, services) =>
               {
                   new ApplicationDbContextSeed().SeedAsync(context, services)
                   .Wait();
               })
               .Run();
           // CreateWebHostBuilder(args).Build().Run();
        }
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //.UseEnvironment("Production")
                .UseEnvironment("Development")
                .UseUrls("http://localhost:5000")
                .UseStartup<Startup>()
                .Build();
        //public static IWebHost BuildWebHost(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //        .UseStartup<Startup>();
    }
}
