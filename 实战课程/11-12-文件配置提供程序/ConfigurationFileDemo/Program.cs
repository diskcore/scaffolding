﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;

namespace ConfigurationFileDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //optional文件不存在报错
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json",optional:false,reloadOnChange:true);
            //后添加的优先级高一些
            //builder.AddIniFile("appsettings.ini");
            //appsettings.Development.json
            //builder.AddJsonFile("appsettings.Development.json");
            var configurationRoot = builder.Build();
            //Console.WriteLine($"Key1:{config.Key1}");
            //Console.WriteLine($"Key5:{config.Key5}");
            var config = new Config()
            {
                Key1 = "config key1",
                Key5 = false
            };


            //在日常开发中使用的比较多
            configurationRoot.GetSection("OrderService").Bind(config,
                binderOptions => { binderOptions.BindNonPublicProperties = true; });

            Console.WriteLine($"Key1:{config.Key1}");
            Console.WriteLine($"Key5:{config.Key5}");
            Console.WriteLine($"Key6:{config.Key6}");




            IChangeToken token = configurationRoot.GetReloadToken();

            //IChangeToken只能使用一次  一次性热更新
            //token.RegisterChangeCallback(state =>
            //{
            //    Console.WriteLine($"Key1:{configurationRoot["Key1"]}");
            //    Console.WriteLine($"Key2:{configurationRoot["Key2"]}");
            //    Console.WriteLine($"Key3:{configurationRoot["Key3"]}");              
            //}, configurationRoot);

            //处理IChangeToken只能使用一次的问题 每次修改都会触发  永久性热更新
            //ChangeToken.OnChange(() => configurationRoot.GetReloadToken(), () =>
            //{
            //    Console.WriteLine($"Key1:{configurationRoot["Key1"]}");
            //    Console.WriteLine($"Key2:{configurationRoot["Key2"]}");
            //    Console.WriteLine($"Key3:{configurationRoot["Key3"]}");
            //});
            //Console.WriteLine("开始了");

            //Console.ReadKey();
            //Console.WriteLine($"Key1:{configurationRoot["Key1"]}");
            //Console.WriteLine($"Key2:{configurationRoot["Key2"]}");
            //Console.WriteLine($"Key3:{configurationRoot["Key3"]}");
            Console.ReadKey();
        }

        class Config
        {
            public string Key1 { get; set; }
            public bool Key5 { get; set; }
            public int Key6 { get; private set; } = 100;
        }

    }
}
