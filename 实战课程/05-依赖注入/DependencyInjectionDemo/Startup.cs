using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DependencyInjectionDemo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DependencyInjectionDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            #region 注册服务不同生命周期的服务
            services.AddSingleton<IMySingletonService, MySingletonService>();

            services.AddScoped<IMyScopedService, MyScopedService>();

            services.AddTransient<IMyTransientService, MyTransientService>();

            #endregion

            #region 花式注册
            services.AddSingleton<IOrderService>(new OrderService());  //直接注入实例
            services.AddSingleton<IOrderService, OrderServiceEx>();
            //services.AddSingleton<IOrderService, OrderService>();
            //services.AddSingleton<IOrderService, OrderService>();

            //工厂方式注册
            services.AddSingleton<IOrderService>(serviceProvider =>
            {
                return new OrderServiceEx();
            });

            //services.AddScoped<IOrderService>(serviceProvider =>
            //{
            //    //serviceProvider.GetService<>
            //    return new OrderServiceEx();
            //});

            #endregion

            #region 尝试注册
            //如果已经注册了就不再注册
            //services.TryAddSingleton<IOrderService, OrderServiceEx>();

            //如果已经注册了同类型的才不注册 否则照常注册
            //services.TryAddEnumerable(ServiceDescriptor.Singleton<IOrderService, OrderServiceEx>());

            //services.TryAddEnumerable(ServiceDescriptor.Singleton<IOrderService, OrderServiceEx>());

            //services.TryAddEnumerable(ServiceDescriptor.Singleton<IOrderService>(new OrderServiceEx()));

            //services.TryAddEnumerable(ServiceDescriptor.Singleton<IOrderService>(p =>
            //{
            //    return new OrderServiceEx();
            //}));
            #endregion

            #region 移除和替换注册
            //替换掉的第一个注册的服务
            //services.Replace(ServiceDescriptor.Singleton<IOrderService, OrderServiceEx>());

            //移除所有实例的注册
            //services.RemoveAll<IOrderService>();

            #endregion




            #region 注册泛型模板
            services.AddSingleton(typeof(IGenericService<>), typeof(GenericService<>));
            #endregion
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
