﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using GrpcServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;
using Polly.Bulkhead;
using Polly.CircuitBreaker;
using static GrpcServices.OrderGrpc;

namespace GrpcClientDemo
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true); //允许使用不加密的HTTP/2协议
            //命名规则服务名+Client
            services.AddGrpcClient<OrderGrpc.OrderGrpcClient>(options =>
            {
                options.Address = new Uri("https://localhost:5002");
            })
             .ConfigurePrimaryHttpMessageHandler(provider =>
              {
                  var handler = new SocketsHttpHandler();
                  handler.SslOptions.RemoteCertificateValidationCallback = (a, b, c, d) => true; //允许无效、或自签名证书
                  return handler;
              })
             .AddTransientHttpErrorPolicy(p => p.WaitAndRetryForeverAsync(i => TimeSpan.FromSeconds(i * 3)));

            //配置自己的策略
            var reg = services.AddPolicyRegistry();

            reg.Add("retryforever", Policy.HandleResult<HttpResponseMessage>(message =>
            {
                return message.StatusCode == System.Net.HttpStatusCode.Created;
            }).RetryForeverAsync());
            //策略应用
            services.AddHttpClient("orderclient").AddPolicyHandlerFromRegistry("retryforever");

            //动态配置策略
            services.AddHttpClient("orderclientv2").AddPolicyHandlerFromRegistry((registry, message) =>
            {
                //请求方法是Get方法就配置retryforever策略 否则响应空的策略
                return message.Method == HttpMethod.Get ?
                registry.Get<IAsyncPolicy<HttpResponseMessage>>("retryforever") 
                : Policy.NoOpAsync<HttpResponseMessage>();
            });

            //服务发生10次异常就熔断10s
            services.AddHttpClient("orderclientv3").AddPolicyHandler(Policy<HttpResponseMessage>.Handle<HttpRequestException>().CircuitBreakerAsync(
                handledEventsAllowedBeforeBreaking: 10, //报错多少次后进行熔断
                durationOfBreak: TimeSpan.FromSeconds(10),//熔断的时间
                onBreak: (r, t) => { },//发生熔断执行的事件
                onReset: () => { },//熔断恢复时的事件
                onHalfOpen: () => { }//验证服务是否可用
                ));

            //根据失败的比例进行熔断
            services.AddHttpClient("orderclientv3").AddPolicyHandler(Policy<HttpResponseMessage>.Handle<HttpRequestException>().AdvancedCircuitBreakerAsync(
                failureThreshold: 0.8,//百分之80
                samplingDuration: TimeSpan.FromSeconds(10),//采样的时间
                minimumThroughput: 100,//最小的吞吐量 请求量小的时候不需要熔断
                durationOfBreak: TimeSpan.FromSeconds(20),//熔断的时长
                onBreak: (r, t) => { },
                onReset: () => { },
                onHalfOpen: () => { }));


            var breakPolicy = Policy<HttpResponseMessage>.Handle<HttpRequestException>().AdvancedCircuitBreakerAsync(
                failureThreshold: 0.8,
                samplingDuration: TimeSpan.FromSeconds(10),
                minimumThroughput: 100,
                durationOfBreak: TimeSpan.FromSeconds(20),
                onBreak: (r, t) => { },
                onReset: () => { },
                onHalfOpen: () => { });

            var message = new HttpResponseMessage()
            {
                Content = new StringContent("{}")
            };
            var fallback = Policy<HttpResponseMessage>.Handle<BrokenCircuitException>().FallbackAsync(message);
            var retry = Policy<HttpResponseMessage>.Handle<Exception>().WaitAndRetryAsync(3, i => TimeSpan.FromSeconds(1));
            //组合策略发生熔断重试3次如果还是失败就响应一个fallback结果
            var fallbackBreak = Policy.WrapAsync(fallback, retry, breakPolicy);
            services.AddHttpClient("httpv3").AddPolicyHandler(fallbackBreak);


            var bulk = Policy.BulkheadAsync<HttpResponseMessage>(
                maxParallelization: 30,//最大并发是30
                maxQueuingActions: 20,//最大的队列数是20 排队20   总数=30+20
                onBulkheadRejectedAsync: contxt => Task.CompletedTask
                );

            var message2 = new HttpResponseMessage()
            {
                Content = new StringContent("{}")
            };
            var fallback2 = Policy<HttpResponseMessage>.Handle<BulkheadRejectedException>().FallbackAsync(message);
            var fallbackbulk = Policy.WrapAsync(fallback2, bulk);
            services.AddHttpClient("httpv4").AddPolicyHandler(fallbackbulk);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                //endpoints.MapGrpcService<GreeterService>();
                endpoints.MapGet("/", async context =>
                {
                    OrderGrpcClient service = context.RequestServices.GetService<OrderGrpcClient>();

                    try
                    {
                        var r = service.CreateOrder(new CreateOrderCommand { BuyerId = "abc" });
                    }
                    catch (Exception ex)
                    {
                    }

                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
