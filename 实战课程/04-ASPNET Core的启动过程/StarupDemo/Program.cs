using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace StarupDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
        //执行方法：ConfigureWebHostDefaults
        //执行方法：ConfigureHostConfiguration
        //执行方法：ConfigureAppConfiguration
        //执行方法：ConfigureServices
        //执行方法：ConfigureLogging
        //执行方法：Startup
        //执行方法：Startup.ConfigureServices
        //执行方法：Startup.Configure



        //执行方法：ConfigureWebHostDefaults       *2将ConfigureWebHostDefaults顺序调到前面
        //执行方法：ConfigureHostConfiguration
        //执行方法：ConfigureAppConfiguration
        //执行方法：Startup
        //执行方法：Startup.ConfigureServices
        //执行方法：ConfigureServices
        //执行方法：ConfigureLogging
        //执行方法：Startup.Configure
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureHostConfiguration(configurationBuilder =>
                {
                    //2
                    Console.WriteLine("执行方法：ConfigureHostConfiguration");
                })
                .ConfigureServices(services =>
                {
                    //4
                    Console.WriteLine("执行方法：ConfigureServices");
                })
                .ConfigureLogging(loggingBuilder =>
                {
                    //5
                    Console.WriteLine("执行方法：ConfigureLogging");
                })
                .ConfigureAppConfiguration((hostBuilderContext, configurationBinder) =>
                {
                    //3
                    Console.WriteLine("执行方法：ConfigureAppConfiguration");
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    //1  webBuilder 相当于Startup
                    Console.WriteLine("执行方法：ConfigureWebHostDefaults");
                    //webBuilder 相当于Startup
                    //webBuilder.ConfigureServices(services =>
                    //{
                    //    services.AddControllers();
                    //});
                    //webBuilder.Configure(service => { 
                    //});
                    webBuilder.UseStartup<Startup>();
                })
              ;
    }
}
