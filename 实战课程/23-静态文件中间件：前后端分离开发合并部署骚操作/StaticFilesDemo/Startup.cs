using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace StaticFilesDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

       
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDirectoryBrowser();
        }
        const int BufferSize = 64 * 1024;

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //默认指向wwwroot
            //app.UseDirectoryBrowser();
             //app.UseDefaultFiles();
             // app.UseStaticFiles();

            //指向file文件 可以设置多指向
            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    RequestPath = "/files",
            //    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "file"))
            //});

            //可以在域名下查看文件结构
             //app.UseDirectoryBrowser();
            //app.UseDefaultFiles();
            //app.UseFileServer();
            //app.UseFileServer(new FileServerOptions()
            // {
            //    FileProvider = new PhysicalFileProvider
            //    (
            //         Path.Combine(Directory.GetCurrentDirectory(), @"file")),   //实际目录地址
            //         RequestPath = new Microsoft.AspNetCore.Http.PathString("/Image"),  //用户访问地址
            //         EnableDirectoryBrowsing = true                                     //开启目录浏览
            //  });

            app.MapWhen(context =>
            {
                return !context.Request.Path.Value.StartsWith("/api");
            }, appBuilder =>
            {
                var option = new RewriteOptions();
                option.AddRewrite(".*", "/index.html", true);
                appBuilder.UseRewriter(option);

                appBuilder.UseStaticFiles();
                //appBuilder.Run(async c =>
                //{
                //    //D:\Core0717\scaffolding\实战课程\23-静态文件中间件：前后端分离开发合并部署骚操作\StaticFilesDemo\wwwroot\
                //    var file = env.WebRootFileProvider.GetFileInfo("index.html");

                //    c.Response.ContentType = "text/html";
                //    using (var fileStream = new FileStream(file.PhysicalPath, FileMode.Open, FileAccess.Read))
                //    {
                //        await StreamCopyOperation.CopyToAsync(fileStream, c.Response.Body, null, BufferSize, c.RequestAborted);
                //    }
                //});
            });
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
