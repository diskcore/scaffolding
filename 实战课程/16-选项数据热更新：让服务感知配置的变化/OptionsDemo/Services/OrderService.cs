﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OptionsDemo.Services
{
    /// <summary>
    /// 服务定义
    /// </summary>
    public interface IOrderService
    {
        int ShowMaxOrderCount();
    }
    public class OrderService : IOrderService
    {
        //IOptions<OrderServiceOptions> _options;        //默认形式
        //IOptionsSnapshot<OrderServiceOptions> _options;  //热更新形式不能和AddSingleton配合使用
        IOptionsMonitor<OrderServiceOptions> _options;     //单例对象也能读到最新的变化
        public OrderService(/*IOptions<OrderServiceOptions>*/ /*IOptionsSnapshot<OrderServiceOptions>*/
            IOptionsMonitor<OrderServiceOptions>  options)
        {
            _options = options;

            //调试模式下可以监听到两个输出 
            _options.OnChange(option =>
            {
                Console.WriteLine($"配置更新了，最新的值是:{_options.CurrentValue.MaxOrderCount}");
            });
        }

        public int ShowMaxOrderCount()
        {
            return _options.CurrentValue.MaxOrderCount;
        }
    }

    public class OrderServiceOptions
    {
       // [Range(30, 100)]
        public int MaxOrderCount { get; set; } = 100;
    }


    //public class OrderServiceValidateOptions : IValidateOptions<OrderServiceOptions>
    //{
    //    public ValidateOptionsResult Validate(string name, OrderServiceOptions options)
    //    {
    //        if (options.MaxOrderCount > 100)
    //        {
    //            return ValidateOptionsResult.Fail("MaxOrderCount 不能大于100");
    //        }
    //        else
    //        {
    //            return ValidateOptionsResult.Success;
    //        }
    //    }
    //}
}
