﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeekTime.Domain
{
    //实体多ID
    public interface IEntity
    {
        object[] GetKeys();
    }
    //实体单ID
    public interface IEntity<TKey> : IEntity
    {
        TKey Id { get; }
    }
}
