﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpClientFactoryDemo.Clients
{
    //注册的时候注册类名 使用的时候使用HttpClient
    public class TypedOrderServiceClient
    {
        //以类名作为名称
        HttpClient _client;
        public TypedOrderServiceClient(HttpClient client)
        {
            _client = client;
        }


        public async Task<string> Get()
        {
            return await _client.GetStringAsync("/OrderService"); //这里使用相对路径来访问
        }
    }
}
