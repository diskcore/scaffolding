﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace ConfigurationCustom
{
    class MyConfigurationProvider : ConfigurationProvider
    {

        Timer timer;

        public MyConfigurationProvider() : base()
        {
            dateTime= DateTime.Now;
            timer = new Timer();
            timer.Elapsed += Timer_Elapsed;
            timer.Interval = 3000;
            timer.Start();
        }
        DateTime dateTime;
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Load(true);
        }
        //首次加载
        public override void Load()
        {
            //加载数据
            Load(false);
        }

        void Load(bool reload)
        {
            //this.Data["lastTime"] = dateTime.ToString();// DateTime.Now.ToString();
            this.Data["lastTime"] = DateTime.Now.ToString();
            if (reload)
            {
                //触发更新重新加载
                base.OnReload();
            }
        }


    }
}
