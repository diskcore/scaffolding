﻿using Microsoft.Extensions.FileProviders;
using System;

namespace FileProviderDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //物理文件
            IFileProvider provider1 = new PhysicalFileProvider(AppDomain.CurrentDomain.BaseDirectory);

            //var contents = provider1.GetDirectoryContents("/");


            //foreach (var item in contents)
            //{

            //    Console.WriteLine(item.Name);
            //}
            //嵌入式文件
            IFileProvider provider2 = new EmbeddedFileProvider(typeof(Program).Assembly);


            var html = provider2.GetFileInfo("emb.html");
           //组合文件
            IFileProvider provider = new CompositeFileProvider(provider1, provider2);



            var contents = provider.GetDirectoryContents("/");


            foreach (var item in contents)
            {
                //读取文件
                var stream= item.CreateReadStream();
                Console.WriteLine(item.Name);
            }

            Console.ReadKey();
        }
    }
}
