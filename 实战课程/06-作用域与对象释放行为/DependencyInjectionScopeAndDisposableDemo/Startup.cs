using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DependencyInjectionScopeAndDisposableDemo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DependencyInjectionScopeAndDisposableDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //瞬时和上下文模式容器管理实例释放
            //services.AddTransient<IOrderService, DisposableOrderService>();
            //services.AddScoped<IOrderService, DisposableOrderService>();
            //services.AddSingleton<IOrderService, DisposableOrderService>();


            //services.AddTransient<IOrderService>(p=>new DisposableOrderService());
            //services.AddScoped<IOrderService>(p => new DisposableOrderService());
            //单列模式容器不管理实例释放  只会根容器释放了才会释放
            //services.AddSingleton<IOrderService>(new DisposableOrderService());


            //var service = new DisposableOrderService();
            //根容器释放了不会释放
            //services.AddSingleton<IOrderService>(new DisposableOrderService());
            //根容器释放会释放
            //services.AddSingleton<IOrderService, DisposableOrderService>();
            //根容器释放会释放
            //services.AddSingleton<IOrderService>(p => new DisposableOrderService());

            services.AddTransient<IOrderService, DisposableOrderService>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //从根容器获取瞬时服务 只有根容器释放了才会回收
            var s = app.ApplicationServices.GetService<IOrderService>();
            var s2 = app.ApplicationServices.GetService<IOrderService>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
