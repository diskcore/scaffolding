﻿using System;

namespace QuartzApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Hello World!");
            QuartZ();


            Console.Read();
        }
        public static async void QuartZ()
        {
            QuartzHelper quartzHelper = new QuartzHelper();
            await quartzHelper.Init();
            quartzHelper.BuildTriggerWithParam();
            quartzHelper.BuildJobDetailWithParam();
            await quartzHelper.BindTriggerJob();
        }
    }
}
