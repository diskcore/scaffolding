﻿using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace QuartzApp
{
    //创建IJob的实现类，并实现Excute方法。
    //执行后保留作业数据
    [PersistJobDataAfterExecution]//更新JobDetail的JobDataMap的存储副本，以便下一次执行这个任务接收更新的值而不是原始存储的值
    public class MyJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {

            //获取Job中的参数
            var jobData = context.JobDetail.JobDataMap;

            //获取Trigger中的参数
            var triggerData = context.Trigger.JobDataMap;

            //当Job中的参数和Trigger中的参数名称一样时，用 context.MergedJobDataMap获取参数时，Trigger中的值会覆盖Job中的值
            //获取Job和Trigger中合并的参数
            var data = context.MergedJobDataMap;

            var value1 = jobData.GetInt("key1");
            var value2 = jobData.GetString("key2");

            var dateString = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
            Random random = new Random();

            ////这里面给key赋值，下次再进来执行的时候，获取的值为更新的值，而不是原始值
            jobData["key1"] = random.Next(1, 20);
            jobData["key2"] = dateString;
            return Task.Run(() =>
            {
                using (StreamWriter sw = new StreamWriter(@"D:\个人开发\error.log", true, Encoding.UTF8))
                {
                    sw.WriteLine($"{dateString} value1:{value1} value2:{value2}");
                    Console.WriteLine($"{dateString} value1:{value1} value2:{value2}");
                }
            });
        }
    }
}
