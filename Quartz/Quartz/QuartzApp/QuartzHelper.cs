﻿using Quartz;
using Quartz.Impl;
using System.Threading.Tasks;

namespace QuartzApp
{
    public  class QuartzHelper
    {

        //调度器
        private IScheduler _scheduler;
        //触发器
        private ITrigger _trigger;
        //任务
        private IJobDetail _jobDetail;
        //多任务调度
        public QuartzHelper()
        {

        }
        /// <summary>
        /// 务必初始化初始化并启动默认调度器
        /// </summary>
        /// <returns></returns>
        public async Task Init()
        {
            //使用默认工厂
            _scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            await _scheduler.Start();
        }

        /// <summary>
        /// 获取触发器Builder
        /// </summary>
        /// <returns></returns>
        public TriggerBuilder GetTriggerBuilder()
        {
            return TriggerBuilder.Create();
        }
        /// <summary>
        /// 创建JobBuilder
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public JobBuilder GetJobBuilder<T>() where T : IJob
        {
            return JobBuilder.Create<T>();
        }
        /// <summary>
        /// 建立Trigger
        /// </summary>
        /// <param name="triggerBuilder"></param>
        /// <returns></returns>
        private ITrigger GetTrigger(TriggerBuilder triggerBuilder)
        {
            return triggerBuilder.Build();
        }
        /// <summary>
        /// 建立IJobDetail
        /// </summary>
        /// <param name="jobBuilder"></param>
        /// <returns></returns>
        private IJobDetail GetJobDetail(JobBuilder jobBuilder)//JobBuilder
        {
            return jobBuilder.Build();
        }

        //泛型实现解耦
        public void SetIdentity<T1, T2>(T1 t1, string name, string group) where T1 : JobBuilder where T2 : TriggerBuilder
        {
            t1.WithIdentity(name, group);
        }


        /// <summary>
        /// 设置Trigger标识
        /// </summary>
        /// <param name="triggerBuilder"></param>
        /// <param name="name"></param>
        /// <param name="group"></param>
        public void SetTriggerIdentity(TriggerBuilder triggerBuilder, string name, string group)
        {
            triggerBuilder.WithIdentity(name, group);
        }
        /// <summary>
        /// 设置Job标识
        /// </summary>
        /// <param name="jobBuilder"></param>
        /// <param name="name"></param>
        /// <param name="group"></param>
        public void SetJobIdentity(JobBuilder jobBuilder, string name, string group)
        {
            jobBuilder.WithIdentity(name, group);
        }

        /// <summary>
        /// 设置Trigger参数
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetTriggerParam<T>(TriggerBuilder triggerBuilder, string key, string value)
        {
            triggerBuilder.UsingJobData(key, value);
        }

        /// <summary>
        /// 设置Trigger参数
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetJobParam<T>(JobBuilder jobBuilder, string key, string value)
        {
            jobBuilder.UsingJobData(key, value);
        }

        /// <summary>
        /// 建立触发器Trigger
        /// SimpleTrigger触发器和CronTrigger触发器。
        /// </summary>
        /// <param name="action"></param>
        public void BuildTrigger(TriggerBuilder triggerBuilder)
        {
            _trigger = GetTrigger(triggerBuilder);
            //创建一个触发器
            //_trigger = TriggerBuilder.Create()
            //                .WithSimpleSchedule(x => x.WithIntervalInSeconds(2).RepeatForever())//每两秒执行一次 一直执行
            //                .Build();

            //限制次数
            //_trigger = TriggerBuilder.Create()
            //                .WithSimpleSchedule(x => x.WithIntervalInSeconds(2).WithRepeatCount(5))
            //                .Build();
            //_trigger= TriggerBuilder.Create().WithDailyTimeIntervalSchedule
            //      (s =>
            //         s.WithIntervalInMinutes(5)
            //        .OnEveryDay()
            //        .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0))
            //      )
            //    .Build();
            //_trigger = TriggerBuilder.Create()
            //               .WithSimpleSchedule(action)//每两秒执行一次
            //               .Build();
        }
        /// <summary>
        /// 触发器含参数
        /// 支持所有基础类型
        /// 复杂类型使用：JobDataMap
        /// </summary>
        public void BuildTriggerWithParam()
        {
            _trigger = TriggerBuilder.Create()
                       .WithDailyTimeIntervalSchedule
                  (s =>
                     s.WithIntervalInSeconds(4)
                    .OnEveryDay()
                    .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(10, 8))
                  )//间隔2秒 一直执行
                        .UsingJobData("key1", 321)
                        .UsingJobData("key2", "123")
                        //使用具有给定名称和组的TriggerKey来标识触发器。
                        .WithIdentity("trigger2", "group1")
                        .Build();
        }


        //设置JobDetail
        public void SetJobDetail(JobBuilder jobBuilder)
        {
            _jobDetail = GetJobDetail(jobBuilder);
        }


        /// <summary>
        /// 建立任务JobDetail
        /// </summary>
        public void BuildJobDetail()
        {
            //创建任务
            _jobDetail = JobBuilder.Create<MyJob>()
                            .WithIdentity("job", "group")
                            .Build();
        }
        /// <summary>
        /// 建立JobDetail含参数
        /// </summary>
        public void BuildJobDetailWithParam()
        {
            _jobDetail = JobBuilder.Create<MyJob>()
                            .UsingJobData("key1", 123)//通过Job添加参数值
                            .UsingJobData("key2", "123")
                            .WithIdentity("job1", "group1")
                           .Build();
        }


        /// <summary>
        /// 将触发器和任务器绑定到调度器中
        /// </summary>
        /// <returns></returns>
        public async Task BindTriggerJob()
        {
            await _scheduler.ScheduleJob(_jobDetail, _trigger);
        }


    }
}
